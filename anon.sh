#!/bin/bash

FLAG="-path ./.git -prune -o"

cp ~/projects/themis/source/.classpath ~/projects/themis/source/.project source/
find . $FLAG -type f -name "*.java"     -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.aj"       -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.md"       -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.xml"      -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "Makefile"   -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.xml"      -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.mk"      -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.txt"      -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +
find . $FLAG -type f -name "*.properties"      -exec sed -i 's/uga\.corse\.themis/anon\.themis/g' {} +

find . $FLAG -type f -name "*.md"       -exec sed -i 's#uga/corse/themis#anon/themis#g' {} +
find . $FLAG -type f -name "*.xml"      -exec sed -i 's#uga/corse/themis#anon/themis#g' {} +
find . $FLAG -type f -name "*.mk"      -exec sed -i 's#uga/corse/themis#anon/themis#g' {} +
find . $FLAG -type f -name "*.properties"      -exec sed -i 's#uga/corse/themis#anon/themis#g' {} +

grep "corse" -r source/
grep "corse" -r experiments/
grep "corse" -r example/
grep "corse" -r plot/
