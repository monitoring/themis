select  b1.spec
  From bench as b1
  INNER join bench as b2 on b2.ltl_hash == b1.ltl_hash AND b2.ltl_comp == b1.ltl_comp AND b1.tid == b2.tid
where b1.alg == "%alg1%" AND b2.alg=="%alg2%"
  AND b1.verdict <> b2.verdict
    GROUP BY b1.verdict, b2.verdict, b1.ltl_hash, b1.spec;
