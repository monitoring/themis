#!/bin/bash

# Output the absolute path where this script is found
function epath {
  echo "$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
}
