# Path to the Themis JAR
THEMISPATH ?=../../themis.jar
# Add classpath
CP= ${CPADD}:${THEMISPATH}

# Java command
RUNXP=java -Xms8G -Xmx12G -cp ${CP} -javaagent:${THEMISPATH} anon.themis.tools.experiment.Experiment

# Text containing formulae
FORMS ?= forms.txt

.PHONY: default clean verify

default:
	$(info Use 'spec' to prepare spec, 'run' to run experiment and 'verify' to verify the result)

# Cleanup by deleting spec 
clean:
	rm -rf spec
	rm run.db old.run.db

# Verify the Run given our algorithms
# Places wrong spec in err.log
verify:
	$(info Verifying data for invalid runs)
	@../verify run.db Orchestration Migration > err.log
	@../verify run.db Orchestration Choreography >> err.log
	test ! -s err.log
	@rm err.log
	@echo Success

# Generate Specification
spec: ${FORMS}
	rm -rf spec
	mkdir spec
	../mkspec ../templates/ltl.xml spec < ${FORMS}

# Run the experiment
run: 
	${RUNXP} .
