attach database 'comp3/run.db' as e1;
attach database 'comp4/run.db' as e2;
attach database 'comp5/run.db' as e3;


CREATE TABLE `bench` (
  `run_num` 	  INTEGER PRIMARY KEY AUTOINCREMENT,
	run_id 	      INTEGER NOT NULL, 
	tag 		      TEXT, 
	run_hash 	    INT NOT NULL, 
	spec          TEXT NOT NULL, 
	ltl_size 	    INT NOT NULL, 
	ltl_hash 	    INT NOT NULL, 
	ltl_host 	    TEXT NOT NULL, 
	ltl_comp 	    INT NOT NULL, 
	ltl_depth 	  INT NOT NULL, 
	aut_outavg 	  REAL NOT NULL, 
	aut_states 	  INT NOT NULL, 
	aut_pairsmax 	INT NOT NULL, 
	aut_pairsavg  REAL NOT NULL, 
	aut_labelavg  REAL NOT NULL, 
	aut_labelmax  INT NOT NULL, 
	aut_outmax    INT NOT NULL, 
	exp_avg       REAL NOT NULL, 
	load_exp      REAL NOT NULL, 
	exp_eval      INT NOT NULL, 
	spread_exp    REAL NOT NULL, 
	tid           INT NOT NULL, 
	msg_data      INT NOT NULL, 
	comps         INT NOT NULL, 
	msg_num       INT NOT NULL, 
	verdict       TEXT NOT NULL, 
	run_len       INT NOT NULL, 
	alg           TEXT NOT NULL, 
	simp_avg      INT NOT NULL, 
	load_simp     REAL NOT NULL, 
	simp_total    INT NOT NULL, 
	spread_simp   REAL NOT NULL, 
	sum_delay     INT NOT NULL, 
	max_delay     INT NOT NULL, 
	resolutions   INT NOT NULL, 
	min_delay     INT NOT NULL
);
BEGIN;
insert into 'bench' select NULL, * from e1.bench;
insert into 'bench' select NULL, * from e2.bench;
insert into 'bench' select NULL, * from e3.bench;
COMMIT;

detach e1;
detach e2;
detach e3;
