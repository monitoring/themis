DIR=.
PAN=pandoc -c ../github.css -s -S README.md -o index.html
docs:
	(cd ${DIR}/source && ${PAN})
	(cd ${DIR}/example && ${PAN})
	(cd ${DIR}/experiments && ${PAN})
	(cd ${DIR}/plot && ${PAN})
	(cd ${DIR}/docker && ${PAN})
	(cd ${DIR} && pandoc -c github.css -s -S README.md -o index.html)
	sed -i 's/README\.md\"/index\.html\"/g' ${DIR}/index.html
