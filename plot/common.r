library(reshape2)
library(ggplot2)
library(dplyr)
library(xtable)

gdraw <- function(g) {
  return(g + theme_minimal() +
    theme(axis.line = element_line(colour = 'black', size = .5))
  )
}
th_quantile <- function(dat, grp, x, col) {
  qt <- function(x, y) {
    return(quantile(x)[y]);
  }
  low  <- function(x) { return(qt(x, 2)) }
  mid  <- function(x) { return(qt(x, 3)) }
  high <- function(x) { return(qt(x, 4)) }
  return( dat %>%
    group_by_(grp, x) %>%
    summarise_at(col, funs(low, mid, high)))
}

th_bench <- function(dat, x, y, xlbl, ylbl, coef=9) { 
  th_group(dat, "alg", "Algorithm", x, y, xlbl, ylbl, coef)
}

th_group <- function(dat, grp, grplbl,  x, y, xlbl, ylbl, coef=9) {
  dd <- th_quantile(dat, grp,  x, y)
  gdraw(ggplot(data=dd, aes_string(x=x, y="mid", group=grp)) +
     geom_line(aes_string(color=grp, linetype=grp), size=1.2) +
     geom_point(aes_string(color=grp))) + 
     #geom_errorbar(aes(ymax=high, ymin=low, x=ltl_comp, color=alg), size=.5, width=.2)
     xlab(xlbl) + ylab(paste("Average", ylbl)) +  labs(color=grplbl, linetype=grplbl) +
     theme(legend.position="bottom")
  ggsave(paste(grp, "_", x, "-", y, ".pdf", sep=""))
  
  names(dd) <- c(grp, paste(y, "-25", sep=""),  paste(y, "-50", sep=""), paste(y, "-75", sep=""))
  print(xtable(dd), include.rownames=FALSE)

  # Re-scale view to  coef times the interquartile coef from the box
  ylim1 = boxplot.stats(dat[[y]], coef=coef)$stats[c(1, 5)]

  gdraw(ggplot(data=dat, aes_string(x=grp, group=grp, y=y))+ geom_boxplot(width=.2, aes_string(fill=grp), outlier.shape=NA)) + coord_cartesian(ylim = ylim1) + 
    ylab(ylbl) +
    xlab(grplbl) + 
    theme(legend.position="none")
  ggsave(paste(grp, "_", x, "-", y, "_all.pdf", sep=""))
}
