package anon.themis.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ProcessBuilder;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.HashSet;

import anon.themis.Env;
import anon.themis.automata.Atom;
import anon.themis.automata.Automata;
import anon.themis.automata.Event;
import anon.themis.automata.Transition;
import anon.themis.automata.formats.FormatEvent;
import anon.themis.automata.formats.LabelEvent;
import anon.themis.automata.formats.TransitionFormatter;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.monitoring.SpecLTL;
import anon.themis.monitoring.Specification;
import anon.themis.parser.FrontAutomataParser;
import anon.themis.symbols.AtomNegation;

/**
 * Conversion Tools
 *
 */
public class Convert {
	public static String DEFAULT_TIMEOUT = "15";

	
	/**
	 * Convert a specification to an Automata Specification
	 * Currently only supports LTL specifications
	 * @param ltl
	 * @return
	 */
	public static SpecAutomata makeAutomataSpec(Specification ltl) {
		if(ltl instanceof SpecAutomata) 
			return (SpecAutomata) ltl;
		else if(ltl instanceof SpecLTL) {
			Automata aut;
			try {
				aut = Convert.fromLTL(ltl.toString(), new FormatEvent());
			} catch (IOException e) {
				aut = null;
			} catch (TimeoutException e) {
				throw new Error("Timeout creating Automaton for: " + ltl.toString());
			}
			
		
			if(aut == null || aut.getStates().isEmpty())
				throw new Error("Could not create automaton from: " + ltl.toString());
			
			completeEvents(aut);
			return new SpecAutomata(aut);
		}
		else
			throw new Error("Unsupported format: " + ltl.getClass().getSimpleName());
	}
	
	/**
	 * Constructs the automaton for an LTL Formula given as string using a transition Formatter to build transition labels
	 * @param formula  String to convert
	 * @param format Formatter to create automata transition labels
	 * @return
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public static Automata fromLTL(String formula, TransitionFormatter format) 
	throws IOException, TimeoutException
	{
		return runMon(formula, format);
	}
	
	/**
	 * Given a monitor automaton generated with LTL2Mon complete its transition labels to include all the alphabet
	 * We do not add all the alphabet in this implementation, but all alphabet necessary to make the distinction between transitions
	 * Therefore it only involves all symbols found on transitions from a given state (not all symbols in all the alphabet)
	 * @param base
	 */
	public static void completeEvents(Automata base)
	{
		HashSet<Atom> symbols = new HashSet<Atom>();
		
		//Iterate over transitions each list represents outbound transitions from one state
		for(List<Transition> outs : base.getDelta().values()) 
		{
			//For each of the transition, catch the events and add them to observed symbols
			for(Transition t : outs) {
				if(!(t.getLabel() instanceof LabelEvent)) return;
				
				Event ev = ((LabelEvent) t.getLabel()).getEvent();
				//Ignore empty events
				if(ev.hasEmpty()) continue;
				for(Atom a : ev.getAtoms())
				{
				  //Follow up to one level of negation
					if(a instanceof AtomNegation)
						a = ((AtomNegation) a).getNegated();
					symbols.add(a);
				}
			}
			//Complete the events with symbols basically
			//If observation sym not seen, it will be !sym instead of nothing
			for(Transition t : outs) {
				Event ev 		= ((LabelEvent) t.getLabel()).getEvent();
				if(!ev.hasEmpty()) {
					Set<Atom> inev 	= ev.getAtoms();
					for(Atom sym : symbols) {
						if(!inev.contains(sym))
							ev.addObservation(new AtomNegation(sym));
					}
				}
				else
				{
					ev.getAtoms().clear();
					for(Atom sym: symbols) ev.addObservation(new AtomNegation(sym));
				}
				
			}
			symbols.clear();
		}
	}
	
	/**
	 * Run the Spot's ltlfilt command line
	 * Mostly used for LTL or Boolean simplifcation
	 * @see <a href="https://spot.lrde.epita.fr/ltlfilt.html">Spot's ltlfilt page</a>
	 * @param expr
	 * @return
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public static String runLTLFILT(String expr) throws IOException, TimeoutException {
		Process p	= runProc(Env.SPOT_LTLFILT.getValue("ltlfilt"), new Object[] {"-r"});
		PrintWriter write		= new PrintWriter(p.getOutputStream());
		InputStream is			= p.getInputStream();
		InputStreamReader isr 	= new InputStreamReader(is);
		BufferedReader br 		= new BufferedReader(isr);
		StringBuilder sb 		= new StringBuilder();
		write.println(expr);
		write.flush();
		write.close();
		try {
			if(!p.waitFor(getTimeout(), TimeUnit.SECONDS)) {
				p.destroyForcibly();
		        write.close();
		        br.close();
				throw new TimeoutException();
			}
		} catch (InterruptedException e) {
	        write.close();
	        br.close();
			return null;
		}

		String line;
		while((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
        br.close();
		return sb.toString();
	}
	
	/**
	 * Run the ltl2mon command line tool
	 * @see <a href="http://ltl3tools.sourceforge.net/">LTL3 Tools</a>
	 * @param expr
	 * @param format
	 * @return
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public static Automata runMon(String expr, TransitionFormatter format) throws IOException, TimeoutException
	{

		Process p	= runProc(Env.LTL2MON_BIN.getValue("ltl2mon"), new Object[] {"-f"});
		PrintWriter write		= new PrintWriter(p.getOutputStream());
		InputStream is			= p.getInputStream();
		InputStreamReader isr 	= new InputStreamReader(is);
		BufferedReader br 		= new BufferedReader(isr);
		write.println(expr);
		write.flush();
		try {
			if(!p.waitFor(getTimeout(), TimeUnit.SECONDS)) {
				p.destroy();
				Thread.sleep(200);
				p.destroyForcibly();
				Thread.sleep(200);
		        write.close();
		        br.close();
				throw new TimeoutException();
			}
		} catch (InterruptedException e) {
	        write.close();
	        br.close();
			return null;
		}
        write.close();
		return FrontAutomataParser.fromStream(br, format);
	}
	 public static String runMonRaw(String expr, TransitionFormatter format) throws IOException, TimeoutException
	  {
	    Process p = runProc(Env.LTL2MON_BIN.getValue("ltl2mon"), new Object[] {"-f"});
	    PrintWriter write   	= new PrintWriter(p.getOutputStream());
	    InputStream is      	= p.getInputStream();
	    InputStreamReader isr 	= new InputStreamReader(is);
	    BufferedReader br     	= new BufferedReader(isr);
	    StringBuilder sb    	= new StringBuilder();
	    write.println(expr);
	    write.flush();
	    try {
	      if(!p.waitFor(getTimeout(), TimeUnit.SECONDS)) {
	        p.destroyForcibly();
	        throw new TimeoutException();
	      }
	    } catch (InterruptedException e) {
	        write.close();
	        br.close();
	        return null;
	    }

	    String line;
	    while((line = br.readLine()) != null) {
	      sb.append(line);
	      sb.append("\n");
	    }
        write.close();
        br.close();
	    return sb.toString();
	  }
	 
	/**
	 * Run a process 
	 * @param name Name of the process
	 * @param args Additional command line arguments
	 * @return
	 * @throws IOException
	 */
	public static Process runProc(String name, Object[] args) throws IOException 
	{
		String[] pargs = new String[args.length + 1];
		pargs[0] = name;
		for(int i = 1; i <= args.length; i++)
			pargs[i] = args[i-1].toString();
		
		ProcessBuilder pb = new ProcessBuilder(pargs);
		Process p 		  = pb.start();
		return p;
	}
	private static int getTimeout() {
		return Integer.parseInt(
				Env.TIMEOUT.getValue(DEFAULT_TIMEOUT)
			);
	}

}
