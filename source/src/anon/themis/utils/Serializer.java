/**
 * Serializer
 *
 * name <email>
 */
package anon.themis.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Serialize/Unserialize object from file
 * 
 */
public class Serializer<T> {
	public  boolean serialize(T object, File file) {
	    try (
    		FileOutputStream dest 	=	new FileOutputStream(file);
	    	ObjectOutputStream out = new ObjectOutputStream(dest);
	    ) {
	    	out.writeObject(object);
	    	out.close();
	    	dest.close();
	    	return true;
	    }catch(IOException i) {
	    	return false;
	    }
	}
	
	public T unserialize(File file) {
		try(
			FileInputStream src = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(src)
		){
			@SuppressWarnings("unchecked")
			T o = (T) in.readObject();
			in.close();
			src.close();
			return o; 
		} catch(IOException | ClassNotFoundException c) {
			return null;
		}
	}
}