package anon.themis.utils;

/**
 * For lambdas to handle options
 * 
 */
public interface OptAction {
  /**
   * Called with given params as specified for the option
   * If an exception is thrown then the option errors
   * @param params
   * @throws Exception
   */
	public void process(String[] params) throws Exception;
}
