package anon.themis.utils;

import anon.themis.expressions.bool.BoolExpression;

/**
 * A boolean expressions simplifier 
 * Needed to simplify expressions for automata representation
 * 
 */
public interface Simplifier {
	 BoolExpression simplify(BoolExpression e) ;
	 String simplify(String e);
}
