/**
 * AutomataCache
 *
 * name <email>
 */
package anon.themis.utils;

import java.io.File;

import anon.themis.automata.Automata;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.monitoring.SpecLTL;
import anon.themis.monitoring.Specification;
import anon.themis.utils.Convert;

/**
 * Cache calls to LTLMON if LTL already converted
 * 
 */
public aspect AutomataCache {
	SpecAutomata around(Specification spec) : 
		execution(SpecAutomata Convert.makeAutomataSpec(Specification))
		&& args(spec)
	{
		String CACHE_DIR = Opts.getGlobal("THEMIS_CACHE_LTL", "");
		if(CACHE_DIR.isEmpty()) return proceed(spec); 
		
		String key = "";
		if(spec instanceof SpecLTL) {
			key = String.valueOf(((SpecLTL) spec).getLTL().toString().hashCode());
		}
		if(key.isEmpty()) return proceed(spec); 
		
		File f = new File(CACHE_DIR, key);
		Serializer<Automata> serial = new Serializer<Automata>();
		if(f.exists()) {
			Automata aut = serial.unserialize(f);
			return new SpecAutomata(aut);
		}
		else
		{
			SpecAutomata res = proceed(spec);
			serial.serialize(res.getAut(), f);
			return res;
		}
	}
}
