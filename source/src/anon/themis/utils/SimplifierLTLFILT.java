package anon.themis.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import anon.themis.Env;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.BoolSym;
import anon.themis.parser.FrontExpressionParsers;
import anon.themis.utils.Opts;

/**
 * Simplifier that uses ltlfilt to simplify
 * ltlfilt has some memory leaks so we reset the process every once and a while
 * @see <a href="https://spot.lrde.epita.fr/ltlfilt.html">Spot's ltlfilt page</a>
 */
public class SimplifierLTLFILT implements Simplifier, AutoCloseable {
  private static int RENEW = Integer.parseInt(Opts.getGlobal("LTLFILT_RENEW", "1000"));
	Process p = null;
	PrintWriter  write;
	BufferedReader	scan;
  int simplifications = 0;
	
	public SimplifierLTLFILT() {
		boot();
	}
	private synchronized void boot()  {
    simplifications = 0;
		try {
			if(p != null) {
				close();
			}
			p = Convert.runProc(Env.SPOT_LTLFILT.getValue("ltlfilt"), new Object[] { "-r" });
			write = new PrintWriter(p.getOutputStream());
			scan  = new BufferedReader(new InputStreamReader(p.getInputStream()));
		} catch (Exception e) {
			throw new Error("Could not start simplifier: " + e.getLocalizedMessage());
		}
	}
	public synchronized BoolExpression simplify(BoolExpression e) {
		if(e instanceof BoolSym) return e;
    renew();		
		Map<String, BoolSym> syms = new HashMap<String, BoolSym>();
		BoolExpression expr 	  = Expressions.reformatBoolean(e, syms);
		String simplified		  = simplify(expr.toString());
		return	FrontExpressionParsers.parseStringBoolFILT(simplified, syms);
	}
	public synchronized String simplify(String e) {
    renew();
		write.println(e);
		write.flush();
		try {
			return scan.readLine();
		}
		catch(IOException ex) {
			throw new Error("Could not simplify: " + e);
		}
	}
  private void renew() {
    if(simplifications++ < RENEW) return;
    boot();
  }
	@Override
	public void close() throws Exception {
		write.close();
		p.destroy();
    p = null;
	}
}
