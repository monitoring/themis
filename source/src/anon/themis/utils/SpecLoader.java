/**
 * SpecLoader
 *
 * name <email>
 */
package anon.themis.utils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import anon.themis.monitoring.Specification;


public class SpecLoader {

	@SuppressWarnings("rawtypes")
	public static Map<String, Specification> loadSpec(File file) {
		Map<String, Specification> map = new HashMap<String, Specification>();
		try {
			SAXReader reader = new SAXReader();
	        Document document = reader.read(file);
	        
	        Element root = document.getRootElement();


	        for ( Iterator i = root.elementIterator( "specification" ); i.hasNext(); ) {
	            Element spec = (Element) i.next();
	            
	            String id 	= spec.attribute("id").getText();
	            String clsn = spec.attribute("class").getText();
	            
	            @SuppressWarnings("unchecked")
				Class<? extends Specification> cls = (Class<? extends Specification>)
	            		SpecLoader.class.getClassLoader().loadClass(clsn);
	            
	            Specification inst = cls.newInstance();
	            
	            Map<String, Method> methods = new HashMap<String, Method>();
	            
	            for(Method m : cls.getDeclaredMethods())
	            	methods.put(m.getName(), m);
	            
	            for ( Iterator v = spec.elementIterator(); v.hasNext(); ) {
	     
	            	Element el = (Element) v.next();

	                if(methods.containsKey(el.getName()))
	                	methods.get(el.getName()).invoke(inst, el.getText());

	            }
	            
	            map.put(id, inst);
	        }
	        
	        return map;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
		
	}
	
	
}
