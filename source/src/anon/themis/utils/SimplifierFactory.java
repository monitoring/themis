package anon.themis.utils;

public class SimplifierFactory {
	private static Simplifier DEFAULT = new SimplifierLTLFILT();
	
	/**
	 * Default Simplifier
	 * @return
	 */
	public static Simplifier getDefault() {
		return DEFAULT;
	}
}
