package anon.themis.utils;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.automata.Transition;
import anon.themis.automata.Verdict;
import anon.themis.automata.formats.LabelEvent;
import anon.themis.automata.formats.LabelExpression;
import anon.themis.expressions.Expression;
import anon.themis.expressions.ExpressionBinary;
import anon.themis.expressions.ExpressionSymbol;
import anon.themis.expressions.ExpressionUnary;
import anon.themis.expressions.bool.*;
import anon.themis.expressions.ltl.LTLExpression;
import anon.themis.inference.Memory;
import anon.themis.inference.Representation;
import anon.themis.inference.Rule;
import anon.themis.parser.FrontExpressionParsers;
import anon.themis.symbols.AtomEmpty;
import anon.themis.symbols.AtomNegation;
import anon.themis.symbols.AtomObligation;
import anon.themis.symbols.AtomString;

/**
 * Utility Functions for expressions
 * 
 */
public class Expressions {
	
	/**
	 * Parses a string into a boolean expression
	 * Grammar defined by grammar/Labels.g4
	 * @param expr
	 * @return
	 */
	public static BoolExpression parseBoolean(String expr)
	{
        return FrontExpressionParsers.parseStringExpression(expr);
	}
	/**
	 * Parses a string into a boolean expression
	 * Grammar defined by grammar/Labels.g4
	 * @param expr
	 * @return
	 */
	public static Event parseEvent(String expr)
	{
        return FrontExpressionParsers.parseStringEvent(expr);
	}
	public static LTLExpression parseLTL(String ltl)
	{
		return FrontExpressionParsers.parseStringLTL(ltl);
	}
	
	/**
	 * Generate obligations from a transition label that can be cast to an expression
	 * @param t
	 * @param timestamp
	 * @return
	 */
	public static BoolExpression fromTransition(Transition t, int timestamp)
	{
		if(t.getLabel() instanceof LabelExpression) {
			BoolExpression e = (BoolExpression) t.getLabel();
			return build(e, timestamp);
		}
		else if(t.getLabel() instanceof LabelEvent) {
			LabelEvent ev = (LabelEvent) t.getLabel();
			return build(ev.getEvent(), timestamp);
		}
		else
			throw new Error("Transition Label Not Supported");
	}
	
	/**
	 * Convert an event to a boolean expression
	 * Add obligations to timestamp
	 * @param e
	 * @param timestamp
	 * @return
	 */
	public static BoolExpression build(Event e, int timestamp)
	{
		
		Set<Atom> syms = e.getAtoms();
		Iterator<Atom> iter = syms.iterator();
		if(!iter.hasNext()) return new True();

		BoolExpression expr = build(iter.next(), timestamp);
		
		while(iter.hasNext())
			expr = new BoolAnd(expr, build(iter.next(), timestamp));
		
		return expr;
	}

	/**
	 * Add obligation to an existing atom
	 * @param a
	 * @param timestamp
	 * @return
	 */
	public static BoolExpression build(Atom a, int timestamp)
	{
		if(a instanceof AtomNegation) 
			return new BoolNot(build(((AtomNegation) a).getNegated(), timestamp));
		else 
			return new Atomic(new AtomObligation(a, timestamp));
	}
	/**
	 * Generate an obligation from an expression e on a given timestamp
	 * @param e			
	 * @param timestamp 
	 * @return
	 */
	public static BoolExpression build(BoolExpression e, int timestamp)
	{
		if(e instanceof LabelExpression)
			return build((BoolExpression) e.getOperands().get(0), timestamp);
		else if(e instanceof Atomic)
		{
			Atom a = (Atom) e.getOperands().get(0);
			Atom n = new AtomObligation(a, timestamp);
			return new Atomic(n);
		}
		else if(e instanceof True || e instanceof False)
		{
			return e;
		}
		else if(e instanceof BoolAnd)
		{
			return new BoolAnd(
						build((BoolExpression) e.getOperands().get(0), timestamp),
						build((BoolExpression) e.getOperands().get(1), timestamp)
					);
		}
		else if(e instanceof BoolOr)
		{
			return new BoolOr(
						build((BoolExpression) e.getOperands().get(0), timestamp),
						build((BoolExpression) e.getOperands().get(1), timestamp)
					);
		}
		else if(e instanceof BoolNot)
		{
			return new BoolNot(
						build((BoolExpression) e.getOperands().get(0), timestamp)
					);
		}
		else if(e instanceof EventExpr)
		{
			Event newev = new Event();
			Event ev = ((EventExpr) e).getEvent();
			for(Atom a : ev.getAtoms())
			{
				if(a instanceof AtomString)
					newev.addObservation(new AtomObligation((AtomString) a, timestamp));
				else
					newev.addObservation(a);
			}
			return new EventExpr(newev);
		}
		else
			return null;
	}
	public static BoolExpression reformatBoolean(BoolExpression expr, Map<String, BoolSym> symbols) {
		if(expr instanceof BoolBinary) {
			BoolExpression left =  ((BoolBinary) expr).getLeft();
			BoolExpression right = ((BoolBinary) expr).getRight();
			left 	= reformatBoolean(left, symbols);
			right 	= reformatBoolean(right, symbols);
			
			if(expr instanceof BoolAnd) return new BoolAnd(left, right);
			if(expr instanceof BoolOr)	return new BoolOr(left, right);
			else throw new Error("Could not Reformat: " + expr);
		}
		if(expr instanceof BoolNot)
			return new BoolNot(reformatBoolean(((BoolNot) expr).getOperand(), symbols));
		if(expr instanceof BoolSym) {
			if( expr instanceof Atomic) {
				Atom atom = ((Atomic) expr).getAtom();
				if(atom instanceof AtomObligation) {
					String sym = ((AtomObligation) atom).getAtom().toString();
					int t = ((AtomObligation) atom).getTimestamp();
					
					String key = "o_" + t + "_" + sym;
					symbols.put(key, (BoolSym) expr);
					return new Atomic(new AtomString(key));
				}
				else if(atom instanceof AtomEmpty) {
					symbols.put(atom.observe(), (BoolSym) expr);
					return expr;
				}
				else{
					return expr;
				}
			}
			else
				return expr;
		}
		else
			throw new Error("Could not Reformat: " + expr);
	}

	public static BoolExpression simplify(BoolExpression e) throws IOException, TimeoutException {
		return SimplifierFactory.getDefault().simplify(e);
	}
	/**
	 * Simplify and replace an expression
	 * @param e
	 * @param mem
	 * @return
	 */
	public static BoolExpression process(BoolExpression e, Memory<?> mem)
	{
		if(e instanceof Atomic || e instanceof EventExpr)
		{
			Verdict v 	= e.eval(mem);	
		
			if(v == Verdict.TRUE)
				return new True();
			else if(v == Verdict.FALSE)
				return new False();
			else
				return e;
		}
		else if(e instanceof True)
		{
			return e;
		}
		else if(e instanceof False)
		{
			return e;
		}
		else if(e instanceof BoolAnd)
		{
			BoolExpression left  = process((BoolExpression) e.getOperands().get(0), mem);
			if(left instanceof False)	return left;
			BoolExpression right = process((BoolExpression) e.getOperands().get(1), mem);
			if(right instanceof False)	return right;
			
			if(left  instanceof True) return right;
			if(right instanceof True) return left;
			
			return new BoolAnd(left, right);
		}
		else if(e instanceof BoolOr)
		{
			BoolExpression left  = process((BoolExpression) e.getOperands().get(0), mem);
			if(left instanceof True)	return left;
			BoolExpression right = process((BoolExpression) e.getOperands().get(1), mem);
			if(right instanceof True)	return right;
			
			if(left instanceof False)	return right;
			if(right instanceof False)	return left;
			
			return new BoolOr(left, right);
		}
		else if(e instanceof BoolNot)
		{
			BoolExpression inline  = process((BoolExpression) e.getOperands().get(0), mem);
			
			if(inline instanceof True)
				return new False();
			else if(inline instanceof False)
				return new True();
			else
				return new BoolNot(inline);
		}
		else
			return null;	
	}
	
	

	/**
	 * Find the earliest obligation in a Representation
	 * The stop specifies whether or not to stop after finding one in a rule
	 * (Prevents to lookup earliest obligations in the next set of rules)
	 * @param rep
	 * @param stop
	 * @return
	 */
	public static AtomObligation findEarliestObligation(Representation rep, boolean stop)
	{
		AtomObligation min = null;
		for(Rule r : rep.getRules().values())
		{
			AtomObligation res = findEarliestObligation(r, stop);
			if(res != null) {
				min = (min != null) &&  min.compareTo(res) <= 0 ? min : res;
				if(stop) break;
			}
		}
		return min;
	}
	
	/**
	 * Find earliest obligation in a Rule
	 * The stop specifies whether or not to stop after finding it for one state in the rule
	 * @param r
	 * @param stop
	 * @return
	 */
	public static AtomObligation findEarliestObligation(Rule r, boolean stop)
	{
		AtomObligation min = null;
		for(BoolExpression e : r.getPairs().values()) {
			AtomObligation res = findEarliestObligation(e);
			if(res != null) {
				min = (min != null) &&  min.compareTo(res) <= 0 ? min : res;
				if(stop) break;
			}
		}
		return min;
	}

	/**
	 * Returns the smallest (earliest) obligation in an expression
	 * Looks for AtomObligation and uses AtomObligation.compareTo
	 * @param e
	 * @return
	 */
	public static AtomObligation findEarliestObligation(BoolExpression e)
	{
		if(e instanceof Atomic)
		{
			Atom a = (Atom) e.getOperands().get(0);
			
			if(a instanceof AtomObligation)
			{
				if(a.isEmpty()) return null;
				return (AtomObligation) a; 
			}
			else
				return null;

		}
		else if(e instanceof True || e instanceof False)
		{
			return null;
		}
		else if(e instanceof EventExpr)
		{
			Event v = ((EventExpr) e).getEvent();
			Set<Atom> atoms = v.getAtoms();
			Iterator<Atom> iter = atoms.iterator();
			AtomObligation min = null;
			while(iter.hasNext())
			{
				Atom next = iter.next();
				if(next instanceof AtomObligation)
					min = (min != null) && (min.compareTo(next) <= 0) ? min : (AtomObligation) next;
					
			}
			return min;
		}
		else if(e instanceof BoolAnd || e instanceof BoolOr)
		{
			AtomObligation left   = findEarliestObligation((BoolExpression) e.getOperands().get(0));
			AtomObligation right  = findEarliestObligation((BoolExpression) e.getOperands().get(1));
			if(left == null)  return right;
			if(right == null) return left;
			
			return (left.compareTo(right) <= 0) ? left : right;
		}
		
		else if(e instanceof BoolNot)
		{
			return findEarliestObligation((BoolExpression) e.getOperands().get(0));
		}
		else
			return null;	
	}

	/**
	 * depth 		: depth of the expression
	 * expressions 	: number of operators
	 * leaves		: list of all leaf nodes
	 */
	public static class ExprSize {
		public int depth 		= 0;
		public int expressions	= 0;
		public List<ExpressionSymbol> leaves = new LinkedList<ExpressionSymbol>();
		public void updateDepth(int d) {
			depth = Math.max(depth, d);
		}
	}
	/**
	 * Returns information about an expression
	 * 
	 * @param expr
	 * @return
	 */
	public static ExprSize getLength(Expression expr) {
		ExprSize size = new ExprSize();
		updateLength(size, expr, 0);
		return size;
	}
	@SuppressWarnings("rawtypes")
	private static void updateLength(ExprSize size, Expression expr, int depth) {
		if(expr instanceof ExpressionBinary) {
			size.updateDepth(depth);
			updateLength(size, ((ExpressionBinary) expr).getLeft()  , depth + 1);
			updateLength(size, ((ExpressionBinary) expr).getRight() , depth + 1);
			
		}
		else if(expr instanceof ExpressionUnary) {
			size.updateDepth(depth);
			updateLength(size, ((ExpressionUnary) expr).getOperand() , depth + 1);
		}
		else if(expr instanceof ExpressionSymbol){
			size.leaves.add((ExpressionSymbol) expr);
		}
		else
			throw new Error("Expression not known: "+ expr + " " + expr.getClass().getSimpleName());
	}
}
