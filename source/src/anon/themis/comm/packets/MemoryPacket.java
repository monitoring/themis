package anon.themis.comm.packets;

import anon.themis.comm.protocol.Message;
import anon.themis.inference.Memory;

/**
 * A message containing a memory
 * 
 */
public class MemoryPacket extends Message {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("rawtypes")
	public Memory mem;
	

	/**
	 * Create an empty memory packet
	 */
	public MemoryPacket() {}
	
	/**
	 * Creates a new memory packet given a memory
	 * @param mem
	 */
	@SuppressWarnings("rawtypes")
	public MemoryPacket(Memory mem) {
		this.mem = mem;
	}
	public String toString() {
		return mem.toString();
	}

}
