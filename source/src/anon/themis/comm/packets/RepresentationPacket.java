package anon.themis.comm.packets;

import anon.themis.comm.protocol.Message;
import anon.themis.inference.Representation;

/**
 * Message containing a representation
 * 
 */
public class RepresentationPacket extends Message {
	private static final long serialVersionUID = 1L;
	
	public Representation repr;
	
	/**
	 * Create an empty message
	 */
	public RepresentationPacket() {}
	/**
	 * Create a new representation packet given a representation
	 * @param repr
	 */
	public RepresentationPacket(Representation repr) {
		this.repr = repr;
	}
	@Override
	public String toString() {
		return repr.toString();
	}

}
