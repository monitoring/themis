package anon.themis.comm.protocol;

import java.io.Serializable;

/**
 * A message that can be communicated over the monitor network
 * 
 */
@SuppressWarnings("serial")
public abstract class Message implements Serializable {}
