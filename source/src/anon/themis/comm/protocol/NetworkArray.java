package anon.themis.comm.protocol;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;

import anon.themis.monitoring.Monitor;

/**
 * The Monitor network 
 * This implementation used a buffered send/receive
 * That is messages will only be available after sync is called
 * Messages are stored in queues so at one timestamp all messages are sent
 * And all messages are received in a queue
 * 
 */
public class NetworkArray {
	Map<Integer, NetQueue> send;
	Map<Integer, NetQueue> recv;
	
	class NetQueue extends LinkedBlockingQueue<Message> {
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * Constructs a network array for the monitors
	 * This implementation constructs a fully connected network
	 * @param monitors
	 */
	public NetworkArray(Map<Integer, ? extends Monitor> monitors) {
		send = new HashMap<Integer, NetQueue>(monitors.size());
		recv = new HashMap<Integer, NetQueue>(monitors.size()); 
		for(Monitor mon : monitors.values()) {
			send.put(mon.getID(), new NetQueue());
			recv.put(mon.getID(), new NetQueue());
		}
	}
	
	/**
	 * Sends a message to id
	 * [Note: Messages are sent when sync is called]
	 * @param id
	 * @param message
	 */
	public void put(int id, Message message) 
	{
		send.get(id).add(message);
	}
	/**
	 * Retrieve a message sent to id
	 * @param index
	 * @return
	 */
	public Message get(int id) 
	{
		return get(id, true);
	}
	/**
	 * Return a message sent for id
	 * @param id
	 * @param consume
	 * @return
	 */
	public Message get(int id, boolean consume) 
	{
		if(!recv.containsKey(id)) return null;
		if(!consume)
			return recv.get(id).peek();
		else
			return recv.get(id).poll();
	}
	
	/**
	 * Clears the network of all messages
	 */
	public void reset() {
		for(NetQueue q : send.values()) q.clear();
		for(NetQueue q : recv.values()) q.clear();
	}
	
	/**
	 * Synchronize
	 * All messages that must be sent are enqueued 
	 * In the receive queue of their receivers
	 */
	public void sync() {
		for(Entry<Integer, NetQueue> entry : send.entrySet()) {
			NetQueue queue = entry.getValue();
			while(!queue.isEmpty())
				recv.get(entry.getKey()).add(queue.poll());
		}
	}

}
