package anon.themis.automata;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * The Event consists of a set of atoms
 * 
 */
public class Event implements Serializable {
	private Set<Atom> atoms;

	/**
	 * Initialize an event
	 */
	public Event() {
		atoms = new HashSet<Atom>();
	}
	/**
	 * Add an observation to the event
	 * @param ap
	 */
	public void addObservation(Atom ap)
	{
		atoms.add(ap);
	}
	/**
	 * Is an atom part of the event?
	 * @param a
	 * @return
	 */
	public boolean observed(Atom a)
	{
		return atoms.contains(a);
	}
	/**
	 * Return all atoms observed
	 * @return
	 */
	public Set<Atom> getAtoms() {
		return atoms;
	}
	/**
	 * Check whether it contains an empty atom
	 * @return
	 */
	public boolean hasEmpty() {
		for(Atom atom : atoms)
			if(atom.isEmpty())
				return true;
		return false;
	}
	/**
	 * Event has no observations?
	 * @return
	 */
	public boolean empty()
	{
		return atoms.isEmpty();
	}
	@Override
	public String toString() {
		if(empty()) return "[]";
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		Iterator<Atom> iter = atoms.iterator();
		sb.append(iter.next());
		while(iter.hasNext())
		{
			sb.append(" ");
			sb.append(iter.next().toString());
		}
		sb.append("]");
			
		return sb.toString();
	}
	/**
	 * Add all observations from another event
	 * @param ev
	 */
	public void merge(Event ev)
	{
		atoms.addAll(ev.atoms);
	}
	/**
	 * Is it equal to another event
	 * Set equality of observations
	 * @param obj
	 * @return
	 */
	public boolean equals(Event obj) {
		return atoms.equals(obj.atoms);
	}
}
