package anon.themis.automata;

import java.io.Serializable;

import anon.themis.symbols.AtomEmpty;

/**
 * The Atom represents an atomic symbol
 */
public interface Atom extends Serializable, Comparable<Atom> {
  /**
   * Return a string version of the atom
   * @return
   */
	String observe();
	
	/**
	 * Return a grouping object, so that atoms can be grouped with other atoms of the same object
	 * @return
	 */
	Object group();
	
	/**
	 * Denotes an atom representing an empty symbol
	 * @return
	 */
	boolean isEmpty();
	
	/**
	 * Compare the atom to another object (mostly another atom)
	 * @param atom
	 * @return
	 */
	boolean equals(Object atom);

	/**
	 * Returns the empty symbol
	 * @return
	 */
	public static Atom empty() {
		return new AtomEmpty();
	}
}
