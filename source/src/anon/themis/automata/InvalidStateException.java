package anon.themis.automata;

/**
 * Wrong state in an automaton
 * 
 */
@SuppressWarnings("serial")
public class InvalidStateException extends Exception {}
