package anon.themis.automata;

import java.io.Serializable;

/**
 * Automata Label
 * Used to select transitions that match an event
 */
public interface Label extends Serializable {
	boolean matches(Event e);
}
