package anon.themis.automata;

import java.io.Serializable;

/**
 * Automaton State
 * 
 */
@SuppressWarnings("serial")
public class State implements Serializable {
	private String name;
	
	/**
	 * Create an automaton state
	 * @param name Name of the state
	 */
	public State(String name)
	{
		this.name = name;
	}

	public String toString() { return name; }
}
