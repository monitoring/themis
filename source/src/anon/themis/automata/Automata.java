package anon.themis.automata;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Automata data struct
 * 
 */
public class Automata implements Serializable {

	private static final long serialVersionUID = 1L;
	//Current state
	State	 				cur	 	= null;
	//Initial State
	State 					init 	= null;
	//Labelling function
	Map<State, Verdict> 	states 	= null;
	//Delta
	Map<State, List<Transition>> delta = null;
	
	/**
	 * Create a new Moore automaton
	 * @param init Initial state
	 * @param states Mapping from states to verdict
	 * @param delta Specify the transition function
	 */
	public Automata(State init, Map<State, Verdict> states, Map<State, List<Transition>> delta) {
		this.init 	= init;
		this.states = states;
		this.delta 	= delta;
		cur			= this.init;
	}
	
	/**
	 * Go back to initial state
	 */
	public void reset()
	{
		cur = init;
	}
	/**
	 * Return the current state the automaton is in
	 * @return
	 */
	public State getCurrentState() {
		return cur;
	}
	/**
	 * Return the initial State
	 * @return
	 */
	public State getInitialState() {
		return init;
	}

	/**
	 * Return all states and their verdicts
	 * @return
	 */
	public Map<State, Verdict> getStates() {
		return states;
	}
	/**
	 * Return the transition function
	 * @return
	 */
	public Map<State, List<Transition>> getDelta() {
		return delta;
	}
	
	/**
	 * Move on an event
	 * Returns the state reached or null if sink
	 * An empty event is associated with an event containing an empty atom
	 * @param e The input event
	 * @return
	 */
	public State move(Event e)
	{
		//Normalize: 
		// an AtomString.empty() labels a transition on empty events
		Event e_normal = e;
		if(e.empty())
		{
			e_normal = new Event();
			e_normal.addObservation(Atom.empty());
		}
		for(Transition t : delta.get(cur))
		{
			if(t.possible(e_normal))
			{
				cur = t.getTo();
				return cur;
			}
		}
		return null;
	}
	/**
	 * Returns verdict associated with a state
	 * @param s
	 * @return
	 * @throws InvalidStateException
	 */
	public Verdict getVerdict(State s) throws InvalidStateException
	{
		if(states.containsKey(s))
			return states.get(s);
		else
		{
			throw new InvalidStateException();
		}
	}
	/**
	 * Returns current verdict
	 * Returns the verdict on the current state of the automaton
	 * @return
	 */
	public Verdict getVerdict()
	{
		return states.get(cur);
	}

}
