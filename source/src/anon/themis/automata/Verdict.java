package anon.themis.automata;

/**
 * Possible Verdict Domain
 * 
 */
public enum Verdict {
	TRUE, FALSE, NA;
	
	/**
	 * Return verdicts associated with boolean
	 * @param b
	 * @return
	 */
	public static Verdict fromBoolean(boolean b)
	{
		if(b) return TRUE;
		else  return FALSE;
	}
	/**
	 * Combine verdicts
	 * This method is used to merge two verdicts that could be at the same time
	 * Basically an NA and anything cannot be merged
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static Verdict merge(Verdict v1, Verdict v2)
	{
		if(v1 == TRUE || v2 == TRUE) 
			return TRUE;
		else if(v1 == FALSE || v2 == FALSE)
			return FALSE;
		else
			return NA;
	}
	
	/**
	 * Check whether or not the verdict is final
	 * A subset of the verdicts is usually final, and allows monitoring to terminate
	 * @return
	 */
	public boolean isFinal() {
		return Verdict.isFinal(this);
	}
	/**
	* Check whether or not the verdict is final
    * A subset of the verdicts is usually final, and allows monitoring to terminate
	* @param v
	* @return
	*/
	public static boolean isFinal(Verdict v)
	{
		return v != null && v != NA;
	}
}
