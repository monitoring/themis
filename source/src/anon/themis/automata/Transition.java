package anon.themis.automata;

import java.io.Serializable;

/**
 * Automaton Transition
 * 
 */
public class Transition implements Serializable {
	State from;
	State to;
	Label label;
	
	/**
	 * Create a transition from a state to another state given on a label
	 * @param from	
	 * @param to	
	 * @param label 
	 */
	public Transition(State from, State to, Label label) {
		this.from = from;
		this.to = to;
		this.label = label;
	}
	/**
	 * Return the state the transition is outbound
	 * @return
	 */
	public State getFrom() {
		return from;
	}
	/**
	 * Return the state the transition is inbound
	 * @return
	 */
	public State getTo() {
		return to;
	}
	/**
	 * Return the label
	 * @return
	 */
	public Label getLabel() {
		return label;
	}
	
	/**
	 * Is the transition possible for a given event?
	 * @param e
	 * @return
	 */
	public boolean possible(Event e)
	{
		return label.matches(e);
	}
	
	@Override
	public String toString() {
		return from + " -> " + to;
	}
}
