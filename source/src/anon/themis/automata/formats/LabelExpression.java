package anon.themis.automata.formats;

import java.util.LinkedList;
import java.util.List;

import anon.themis.automata.Event;
import anon.themis.automata.Label;
import anon.themis.automata.Verdict;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.inference.Memory;

/**
 * Transition Label: Boolean Expression
 * To check if a transition is possible, it evaluates a boolean expression
 */
@SuppressWarnings("serial")
public class LabelExpression implements Label, BoolExpression {

	private BoolExpression label;
	
	/**
	 * Initialize a transition label with a boolean expression
	 * @param label
	 */
	public LabelExpression(BoolExpression label) {
		this.label = label;
	}
	
	@Override
	public boolean matches(Event e) {
		return label.eval(e);
	}
	@Override
	public String toString() {
		
		return label.toString();
	}

	@Override
	public Boolean eval(Event e) {
		return label.eval(e);
	}

	@SuppressWarnings("rawtypes")
    @Override
	public Verdict eval(Memory m) {
		return label.eval(m);
	}

	@Override
	public List<Object> getOperands() {
		List<Object> l = new LinkedList<Object>();
		l.add(label);
		return l;
	}


}
