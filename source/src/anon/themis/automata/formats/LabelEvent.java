package anon.themis.automata.formats;

import anon.themis.automata.Event;
import anon.themis.automata.Label;

/**
 * Transition Label: Event
 * To check if a transition is possible, the events must match
 * 
 */
public class LabelEvent implements Label  {

	private Event event;
	public LabelEvent(Event event) {
		this.event = event;
	}
	
	@Override
	public boolean matches(Event e) {
		return event.equals(e);
	}
	@Override
	public String toString() {
		return event.toString();
	}
	
	public Event getEvent() {
		return event;
	}


}
