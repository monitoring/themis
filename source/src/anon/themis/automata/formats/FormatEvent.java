package anon.themis.automata.formats;

import anon.themis.automata.Label;
import anon.themis.utils.Expressions;

/**
 * Formats the transition text as Events
 * 
 */
public class FormatEvent implements TransitionFormatter {

	@Override
	public Label getLabel(String text) {
		return new LabelEvent(Expressions.parseEvent(text));
	}

}
