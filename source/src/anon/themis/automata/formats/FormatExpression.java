package anon.themis.automata.formats;

import anon.themis.automata.Label;
import anon.themis.utils.Expressions;

/**
 * Formats the text as a boolean Expression
 * 
 */
public class FormatExpression implements TransitionFormatter {

	@Override
	public Label getLabel(String text) {
		return new LabelExpression(Expressions.parseBoolean(text));
	}

}
