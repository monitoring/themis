package anon.themis.automata.formats;

import anon.themis.automata.Label;

/**
 * Parses a text to generate a transition Label
 * Used when parsing automata
 * 
 */
public interface TransitionFormatter {
	Label getLabel(String text);
}
