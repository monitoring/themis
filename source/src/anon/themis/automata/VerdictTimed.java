package anon.themis.automata;

/**
 * A pair of Verdict and Timestamp
 * To denote a verdict for (or at) a given timestamp
 */
public class VerdictTimed {
	private Verdict verdict;
	private int	timestamp;
	
	
	/**
	 * Initialize a timed verdict with a verdict and a timestamp
	 * @param verdict
	 * @param timestamp
	 */
	public VerdictTimed(Verdict verdict, int timestamp) {
		this.verdict = verdict;
		this.timestamp = timestamp;
	}
	
	/**
	 * Returns the verdict
	 * @return
	 */
	public Verdict getVerdict() {
		return verdict;
	}
	/**
	 * Change the verdict
	 * @param verdict
	 */
	public void setVerdict(Verdict verdict) {
		this.verdict = verdict;
	}
	/**
	 * Return the timestamp
	 * @return
	 */
	public int getTimestamp() {
		return timestamp;
	}
	/**
	 * Change Timestamp
	 * @param timestamp
	 */
	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * Check if the verdict is final
	 * @return
	 */
	public boolean isFinal() {
		return Verdict.isFinal(verdict);
	}
	
	@Override
	public String toString() {
		return verdict + "@" + timestamp;
	}
}
