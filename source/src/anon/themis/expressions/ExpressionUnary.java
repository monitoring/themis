package anon.themis.expressions;

public interface ExpressionUnary<T extends Expression> {
	/**
	 * Return subexpression
	 * @return
	 */
	T getOperand();
}
