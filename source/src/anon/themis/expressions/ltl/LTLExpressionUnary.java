package anon.themis.expressions.ltl;

import anon.themis.expressions.ExpressionUnary;

public interface LTLExpressionUnary extends LTLExpression, ExpressionUnary<LTLExpression> {
	LTLExpression getOperand();
}
