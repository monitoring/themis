package anon.themis.expressions.ltl;

import anon.themis.expressions.ExpressionSymbol;

public class LTLSymbol  implements LTLExpression, ExpressionSymbol{
	private String sym;
	public LTLSymbol(String sym) {
		this.sym = sym;
	}
	public String getSym() {
		return sym;
	}
	public void setSym(String sym) {
		this.sym = sym;
	}
	@Override
	public String toString() {
		return sym;
	}
	
}
