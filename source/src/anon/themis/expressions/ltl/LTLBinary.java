package anon.themis.expressions.ltl;


public class LTLBinary extends LTLExpressionBase implements LTLExpressionBinary {

	public LTLBinary(LTLOperator op, LTLExpression left, LTLExpression right) {
		super(op, (LTLExpression[]) null);
		exprs = new LTLExpression[2];
		exprs[0] = left;
		exprs[1] = right;
	}

	public void setLeft(LTLExpression expr) {
		exprs[0] = expr;
	}
	public void setRight(LTLExpression expr) {
		exprs[1] = expr;
	}
	@Override
	public LTLExpression getLeft() {
		return exprs[0];
	}
	@Override
	public LTLExpression getRight() {
		return exprs[1];
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(")
			.append(exprs[0].toString())
			.append(") " + op.toString() + " (")
			.append(exprs[1].toString())
			.append(")");
		return sb.toString();
	}
}
