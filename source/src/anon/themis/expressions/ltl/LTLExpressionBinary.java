package anon.themis.expressions.ltl;

import anon.themis.expressions.ExpressionBinary;

public interface LTLExpressionBinary extends LTLExpression, ExpressionBinary<LTLExpression> {

}
