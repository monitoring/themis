package anon.themis.expressions.bool;

import java.util.LinkedList;
import java.util.List;

import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.inference.Memory;

/**
 * A Boolean symbol representing an Event
 * 
 */
@SuppressWarnings("serial")
public class EventExpr implements BoolSym {
	private Event 	event;
	public EventExpr(Event event)
	{
		this.event = event;
	}
	
	@Override
	public Boolean eval(Event e) {
		return event.equals(e);
	}
	
	@Override
	public String toString() {
		return event.toString();
	}

	@SuppressWarnings("rawtypes")
  @Override
	public Verdict eval(Memory m) {
		return m.get(event);
	}

	public Event getEvent() {
		return event;
	}
	@Override
	public List<Object> getOperands() {
		List<Object> l = new LinkedList<Object>();
		l.add(event);
		return l;
	}
}
