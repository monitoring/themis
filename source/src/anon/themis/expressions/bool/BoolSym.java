package anon.themis.expressions.bool;

import anon.themis.expressions.ExpressionSymbol;

public interface BoolSym extends BoolExpression, ExpressionSymbol {

}
