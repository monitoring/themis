package anon.themis.expressions.bool;

import java.util.LinkedList;
import java.util.List;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.inference.Memory;

/**
 * A Boolean symbol that contains an Atom
 * 
 */
@SuppressWarnings("serial")
public class Atomic implements BoolSym {
	private Atom 	atom;
	public Atomic(Atom atom)
	{
		this.atom = atom;
	}
	
	@Override
	public Boolean eval(Event e) {
		return e.observed(atom);
	}
	
	@Override
	public String toString() {
		return atom.toString();
	}

	@SuppressWarnings("rawtypes")
  @Override
	public Verdict eval(Memory m) {
		return m.get(atom);
	}

	public Atom getAtom() 
	{
		return atom;
	}
	@Override
	public List<Object> getOperands() {
		List<Object> l = new LinkedList<Object>();
		l.add(atom);
		return l;
	}
}
