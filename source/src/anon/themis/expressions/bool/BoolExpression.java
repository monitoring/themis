package anon.themis.expressions.bool;

import java.io.Serializable;
import java.util.List;

import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.expressions.Expression;
import anon.themis.inference.Memory;

public interface BoolExpression extends Expression, Serializable {
	Boolean eval(Event e); 
	@SuppressWarnings("rawtypes")
	Verdict eval(Memory m);
	List<Object> getOperands();
}
