package anon.themis.expressions.bool;

import java.util.List;

import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.inference.Memory;

@SuppressWarnings("serial")
public class False implements BoolSym {
	@Override
	public Boolean eval(Event e) {
		return false;
	}
	
	@Override
	public String toString() {
		return "false";
	}

	@SuppressWarnings("rawtypes")
  @Override
	public Verdict eval(Memory m) {
		return Verdict.FALSE;
	}

	@Override
	public List<Object> getOperands() {
		return null;
	}


}
