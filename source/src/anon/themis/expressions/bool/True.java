package anon.themis.expressions.bool;

import java.util.List;

import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.inference.Memory;

@SuppressWarnings("serial")
public class True implements BoolSym {
	@Override
	public Boolean eval(Event e) {
		return true;
	}
	
	@Override
	public String toString() {
		return "true";
	}

	@SuppressWarnings("rawtypes")
  @Override
	public Verdict eval(Memory m) {
		return Verdict.TRUE;
	}

	@Override
	public List<Object> getOperands() {
		return null;
	}


}
