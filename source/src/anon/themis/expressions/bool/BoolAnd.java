package anon.themis.expressions.bool;

import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.inference.Memory;

public class BoolAnd extends BoolBinary {
	
	private static final long serialVersionUID = 1L;
	
	public BoolAnd(BoolExpression a, BoolExpression b)
	{
		super(a,b);
		operator = "&&";
	}
	
	public Boolean eval(Event e) {
		return a.eval(e) && b.eval(e);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public Verdict eval(Memory m) {
		Verdict v1 = a.eval(m);
		if(v1 ==  Verdict.NA)	return Verdict.NA;
		Verdict v2 = b.eval(m);
		if(v2 == Verdict.NA) 	return Verdict.NA;
		
		if( (v1 == v2) && (v1 == Verdict.TRUE))	
			return Verdict.TRUE;
		else
			return Verdict.FALSE;
		
	}
}
