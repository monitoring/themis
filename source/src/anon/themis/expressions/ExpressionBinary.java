package anon.themis.expressions;

public interface ExpressionBinary<T extends Expression> {
	/**
	 * Return left sub-expression
	 * @return
	 */
	T getLeft();
	/**
	 * Return right sub-expression
	 * @return
	 */
	T getRight();
}
