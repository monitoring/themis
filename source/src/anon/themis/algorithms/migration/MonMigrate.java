package anon.themis.algorithms.migration;

import anon.themis.automata.Atom;
import anon.themis.automata.Automata;
import anon.themis.automata.VerdictTimed;
import anon.themis.comm.packets.RepresentationPacket;
import anon.themis.comm.protocol.Message;
import anon.themis.inference.Memory;
import anon.themis.inference.MemoryAtoms;
import anon.themis.inference.Representation;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.symbols.AtomObligation;
import anon.themis.utils.Expressions;
import anon.themis.utils.Simplifier;
import anon.themis.utils.SimplifierFactory;

/**
 * Migration Monitor
 * 
 */
public class MonMigrate extends GeneralMonitor {
	protected Automata spec;
	protected Representation autRep;
	protected Memory<Atom> m;
	protected boolean isMonitoring  = false;
	protected Simplifier simplifier = SimplifierFactory.getDefault();

	public MonMigrate(Integer id) {
		super(id);
	}
	//Generate an automaton from the given Specification
	public void setup() {
		spec 	= ((SpecAutomata) getConfig().getSpec().get("root")).getAut();
		m 		= new MemoryAtoms();
	}
	@Override
	public void reset() {
		m.clear();
		autRep = new Representation(spec, simplifier);
		checkStart();
	}
	
	protected void checkStart() {
		//Move one to determine if we should start monitoring
		autRep.tick();
		
		//Find next obligation
		AtomObligation early = Expressions.findEarliestObligation(autRep, true);
		//Is the next obligation mine to monitor?
		isMonitoring = (getConfig().getMapper().monitorFor(early) == getID());
		
		//Undo our move
		autRep = autRep.slice(0, 0);
	}
	
	//Get the next monitor to send to
	public int getNext() {
		return getConfig().getMapper()
				.monitorFor(Expressions.findEarliestObligation(autRep, true));
	}
	@Override
	public void communicate() {}
	
	@Override
	public void monitor(int t, Memory<Atom> observations) throws ReportVerdict, ExceptionStopMonitoring {
		m.merge(observations);

		//Received formula? Start monitoring if its not
		if(receive()) isMonitoring = true;
		
		//Are we monitoring?
		if(isMonitoring)
		{
			//Update one timestamp ahead if we observe something, otherwise trace over
			if(!observations.isEmpty())
				autRep.tick();

			//Update the representation
			boolean b	= autRep.update(m, -1);
			
			//Find Next
			int next	= getNext();
			
			//Verdicts Updated
			if(b)
			{
				VerdictTimed v = autRep.scanVerdict();
				if(v.isFinal())
					throw new ReportVerdict(v.getVerdict(), t);
				autRep.dropResolved();
			}

			//Have to Forward
			if(next != getID())
			{
				//Pack content from last known state in the automaton
				Representation toSend = autRep.sliceLive();
				send(next, new RepresentationPacket(toSend));
				//Stop monitoring
				isMonitoring = false;
			}
			
		}
	}

	/**
	 * Check the receipt of representations from other monitors
	 * @return
	 */
	protected boolean receive() {
		Message m;
		boolean result = false;
		Representation repr = null;
		while((m = recv()) != null) {
			result = true;
			RepresentationPacket packet = (RepresentationPacket) m;
			if(repr != null)
				repr.mergeForward(packet.repr);
			else
				repr = packet.repr;
		}
		if(repr != null) {
			if(isMonitoring)
				autRep.mergeForward(repr);
			else
				autRep = repr;
		}
		return  result;
	}
}
