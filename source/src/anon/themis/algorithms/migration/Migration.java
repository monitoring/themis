package anon.themis.algorithms.migration;

import anon.themis.monitoring.Monitor;


/**
 * Migration Monitoring Algorithm
 * 
 */
public class Migration extends MigrationAlg<MonMigrate> {
	@Override
	protected Class<? extends MonMigrate> getMonitorType() {
		return MonMigrate.class;
	}
}
