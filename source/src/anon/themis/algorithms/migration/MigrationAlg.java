package anon.themis.algorithms.migration;

import java.util.HashMap;
import java.util.Map;

import anon.themis.expressions.ltl.LTLExpression;
import anon.themis.monitoring.Component;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MultiMonitorAlgorithm;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.monitoring.SpecLTL;
import anon.themis.monitoring.Specification;
import anon.themis.utils.Convert;

/**
 * Migration Monitoring Algorithm
 * 
 */
public abstract class MigrationAlg<T extends Monitor> extends MultiMonitorAlgorithm<T> {
	

	public MigrationAlg() {
		super();
	}
	public MigrationAlg(MonitorSettings config) {
		super(config);
	}
	@Override
	protected Map<Integer, ? extends Monitor> setup() 
	{

		//Generate the specification
		config.getSpec().put("root", 
				Convert.makeAutomataSpec(config.getSpec().get("root")));

		int c = config.getComponentsSize();
		Map<Integer, T> mons = new HashMap<Integer, T>(c);

		//Generate one monitor per component
		Integer i = 0;
		for(Component comp : config.getComponents()) {
			
			T mon = newMonitor(i);
			attachMonitor(comp, mon);
			mons.put(i, mon);

			i++;
		}
		
		return mons;
	}
	
	@Override
	public void report(ReportVerdict v) {
		System.out.println("[Verdict] " + v);
	}
	
	@Override
	public void abort(ExceptionStopMonitoring error) {
		System.err.println("[Abort] " + error.getError());
	}
	
}
