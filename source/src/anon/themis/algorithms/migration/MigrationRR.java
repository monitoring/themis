package anon.themis.algorithms.migration;


/**
 * Migration Monitoring Algorithm
 * 
 */
public class MigrationRR extends MigrationAlg<MonMigrateRR> {
	@Override
	protected Class<? extends MonMigrateRR> getMonitorType() {
		return MonMigrateRR.class;
	}
	
}
