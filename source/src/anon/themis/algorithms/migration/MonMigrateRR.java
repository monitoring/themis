package anon.themis.algorithms.migration;


/**
 * Migration Monitor with Round Robin swapping
 * 
 */
public class MonMigrateRR extends MonMigrate {

	public MonMigrateRR(Integer id) {
		super(id);
	}
	
	@Override
	public int getNext() {
		return (getID() + 1) % getConfig().getComponentsSize();
	}
}
