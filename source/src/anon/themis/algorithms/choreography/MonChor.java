package anon.themis.algorithms.choreography;

import java.util.HashSet;
import java.util.Set;

import anon.themis.automata.Atom;
import anon.themis.automata.Automata;
import anon.themis.automata.Verdict;
import anon.themis.automata.VerdictTimed;
import anon.themis.comm.protocol.Message;
import anon.themis.expressions.ltl.LTLExpression;
import anon.themis.inference.Memory;
import anon.themis.inference.MemoryAtoms;
import anon.themis.inference.Representation;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.monitoring.SpecLTL;
import anon.themis.symbols.AtomObligation;
import anon.themis.symbols.AtomString;
import anon.themis.utils.Convert;
import anon.themis.utils.SimplifierFactory;

/**
 * Choreography Monitor
 * @author Tony Hokayem <hokayem.tony@gmail.com>
 */
public class MonChor extends GeneralMonitor {
  //Should the monitor respawn
	private boolean respawn 	   = false;
	//Monitors to inform of verdict
	private Set<Integer> ref 	   = new HashSet<Integer>();
	//Monitors to wait on verdict from
	private Set<Integer> coref 	 = new HashSet<Integer>();
	//Monitors no longer requesting verdict information
	//If kill = ref then this monitor is killed
	private Set<Integer> kill	   = new HashSet<Integer>();
	//Specification to monitor
	private SpecLTL	spec		     = null;
	//Automaton of specification
	private Automata aut		     = null;
	//Inference rules for the automaton
	private Representation repr  = null;
	//Memory
	private Memory<Atom> mem	   = new MemoryAtoms();
	//Set false when killed
	private boolean isMonitoring = true;
	//Which timestamp am I monitoring for?
	private int monTimestamp	   = 1;
	//What is the window of monitoring
	//Set by the algorithm's setup to network depth
	private int window			     = 1;
	
	public MonChor(Integer id) {
		super(id);
	}
	
	/**
	 * Set the window to monitor
	 * This is not used for the moment but can be used for optimizations
	 * @param window
	 */
	public void setWindow(int window) {
		if(window < 1) return;
		this.window = window;
	}
	/**
	 * Get the window depth
	 * @return
	 */
	public int getWindow() {
		return window;
	}
	
	@Override
	public void reset() {
		isMonitoring = true;
		monTimestamp = 1;
		repr = new Representation(aut, SimplifierFactory.getDefault());
		mem.clear();
		kill.clear();
		repr.tick();
	}
	public Representation getRepresentation() {
		return repr;
	}
	public Automata getAutomata() {
		return aut;
	}
	public void setFormula(LTLExpression formula) {
		spec	= new SpecLTL(formula);
		aut 	= Convert.makeAutomataSpec(spec).getAut();
	}
	
	public void setRespawn(boolean respawn) {
		this.respawn = respawn;
	}
	public boolean isRespawn() {
		return respawn;
	}
	public Set<Integer> getRef() {
		return ref;
	}
	public Set<Integer> getCoref() {
		return coref;
	}
	
	@Override
	public void setup() {
	}
	
	@Override
	public void monitor(int t, Memory<Atom> observations) 
	throws ReportVerdict, ExceptionStopMonitoring 
	{
	  //Killed?
		if(!isMonitoring) return;
		
		//We keep only 1 processing state 
		//(the first is true and denotes the initial/resolved state)
		if(repr.size() < 2) {
			repr.tick();
		}
		
		//Observe
		mem.merge(observations);
		
		Message m;
		//Process Incoming Messages
		while((m = recv()) != null) {
			//Handle Kill - Killed do not Respawn
			if(m instanceof MessageKill) {
				int from = ((MessageKill) m).getFrom();
				kill.add(from);
				//Am I still needed?
				//More than one monitor may require my verdict
				if(kill.equals(ref))
					die();
				return;
			}
			//Handle Verdicts
			if(m instanceof MessageVerdict) {
				
				MessageVerdict v = (MessageVerdict) m;
				AtomString atom  = LTLMonRef.generateAtom(v.getFrom());
				//Update memory with the verdict for a given timestamp
				mem.add(new AtomObligation(atom, v.getTimestamp()), 
					v.getVerdict() == Verdict.TRUE);
			}
		}
		
		//Attempt to resolve
		boolean updated = repr.update(mem, -1);
		
		//Resolved
		while(updated) {
			//Get Earliest Verdict
			VerdictTimed v = repr.scanVerdict();
			
			//Verdict Reached
			if(v.isFinal()) {
				//Root node : Report final verdict
				if(ref.isEmpty())
					throw new ReportVerdict(v.getVerdict(), t);
				//Not root
				else {
				  //Inform all my ref
					for(Integer id : ref) {
					  //Unless they sent me a kill
						if(kill.contains(id)) continue;
						send(id, new MessageVerdict(v.getVerdict(), monTimestamp, getID()));
					}
					
					//Respawn or die
					if(isRespawn())	respawn();
					else 			die();
				}
				
			}
			//Cleanup resolved states
			repr.dropResolved();
			//Keep going so long as I havent reached the timestamp
			if(repr.getEnd() < t && repr.size() < 2) {
				repr.tick();
			}
			//Attempt to resolve again
			updated = repr.update(mem, -1);
		}
	}

	/**
	 * Respawn to monitor the property monTimestamp + 1
	 * @param timestamp
	 * @param t
	 */
	private void respawn() {
		isMonitoring = true;
		repr.reset(monTimestamp);
		monTimestamp++;
		repr.tick();
	}
	/**
	 * Die and send appropriate Kill messages
	 */
	private void die() {
		isMonitoring = false;
		mem.clear();
		for(Integer ref : coref) 
			send(ref, new MessageKill(getID()));
	}
	
	@Override
	public void communicate() {}

	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(respawn ? "(R) " : "").append(getID())
		  .append(" , -> ").append(ref.toString())
		  .append(" , <- ").append(coref.toString())
		  .append(" , ").append(spec.getLTL().toString())
		  .append(" ]");
		return sb.toString();
	}
}
