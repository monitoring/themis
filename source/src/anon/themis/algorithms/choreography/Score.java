package anon.themis.algorithms.choreography;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import anon.themis.expressions.ltl.LTLBinary;
import anon.themis.expressions.ltl.LTLExpression;
import anon.themis.expressions.ltl.LTLSymbol;
import anon.themis.expressions.ltl.LTLUnary;
import anon.themis.monitoring.SymbolMapper;
import anon.themis.symbols.AtomString;

/**
 * Computes a score tree to be associated with an LTL formula tree
 * 
 */
public class Score {

	public Map<String, Double> score;
	public String comp = null;
	public Score[] subtrees = null;
	public Score() {
		score = new HashMap<String, Double>();
	}
	public void addScores(Score f2) {
		for(Entry<String, Double> entry : f2.score.entrySet())
		{
			String comp = entry.getKey();
			Double w	= entry.getValue();
			
			if(score.containsKey(comp))
				w += score.get(comp);
			score.put(comp, w);
		}
	}
	public void addSubtrees(Score... ltl) {
		subtrees = ltl;
	}
	public void recompute() {
		Double max 	= 0d;
		for(Entry<String, Double> entry : score.entrySet())
		{
			if(entry.getValue() > max)
			{
				max 	= entry.getValue();
				comp 	= entry.getKey();
			}
		}
	}
	
	/**
	 * Computes a score tree for a formula
	 * A mapper is required to map an AP -> Component
	 * @param formula
	 * @param mapper
	 * @return
	 */
	public static Score getScoreTree(LTLExpression formula, SymbolMapper mapper) {
		if(formula instanceof LTLSymbol) {
			AtomString atom		= new AtomString(((LTLSymbol) formula).getSym());
			String component 	= mapper.componentFor(atom);
			Score re 		= new Score();
			re.score.put(component, 1d);
			re.recompute();
			return re;
		}
		else if(formula instanceof LTLUnary) {
			Score sub  = getScoreTree(((LTLUnary) formula).getOperand(), mapper);
			Score root = new Score();
			root.addScores(sub);
			root.addSubtrees(sub);
			root.recompute();
			return root;
		}
		else if(formula instanceof LTLBinary) {
			Score left 	= getScoreTree((LTLExpression) ((LTLBinary) formula).getLeft(), mapper);
			Score right = getScoreTree((LTLExpression) ((LTLBinary) formula).getRight(), mapper);
			Score root  = new Score();
			root.addScores(left);
			root.addScores(right);
			root.addSubtrees(left, right);
			root.recompute();
			return root;
		}
		else
			throw new Error("[Not Supported] LTLExpression : " 
						+ formula.getClass().getSimpleName());
	}
}
