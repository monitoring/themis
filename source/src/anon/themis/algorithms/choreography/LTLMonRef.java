package anon.themis.algorithms.choreography;

import anon.themis.expressions.ltl.LTLExpression;
import anon.themis.symbols.AtomString;

/**
 * LTL Symbol to denote a reference to a monitor
 * The prefix is used to encode the symbol so that it can be passed ot the simplifier
 */
public class LTLMonRef implements LTLExpression {
	public static final String MON_PREFIX = "m_";
	private Integer id;
	/**
	 * Generate a reference to the monitor with the given ID
	 * @param id
	 */
	public LTLMonRef(int id) {
		this.id = id;
	}
	/**
	 * Return the ID of the monitor referenced
	 * @return
	 */
	public Integer getID() {
		return id;
	}
	@Override
	public String toString() {
		return MON_PREFIX + id.toString();
	}
	
	/**
	 * Generate the atomic string from the reference
	 * Used to pass to simplified and read it back from simplifier
	 * This will be the key that identifies it
	 * (Make sure it is different from normal atoms)
	 * @param id
	 * @return
	 */
	public static AtomString generateAtom(int id) {
		return new AtomString(MON_PREFIX + id);
	}
}
