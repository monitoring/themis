package anon.themis.algorithms.choreography;

import anon.themis.comm.protocol.Message;

/**
 * Message sent to kill a monitor
 */
public class MessageKill extends Message {
  
	private static final long serialVersionUID = 1L;
	private int from;
	
	public MessageKill(int from) {
		this.from = from;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	

}
