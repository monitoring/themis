package anon.themis.algorithms.choreography;

import anon.themis.automata.Verdict;
import anon.themis.automata.VerdictTimed;
import anon.themis.comm.protocol.Message;

/**
 * Message to announce verdict for a given timestamp
 * 
 */
public class MessageVerdict extends Message {
	private static final long serialVersionUID = 1L;
	
	private VerdictTimed verdict;
	private int from;
	
	public MessageVerdict(Verdict v, int time, int from) {
		setVerdict(new VerdictTimed(v, time));
		setFrom(from);
	}
	public MessageVerdict(VerdictTimed v, int from) {
		setVerdict(v);
		setFrom(from);
	}
	
	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public Verdict getVerdict() {
		return verdict.getVerdict();
	}
	public void setVerdict(VerdictTimed verdict) {
		this.verdict = (verdict);
	}
	public void setVerdict(Verdict verdict) {
		this.verdict.setVerdict(verdict);
	}
	public int getTimestamp() {
		return verdict.getTimestamp();
	}
	
	@Override
	public String toString() {
		return "<" + from + ", " + verdict.toString() + ">";
	}
}
