package anon.themis.algorithms.orchestration;

import anon.themis.automata.Atom;
import anon.themis.comm.packets.MemoryPacket;
import anon.themis.inference.Memory;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;

/**
 * Slave Monitor for Orchestration
 * Forwards observation to the main monitor
 */
public class MonOrchSlave extends GeneralMonitor {

	public MonOrchSlave(Integer id) {
		super(id);
	}
	@Override
	public void setup() {}

	@Override
	public void reset() {
		
	}
	@Override
	public void monitor(int t, Memory<Atom> observations) 
	throws ReportVerdict, ExceptionStopMonitoring 
	{
	  //Main monitor is ID = 0
		send(0, new MemoryPacket(observations));
	}

	@Override
	public void communicate() {}

}
