package anon.themis.algorithms.orchestration;

import anon.themis.automata.Atom;
import anon.themis.automata.Automata;
import anon.themis.automata.VerdictTimed;
import anon.themis.comm.packets.MemoryPacket;
import anon.themis.comm.protocol.Message;
import anon.themis.inference.Memory;
import anon.themis.inference.MemoryAtoms;
import anon.themis.inference.Representation;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.utils.SimplifierFactory;

/**
 * Main Orchestration Monitor
 * 
 */
public class MonOrchMain extends GeneralMonitor {
	private Automata spec;
	private Representation autRep;
	private Memory<Atom> memory;
	
	public MonOrchMain(Integer id) {
		super(id);
	}
	@Override
	public void reset() {
		memory.clear();
		autRep = new Representation(spec, SimplifierFactory.getDefault());
	}
	@Override
	public void setup() 
	{
		spec = ((SpecAutomata) getConfig().getSpec().get("root")).getAut();
		autRep = new Representation(spec);
		memory = new MemoryAtoms();
	}

	public Representation getRepresentation() {
		return autRep;
	}
	public Automata getAutomata() {
		return spec;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void monitor(int t, Memory<Atom> observations) 
	throws ReportVerdict, ExceptionStopMonitoring 
	{	
		memory.merge(observations);
		Message m;
		while((m = recv()) != null) {
			MemoryPacket packet = (MemoryPacket) m;
			memory.merge((Memory<Atom>) packet.mem);
		}
		//Nothing observed: Either first timestamp or end of trace
		if(memory.isEmpty())
			return;
		else
			autRep.tick();
		
		autRep.update(memory, -1);
		VerdictTimed v = autRep.scanVerdict();
		memory.clear();
		
		if(v.isFinal()) {
			throw new ReportVerdict(v.getVerdict(), t);
		}
		
		autRep.dropResolved();
	}

	@Override
	public void communicate() {}

}
