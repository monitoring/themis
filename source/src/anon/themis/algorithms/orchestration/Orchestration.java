package anon.themis.algorithms.orchestration;


import java.util.HashMap;
import java.util.Map;

import anon.themis.monitoring.Component;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.utils.Convert;

/**
 * Orchestration Algorithm
 * 
 */
public class Orchestration extends MonitoringAlgorithm {
	public Orchestration() {
		super();
	}
	public Orchestration(MonitorSettings config) {
		super(config);
	}

	protected Map<Integer, ? extends Monitor> setup()  {
		SpecAutomata spec = Convert.makeAutomataSpec(config.getSpec().get("root"));
		config.getSpec().put("root", spec);
		int c 		   				= config.getComponentsSize();
		Map<Integer, Monitor> mons 	= new HashMap<Integer, Monitor>(c+1);
		
		Monitor main = new MonOrchMain(0);
		//Main monitor is ID = 0
		mons.put(0, main);
		int i = 0;
		//Create one slave per component
		for(Component comp : config.getComponents()) {
			if(i == 0) {
				attachMonitor(comp, main);
				i++;
				continue;
			}
			Monitor mon = new MonOrchSlave(i);
			attachMonitor(comp, mon);
			mons.put(i,  mon);
			i++;
		}
		return mons;
	}
	@Override
	public void report(ReportVerdict v) {
		System.out.println("[Verdict] " + v);
	}
	
	@Override
	public void abort(ExceptionStopMonitoring error) {
		System.err.println("[Abort] " + error.getError());
	}
	
}
