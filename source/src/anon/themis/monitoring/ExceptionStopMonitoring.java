package anon.themis.monitoring;

/**
 * Exception to abort the monitoring algorithm
 * 
 */
@SuppressWarnings("serial")
public class ExceptionStopMonitoring extends Exception {
	private String error;
	
	/**
	 * Halt monitoring and report a message
	 * @param error
	 */
	public ExceptionStopMonitoring(String error) {
		this.error = error;
	}
	public String getError() {
		return error;
	}
}
