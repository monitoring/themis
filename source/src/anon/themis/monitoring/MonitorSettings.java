package anon.themis.monitoring;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import anon.themis.comm.protocol.Message;
import anon.themis.traces.Trace;
import anon.themis.utils.SpecLoader;

/**
 * Configuration for the monitoring algorithm
 * Individual monitors should have access to this
 * 
 */
public class MonitorSettings extends Message {
	private static final long serialVersionUID = 1L;
	
	int runLength;
	Map<String, Specification> spec;
	SymbolMapper mapper;
	Map<Component, Trace> traces = new HashMap<Component,Trace>();
	
	/**
	 * Return the used SymbolMapper
	 * @return
	 */
	public SymbolMapper getMapper() {
		return mapper;
	}
	/**
	 * Set the SymbolMapper to use
	 * @param mapper
	 */
	public void setMapper(SymbolMapper mapper) {
		this.mapper = mapper;
	}
	/**
	 * Return the number of components
	 * @return
	 */
	public int getComponentsSize() {
		return traces.size();
	}
	/**
	 * Set the decentralized specification
	 * Typically parsed by using {@link SpecLoader#loadSpec(java.io.File)}
	 * @param spec
	 */
	public void setSpec(Map<String, Specification> spec) {
		this.spec = spec;
	}
	/**
	 * Return the decentrlized specification
	 * @return
	 */
	public Map<String, Specification> getSpec() {
		return spec;
	}
	
	/**
	 * Set the maximum length of the run
	 * After this timestamps the algorithm times out
	 * Could exceed trace length to account for delay
	 * @param traceLength
	 */
	public void setRunLength(int traceLength) {
		this.runLength = traceLength;
	}

	/**
	 * Returns the maximum run length after which to timeout
	 * @return
	 */
	public int getRunLength() {
		return runLength;
	}
	/**
	 * Return the set of components
	 * @return
	 */
	public Set<Component> getComponents() {
		return traces.keySet();
	}
	/**
	 * Remove all traces
	 */
	public void clearTraces() {
		for(Trace t : traces.values()) 
			t.stop();
		traces.clear();
	}
	/**
	 * Attach a trace to a component
	 * @param comp
	 * @param trace
	 */
	public void registerTrace(Component comp, Trace trace) {
		traces.put(comp, trace);
	}
	/**
	 * Return a trace for a component
	 * @param comp
	 * @return
	 */
	public Trace getTrace(Component comp) {
		return traces.get(comp);
	}
}
