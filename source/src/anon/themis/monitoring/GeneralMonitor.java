package anon.themis.monitoring;

import anon.themis.comm.protocol.Message;

/**
 * A simple monitor with basic functionality
 * 
 * Basic monitor sets/gets ID and has send/recv
 * Needs to be extended to implement monitor() and communicate()
 * 
 */
public abstract class GeneralMonitor implements Monitor {

	/**
	 * Monitor ID
	 */
	protected Integer id;
	/**
	 * Monitor Algorithm Reference
	 */
	protected MonitoringAlgorithm alg;
	
	/**
	 * Create a monitor with a given ID
	 * @param id
	 */
	public GeneralMonitor(Integer id) {
		this.id = id;
	}
	/**
	 * Set the monitor ID
	 * @param id
	 */
	public void setID(Integer id) {
		this.id = id;
	}
	/**
	 * Return the Monitor ID
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Return the configuration
	 * @return
	 */
	protected final MonitorSettings getConfig() {
		return alg.getConfig();
	}
	
	public final void setAlg(MonitoringAlgorithm alg) {
		this.alg = alg;
	}
	public final MonitoringAlgorithm getAlg() {
		return alg;
	}
	
	public void send(int to, Message msg) {
		alg.getNet().put(to, msg);
	}
	
	/**
	 * Default receive will consume the message
	 * @return
	 */
	public  Message recv() {
		return alg.getNet().get(getID(), true);
	}
	public  Message recv(boolean consume) {
		return alg.getNet().get(getID(), consume);
	}

}
