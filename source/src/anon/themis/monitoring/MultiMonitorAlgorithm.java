package anon.themis.monitoring;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public abstract class MultiMonitorAlgorithm<T extends Monitor> extends MonitoringAlgorithm  {
	
	protected abstract Class<? extends T> getMonitorType();
	
	protected Class<? extends T> cls;
	
	public MultiMonitorAlgorithm() {
		cls = getMonitorType();
		
	}
	
	public MultiMonitorAlgorithm(MonitorSettings settings) {
		super(settings);
		cls = getMonitorType();
	}
	
	@SuppressWarnings("unchecked")
	public T newMonitor(Object ... args) {
		Class<?>[] ar = new Class<?>[args.length];
		
		for(int i = 0; i < args.length; i ++)
			ar[i] = args[i].getClass();
		
		Method method = null;
		for(Method m : cls.getClass().getMethods()) {
			if(m.getName().equals("getConstructor")) {
				method = m;
				break;
			}
		}
		try {
			Constructor<T> cons = (Constructor<T>) method.invoke(cls, new Object[] {ar});
			return cons.newInstance(args);
			
		} catch (IllegalAccessException | IllegalArgumentException | InstantiationException e) {
			e.printStackTrace();
			throw new Error("Monitor cannot be instantiated");
		}
		catch (InvocationTargetException  e) {
			e.getTargetException().printStackTrace();
			throw new Error("Monitor cannot be instantiated");
		}
	}
}
