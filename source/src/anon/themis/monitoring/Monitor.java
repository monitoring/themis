package anon.themis.monitoring;

import anon.themis.automata.Atom;
import anon.themis.comm.protocol.Message;
import anon.themis.inference.Memory;

/**
 * A monitor that needs to be setup by the algorithm
 * 
 */
public interface Monitor {
  /**
   * Setup the monitor, this is run only once before reading all traces
   */
	void setup();
	/**
	 * Main monitoring function
	 * @param timestamp  Timestamp as given by the algorithm
	 * @param observations Observations provided by the algorithm
	 * @throws ReportVerdict Upon reaching the final verdict
	 * @throws ExceptionStopMonitoring To abort the monitoring
	 */
	void monitor(int timestamp, Memory<Atom> observations) throws ReportVerdict, ExceptionStopMonitoring;
	/**
	 * Called after all monitors have done their monitor function
	 * Typically used for specific communication
	 */
	void communicate();
	/**
	 * Associate the monitor with an algorithm
	 * @param alg
	 */
	void setAlg(MonitoringAlgorithm alg);
	/**
	 * Setup the monitor, this is run for every trace before its read
	 */
	void reset();
	/**
	 * Return the associated monitoring algorithm
	 * @return
	 */
	MonitoringAlgorithm getAlg();
	/**
	 * Return the monitor ID
	 * @return
	 */
	int  getID();
	/**
	 * Send a message to another monitor
	 * @param to
	 * @param msg
	 */
	void send(int to, Message msg);
	/**
	 * Read the received message
	 * @param consume peek (false) or consume (true) the message read
	 * @return
	 */
	Message recv(boolean consume);
	
}
