package anon.themis.monitoring;

import java.io.File;

import anon.themis.automata.Automata;
import anon.themis.automata.formats.FormatExpression;
import anon.themis.parser.FrontAutomataParser;

/**
 * Specification using Automata
 * 
 */
public class SpecAutomata implements Specification {
	Automata aut;
	
	/**
	 * Create an empty specification
	 */
	public SpecAutomata() {
		
	}
	
	/**
	 * Create a new specification given an automaton
	 * @param aut
	 */
	public SpecAutomata(Automata aut) {
		this.aut = aut;
	}
	/**
	 * Return the automaton
	 * @return
	 */
	public Automata getAut() {
		return aut;
	}
	
	
	/**
	 * Load an automaton from a file
	 * Format is DOT format obtained from LTL3Tools
	 * @param filename
	 * @see <a href="http://ltl3tools.sourceforge.net/">LTL3Tools</a>
	 */
	public void loadFile(String filename) {
		try {
			aut    = FrontAutomataParser.load(filename, 
							new FormatExpression());
		}
		catch(Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
}
