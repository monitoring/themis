package anon.themis.monitoring;

import java.io.File;
import java.util.Scanner;

import anon.themis.expressions.ltl.LTLExpression;
import anon.themis.utils.Expressions;

/**
 * Specification using LTL
 * 
 */
public class SpecLTL implements Specification {
	private LTLExpression ltl;
	
	/**
	 * Create an empty spec
	 */
	public SpecLTL() {
		
	}
	/**
	 * Create a new LTL specification by parsing the string
	 * @param expr
	 */
	public SpecLTL(String expr) {
		setLTL(expr);
	}
	/**
	 * Create a new LTL specification by using an LTL expression
	 * @param expr
	 */
	public SpecLTL(LTLExpression expr) {
		setLTL(expr);
	}
	/**
	 * Create a new LTL specification by parsing the first line in the file 
	 * @param fname
	 */
	public void fromFile(String fname) {
		File f = new File(fname);
		Scanner scan;
		try {
			scan = new Scanner(f);
			setLTL(scan.nextLine());
			scan.close();
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	/**
	 * Set the LTL spec by parsing string
	 * @param ltl 
	 */
	public void setLTL(String ltl) {
		this.ltl = Expressions.parseLTL(ltl);
	}
	/**
	 * Set the LTL spec to the expression
	 * @param ltl
	 */
	public void setLTL(LTLExpression ltl) {
		this.ltl = ltl;
	}
	/**
	 * Return the LTL expression
	 * @return
	 */
	public LTLExpression getLTL() {
		return ltl;
	}
	@Override
	public String toString() {
		return ltl.toString();
	}
}
