package anon.themis.monitoring;

import anon.themis.automata.Atom;
import anon.themis.symbols.AtomObligation;
import anon.themis.symbols.AtomString;

/**
 * Assumes AP are strings of [a-zA-Z]+[0-9]+ 
 * The component id is the alpha part
 * The monitor is the alpha converted into number:
 * a = 0, z = 25
 * aa = 26 - ab = 27 etc.
 */
public class DefaultSymbolMapper implements SymbolMapper {

	@Override
	public String componentFor(Atom ap) {
		if(ap instanceof AtomString)
			return ((AtomString) ap).toString().split("[0-9]")[0].toLowerCase();
		else if(ap instanceof AtomObligation)
			return componentFor(((AtomObligation) ap).getAtom());
		
		return "";
	}

	@Override
	public Integer monitorFor(String component) {
		component = component.toLowerCase();
		int num = 0;
		byte[] letters = component.getBytes();
		for(int i = 0; i < letters.length; i++)
			num += (letters[i] - 'a') + (26 * i);
		return num;
	}

	@Override
	public Integer monitorFor(Atom ap) {
		return monitorFor(componentFor(ap));
	}

}
