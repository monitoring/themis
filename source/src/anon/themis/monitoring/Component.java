package anon.themis.monitoring;

/**
 * Wrapper that represents a component
 * 
 */
public class Component  {
	private String name;

	/**
	 * Name of the component
	 * @param name
	 */
	public Component(String name) {
		this.name = name;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Component)
			return ((Component) obj).name.equals(name);
		else
			return false;
	}

	/**
	 * Return the component name
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the component name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	
}
