package anon.themis.monitoring;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import anon.themis.automata.Atom;
import anon.themis.automata.Verdict;
import anon.themis.comm.protocol.NetworkArray;
import anon.themis.inference.Memory;
import anon.themis.inference.MemoryAtoms;
import anon.themis.symbols.AtomNegation;
import anon.themis.symbols.AtomObligation;

/**
 * A general Monitoring Algorithm
 * 
 * Uses the simple {@link NetworkArray} communication interface
 * And provides basic trace reading functionality
 * Used to implement basic monitoring algorithm by just extending 
 * {@link #setup()} {@link #report(ReportVerdict)} {@link #abort(ExceptionStopMonitoring)}
 * 
 */
public abstract class MonitoringAlgorithm  {
	
	
	/**
	 * Configuration for the algorithm
	 */
	protected MonitorSettings config;
	/**
	 * Communication Platform
	 */
	protected NetworkArray net;
	/**
	 * Map of monitors available
	 */
	protected Map<Integer, ? extends Monitor>  monitors;
	/**
	 * Monitor <-> Component bindings
	 */
	protected Map<Integer, Component> attached = new HashMap<Integer, Component>();
	
	/**
	 * Implement the setup phase for the algorithm
	 * Typically an algorithm would:
	 * Create the monitors and 
	 * Call {@link #attachMonitor(Component, Monitor)}
	 * 
	 * @return
	 */
	protected abstract Map<Integer, ? extends Monitor> setup();
	/**
	 * Handle a verdict report
	 * @param v
	 */
	public abstract void report(ReportVerdict v);
	/**
	 * Abort monitoring
	 * @param error
	 */
	public abstract void abort(ExceptionStopMonitoring error);
	
	/**
	 * Empty constructor is necessary to easily instanciate the class from String 
	 */
	public MonitoringAlgorithm() {}
	
	/**
	 * Create a new monitoring algorithm with the settings
	 * @param settings
	 */
	public MonitoringAlgorithm(MonitorSettings settings) {
		setConfig(settings);
	}
	
	
	/**
	 * Set a configuration
	 * @param config
	 */
	public void setConfig(MonitorSettings config) {
		this.config = config;
	}
	
	/**
	 * Executes the Monitoring
	 */
	public void start() {
		for(Monitor mon : monitors.values()) mon.reset();
		net.reset();
		monitor();
	}
	
	/**
	 * Attach a monitor to a component
	 * @param component
	 * @param mon
	 * @return
	 */
	protected boolean attachMonitor(Component component, Monitor mon) {
		if(config.getComponents().contains(component))
		{
			attached.put(mon.getID(), component);
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Setup Phase
	 */
	public void setupNetwork() {
		if(config.getComponentsSize() <= 0) {
			System.err.println("Need at least 1 component to monitor");
			return;
		}
		monitors = setup();
		if(monitors.size() < 1) {
			System.err.println("No Monitor Registered");
			return;
		}
		net = new NetworkArray(monitors);
		for(Monitor mon : monitors.values()) {
			mon.setAlg(this);
			mon.setup();
		}
	}
	
	/**
	 * Monitor Phase
	 * Go over all timestamps in a run invoking {@link #monitorTimestamp(int)}
	 * Calls {@link #report(ReportVerdict)} or {@link #abort(ExceptionStopMonitoring)}
	 * when a monitor throws it
	 */
	protected void monitor() {
		int t = 1;
		for(; t <= config.getRunLength(); t++) {
			try
			{
				monitorTimestamp(t);
			}
			catch(ReportVerdict verdict) {
				report(verdict);
				return;
			}
			catch(ExceptionStopMonitoring stop) {
				abort(stop);
				return;
			}
		}
		report(new ReportVerdict(Verdict.NA, t - 1));
	}
	
	/**
	 * Monitor a timestamp t 
	 * This reads all observations {@link #getObservations(int)}
	 * Then iterates over all monitors executing their {@link Monitor#monitor(int, Memory)} method
	 * Once all have executed monitor it iterates over all monitors and executes {@link Monitor#communicate()}
	 * Once both are done it calls {@link #sync()}
	 * @param t
	 * @throws ReportVerdict
	 * @throws ExceptionStopMonitoring
	 */
	protected void monitorTimestamp(int t) throws ReportVerdict, ExceptionStopMonitoring {
		Map<Component, Memory<Atom>> observations = getObservations(t) ;
		for(Monitor mon : monitors.values())
		{
			Component comp = attached.get(mon.getID());
			mon.monitor(t, observations.get(comp));
		}
		for(Monitor mon : monitors.values())
			mon.communicate();
		sync();
	}
	
	/**
	 * Return the number of monitors
	 * @return
	 */
	public Integer getMonitorCount() {
		return monitors.size();
	}
	
	/**
	 * Return the number of components
	 * @return
	 */
	public Integer getComponentCount() {
		return config.getComponentsSize();
	}
	
	
	/**
	 * Return the component to which the monitor with id is attached
	 * @param id
	 * @return
	 */
	public Component getComponentForMonitor(Integer id) {
		return attached.get(id);
	}
	
	
	/**
	 * Returns the monitors attached to a component
	 * @param comp
	 * @return
	 */
	public Set<Monitor> getMonitorsForComponent(Component comp) {
		Set<Monitor> res = new HashSet<Monitor>();
		
		for(Entry<Integer, Component> mon : attached.entrySet())
			if(mon.getValue().getName().equals(comp.toString()))
				res.add(monitors.get(mon.getKey()));
		
		return res;
	}
	
	/**
	 * Return a map of observations per component for a given timestamp
	 * @param t
	 * @return
	 */
	protected Map<Component, Memory<Atom>> getObservations (int t) {
		Map<Component, Memory<Atom>> data = new HashMap<Component, Memory<Atom>>();
		for(Component comp : config.getComponents()) {
			Memory<Atom> timestamped = new MemoryAtoms();
			Memory<Atom> symbols	 = config.getTrace(comp).next();
			
			for(Atom atom : symbols.getData()) {
				if(atom instanceof AtomNegation) {
					atom = ((AtomNegation) atom).getNegated();
					timestamped.add(new AtomObligation(atom, t), false);
				}
				else
					timestamped.add(new AtomObligation(atom, t), true);
			}
			data.put(comp, timestamped);
		}
		return data;
	}
	
	/**
	 * Return all monitor IDs
	 * @return
	 */
	public Set<Integer> getMonitorIDs() {
		return monitors.keySet();
	}
	
	/**
	 * Return the configuration
	 * Used by monitors to read configuration
	 * @return
	 */
	public MonitorSettings getConfig() {
		return config;
	}
	
	/**
	 * Return the network
	 * @return
	 */
	public NetworkArray getNet() {
		return net;
	}
	
	/**
	 * Synchronized communication
	 */
	public void sync(){
		net.sync();
	}
}
