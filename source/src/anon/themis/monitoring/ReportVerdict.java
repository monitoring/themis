package anon.themis.monitoring;

import anon.themis.automata.Verdict;
import anon.themis.automata.VerdictTimed;

/**
 * An exception used to report the final verdict
 * 
 */
public class ReportVerdict extends Exception {
	private static final long serialVersionUID = 1L;
	private VerdictTimed verdict;
	
	public ReportVerdict(VerdictTimed verdict) {
		this.verdict = verdict;
	}
	public ReportVerdict(Verdict verdict, int timestamp) {
		this.verdict 	= new VerdictTimed(verdict, timestamp);
	}
	public Verdict getVerdict() {
		return verdict.getVerdict();
	}
	public int getTimestamp() {
		return verdict.getTimestamp();
	}
	@Override
	public String toString() {
		return verdict.toString();
	}
}
