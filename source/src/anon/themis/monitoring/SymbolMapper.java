package anon.themis.monitoring;

import anon.themis.automata.Atom;

/**
 * The interface to associate symbols
 * 
 */
public interface SymbolMapper {
	/**
	 * Maps an atom to a component name
	 * @param ap
	 * @return
	 */
	String	componentFor(Atom ap); 
	/**
	 * Maps a component to a monitor ID
	 * @param component
	 * @return
	 */
	Integer	monitorFor(String component);
	/**
	 * Maps an atom to a monitor ID
	 * @param ap
	 * @return
	 */
	Integer	monitorFor(Atom ap);
}
