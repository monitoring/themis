package anon.themis.measurements;

import anon.themis.automata.Atom;
import anon.themis.automata.State;
import anon.themis.automata.Verdict;
import anon.themis.comm.protocol.Message;
import anon.themis.comm.protocol.NetworkArray;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.inference.Memory;
import anon.themis.inference.Rule;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MonitoringAlgorithm;

/**
 * Utility Functions for Benchmarks
 * 
 */
public privileged aspect Commons {
	private MonitoringAlgorithm currentAlg;
	
	//Formats
	public static final String FMT_KEY = "%-35s";
	public static final String FMT_SEP = " : ";
	public static final String FMT_NUM = "%15d";
	public static final String FMT_DEC = "%15.2f";
	public static final String FMT_BOOL= "%15b";
	public static final String FMT_STR = "%15s";
	
	//Pointcut to avoid the benchmarks package
	public pointcut benchPkg() :
		within(anon.themis.benchmarks.*);
	
	//Pointcut to indicate the setup phase of monitoring
	public pointcut monitoringSetup(MonitoringAlgorithm alg) : 
		call(void MonitoringAlgorithm.setupNetwork()) && target(alg);
	
	//Pointcut to indicate the run phase of monitoring
	public pointcut monitoringMain() : 
		call(void MonitoringAlgorithm.start());
	
	//Pointcut to indicate the step phase for each timestamp
	public pointcut monitoringStep(Integer t) :
		call(protected void MonitoringAlgorithm.monitorTimestamp(int)) 
		&& args(t);
	public pointcut monitorStep(int t, Memory<Atom> obs) :
		call(void Monitor.monitor(int, Memory<Atom>))
		&& args(t, obs);
	
	
	//Pointcut to capture messages sent
	public pointcut sendMessage(Integer to, Message m) :
		call(void NetworkArray.put(int, Message)) 
		&& args(to, m);
	
	//Pointcut to capture evaluating Rules
	public pointcut evalRule() :
		call(State Rule.resolve(Memory));
	
	//Pointcut to capture evaluating expressions
	public pointcut evalExpr() :
		call(Verdict BoolExpression.eval(Memory));
	
	//Pointcut to capture Monitoring call
	public pointcut inMonitor(Monitor mon) :
		cflow(execution(void Monitor.monitor(..)) && target(mon));
	
	//Get Algorithm
	after() : initialization(MonitoringAlgorithm.new()) {
		currentAlg = ((MonitoringAlgorithm) thisJoinPoint.getThis());
	}
	
	
	public MonitoringAlgorithm getAlgorithm() {
		return currentAlg;
	}
	public MonitorSettings getConfig() {
		return currentAlg.getConfig();
	}
	
	public static void printSection(String title) {
		System.out.println();
		System.out.println("["+ title + "]");
	}
	public static void printMeasure(String label, String fmt, Object data) {
		System.out.printf(
			FMT_KEY + FMT_SEP + fmt,
			label,
			data
		);
	}
	public static void printMeasureln(String label, String fmt, Object data) {
		System.out.printf(
			FMT_KEY + FMT_SEP + fmt + "\n",
			label,
			data
		);
	}
	public static void printMeasures(String sep, Object... data) 
	{
		if((data.length % 3) != 0)
			throw new Error("Measures supplied are not correct tuples");
		
		for(int i = 0; i < data.length; i += 3) {
			printMeasure((String) data[i], (String) data[i+1], data[i+2]);
			System.out.print(sep);
		}
	}
}
