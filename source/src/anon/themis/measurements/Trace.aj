package anon.themis.measurements;


import java.io.File;

import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.tools.experiment.Experiment;

/**
 * Store the Trace ID
 * 
 */
public privileged aspect Trace extends Instrumentation {
	int lasttrace = -1;
	
	before(File input, int tid, int ncomps, MonitorSettings settings) : 
		execution(void Experiment.bindTrace(File, int, int, MonitorSettings))	
	 && args(input, tid, ncomps, settings)
	{
		lasttrace = tid;
		
	}

	@Override
	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Trace Info");
		addMeasures(
			  new Measure("tid"  , "Trace ID" 		, lasttrace , Measures.replace)
		);
	}
}
