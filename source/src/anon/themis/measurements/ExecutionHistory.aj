package anon.themis.measurements;


import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.inference.Representation;

/**
 * Benchmarks for Formula Evaluations
 * 
 */
public privileged aspect ExecutionHistory extends Instrumentation {
	
	int ts_cur = 0;
	
	before(Integer nval, Monitor mon) : 
		set(int Representation.lastResolved)
		&& args(nval)
		&& withincode(* Representation.update(..))
		&& Commons.inMonitor(mon)
	{
		
		long old 	=  (long) (((Representation) thisJoinPoint.getThis()).lastResolved);
		long delay 	= (ts_cur - 1) - old;
		if(delay > 0) {
			update("resolutions", 1);
			update("sum_delay", delay);
			update("max_delay", delay);
			update("min_delay", delay);
		}
	}

	@Override
	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Execution History");
		addMeasure(new Measure("sum_delay"  , "Total Delay"	, 0l, Measures.addLong));
		addMeasure(new Measure("resolutions", "Total Resolutions"	, 0, Measures.addInt));
		addMeasure(new Measure("max_delay"  , "Max Delay", 0l, Measures.max));
		addMeasure(new Measure("min_delay"	, "Min Delay", (long) alg.getConfig().getRunLength(), Measures.min));

	}
	
	@Override
	protected void stepBegin(int t)
	{
		ts_cur = t;
	}
}
