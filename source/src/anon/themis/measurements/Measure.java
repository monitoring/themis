package anon.themis.measurements;

/**
 * A Benchmark Measure
 * 
 */
public class Measure {
  /**
   * Measure key used to update or save measure
   */
	protected String key;
	protected String description;
	/**
	 * Value of the measure
	 */
	protected Object value;
	/**
	 * Default update measure for the function
	 */
	protected MeasureFunction func;
	protected boolean persistant;
	
	@Override
	public int hashCode() {
		return key.hashCode();
	}

	public Measure(String key, String description, Object value) {
		this(key, description, value, MeasureFunction.Default, false);
	}
	public Measure(String key, String description, Object value, boolean persist) {
		this(key, description, value, MeasureFunction.Default, persist);
	}
	public Measure(String key, String description, Object value, MeasureFunction updateFunc) {
		this(key, description, value, updateFunc, false);
	}

	public Measure(String key, String description, Object value, MeasureFunction updateFunc, boolean persist) {
		this.key 			= key;
		this.description 	= description;
		this.value 			= value;
		this.func			= updateFunc;
		this.persistant		= persist;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * Sets a label description for the measure
	 * (For printing mostly)
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	public Object update(Object... args) {
		value = this.func.update(value, args);
		return value;
	}
	/**
	 * Measure is not reset when reading a new trace
	 * @param persistant
	 */
	public void setPersistant(boolean persistant) {
		this.persistant = persistant;
	}
	public void setFunc(MeasureFunction func) {
		this.func = func;
	}
	public MeasureFunction getFunc() {
		return func;
	}
	
	/**
	 * Measure should not reset when reading a new trace
	 * @return
	 */
	public boolean isPersistant() {
		return persistant;
	}
	
}
