package anon.themis.measurements;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.LinkedList;

import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.Specification;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.SpecLTL;
import anon.themis.algorithms.choreography.Choreography;
import anon.themis.algorithms.choreography.MonitorNetwork;
import anon.themis.algorithms.choreography.Score;
import anon.themis.utils.Expressions;
import anon.themis.expressions.ltl.LTLExpression;

/**
 * Benchmarks Related to the LTL information
 * 
 */
public privileged aspect LTLData extends Instrumentation {

	private LTLExpression expr = null;
	private int data = 0;
	private MonitorSettings config = null;
	
	after(Map<String, Specification> spec) : 
		call(void MonitorSettings.setSpec(Map<String, Specification>)) 
		&& args(spec)
		&& withincode(static void *.main(String[]))
	{
		config 	= (MonitorSettings) thisJoinPoint.getTarget();
		
		data = 0;
		expr = null;
		if(spec.containsKey("root")) {
			Specification rspec = spec.get("root");
			if(rspec instanceof SpecLTL) {
				expr 	= ((SpecLTL) rspec).getLTL();
				data	= Expressions.getLength(expr).leaves.size();
			}
		}
	}
	
	@Override
	protected void setupOnce(MonitoringAlgorithm alg) {
		setDescription("LTL Information");
		addMeasure(new Measure("ltl_size", "LTL Length", data, true));
		process();
		addHash();
	}
	@Override
	protected void setupRun(MonitoringAlgorithm alg) {
		
	}
	protected void addHash() {
		addMeasure(new Measure("ltl_hash", "LTL Hash", expr.toString().hashCode(), true));
	}
	
	private void process() {
		if(expr == null) {
			System.err.println("expr is null!");
			System.exit(1);
			addMeasure(new Measure("ltl_depth", "LTL Depth", 			-1, 	true));
			addMeasure(new Measure("ltl_comp", "Components in Spec", 	-1, 	true));
			addMeasure(new Measure("ltl_host", "Root Component", 		"a", 	true));
			return;
		}
		
		Score data = Score.getScoreTree(expr, config.getMapper());
		Choreography.resetId(0);
		MonitorNetwork net = Choreography.splitFormula(Choreography.newId(), data.comp, data, expr, false);
		Choreography.compactNetwork(net);
		Choreography.resetId(0);
		
		//Compute depth by checking references
		Map<Integer, List<Integer>> mons = new HashMap<Integer, List<Integer>>(net.nodes.size()); 
		for(Entry<Integer, Set<Integer>> entry : net.edges.entrySet()) {
			for(Integer coref : entry.getValue()) {
				if(!mons.containsKey(coref)) mons.put(coref, new LinkedList<Integer>());
				mons.get(coref).add(entry.getKey());
			}
		}
		int netd 		= depth(net.root.id, mons);
		//Compute actual number of components referenced by formula and highest one referenced
		int sc			= 0;
		String maxc		= net.root.comp;
		
		for(Map.Entry<String, Double> entry : data.score.entrySet())
			if(entry.getValue() > 0)
				sc++;

		addMeasure(new Measure("ltl_depth", "LTL Depth", 			netd, 	true));
		addMeasure(new Measure("ltl_comp", "Components in Spec", 	sc, 	true));
		addMeasure(new Measure("ltl_host", "Root Component", 		maxc, 	true));
	}
	public static int depth(int id, Map<Integer, List<Integer>> mons) {
		List<Integer> mon = mons.get(id);
		
		if(mon == null || mon.isEmpty()) {
			return 0;
		}
		else {
			int max = 0;
			for(Integer coref : mon)
				max = Math.max(max, depth(coref, mons));
			return 1 + max;
		}
	}
	
}
