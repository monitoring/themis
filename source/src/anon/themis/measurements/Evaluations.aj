package anon.themis.measurements;

import java.util.HashMap;
import java.util.Map;

import anon.themis.expressions.bool.BoolExpression;
import anon.themis.monitoring.Component;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;

/**
 * Benchmarks for Formula Evaluations
 * 
 */
public privileged aspect Evaluations extends Instrumentation {
	private Map<Integer, ? extends Monitor> mons;
	private Map<Integer, Long> evals; 
	private Long total = 0l;
	private Deviations<Component> devs;
	private MonitoringAlgorithm alg;
	
	
	
	after() returning (Map<Integer, ? extends Monitor> mons): 
		call(protected Map<Integer, ? extends Monitor> MonitoringAlgorithm.setup())
	{
		this.mons = mons;
	}
	
	private void computeMax() {
		Long max = 0L;
		for(Long x : evals.values())
			max = Math.max(max, x);
		
		total += max;
	}
	
	after(Monitor mon) : 
		execution(* BoolExpression.eval(*))
		&& Commons.inMonitor(mon)
	{
		evals.put(mon.getID(), evals.get(mon.getID()) + 1);
		devs.push(alg.getComponentForMonitor(mon.getID()));
		update("exp_eval", 1L);
	}

	@Override
	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Evaluations");
		addMeasure(new Measure("exp_eval" , "Expressions Evaluated"	, 0L, Measures.addLong));
		addMeasure(new Measure("exp_avg", "Average Eval /mon/round", 0d, Measures.replace));
		addMeasure(new Measure("load_exp"	, "Eval Load", 0d, Measures.replace));
		addMeasure(new Measure("spread_exp" , "Eval Spread", 0d, Measures.addDouble));
		
		devs 		= new Deviations<Component>(alg.getConfig().getComponents());
		this.alg 	= alg;
		total 		= 0L;
	}
	
	protected void updateLoad() {
		double[] d = devs.update();
		
		if(d[0] > 0) {
			update("load_exp"  , d[0]);
			update("spread_exp", d[1]);
		}
	}
	
	@Override
	protected void stepBegin(int t)
	{
		evals = new HashMap<Integer, Long>(mons.size());
		for(Monitor mon : mons.values())
			evals.put(mon.getID(), 0l);
		devs.reset();
	}
	@Override
	protected void stepEnd(int t)
	{
		recompute(t);
	}
	protected void stepReport(int t, ReportVerdict rep) 
	{
		recompute(t);
	}
	protected void recompute(int t) {
		computeMax();
		update("exp_avg", new Double(total / (double) t));
		updateLoad();
	}
}
