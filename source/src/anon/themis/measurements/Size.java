package anon.themis.measurements;

/**
 * Size of an element
 * 
 */
public interface Size {
	Long get(DataSize s);
}
