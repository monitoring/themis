package anon.themis.measurements;

import anon.themis.algorithms.choreography.MessageKill;
import anon.themis.algorithms.choreography.MessageVerdict;
import anon.themis.automata.Atom;
import anon.themis.comm.packets.MemoryPacket;
import anon.themis.comm.packets.RepresentationPacket;
import anon.themis.comm.protocol.Message;
import anon.themis.expressions.bool.Atomic;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.False;
import anon.themis.expressions.bool.True;
import anon.themis.inference.Memory;
import anon.themis.inference.Rule;
import anon.themis.symbols.AtomEmpty;
import anon.themis.symbols.AtomNegation;
import anon.themis.symbols.AtomObligation;
import anon.themis.symbols.AtomString;
import anon.themis.utils.Expressions;
import anon.themis.utils.Expressions.ExprSize;

/**
 * Benchmark Assumptions and Configurations
 * 
 */
public class Config {
	
  /**
   * Default Sizes assumptions in Bytes
   * 
   */
	public static class SizeDefault implements Size {
		@Override
		public Long get(DataSize s) {
			switch(s) {
			case S_BOOL:
			case S_CHAR:
			case S_CTRL:
			case S_OPER:
			case S_VER:
				return 1L;
			case S_ID:
			case S_TIME:
				return 4L;
			case S_STATE:
				return 2L;
			default:
				return 0L;
			}
		}
	}
	
	/**
	 * Computes the message size given a set of size assumptions
	 * @param m
	 * @param size
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Long sizeMessage(Message m, Size size) {
		
		//Choreography Messages
		if(m instanceof MessageKill) 		 
			return size.get(DataSize.S_CTRL) + size.get(DataSize.S_ID);
		else if(m instanceof MessageVerdict)
			return size.get(DataSize.S_ID)
					+ size.get(DataSize.S_TIME)
					+ size.get(DataSize.S_VER);
		//Orchestration Messages
		else if(m instanceof MemoryPacket)	 {
			Memory<Atom> mem =(Memory<Atom>) ((MemoryPacket) m).mem;
			Long total = 0l;
			for(Atom at : mem.getData()) 
				total += atomsLength(at, size);
			return total;
		}
		//Migration Messages
		else if(m instanceof RepresentationPacket) {
			Long total = 0l;
			for(Rule r : ((RepresentationPacket) m).repr.getRules().values()) {
				//A rule is a map of state -> expression
				//Size = size(state) + size(expression)
				for(BoolExpression expr : r.getPairs().values()) {
					ExprSize s = Expressions.getLength(expr);
					for(Object leaf : s.leaves)
						total += atomsLength(leaf, size);
					// size(operator) * (no of ops) + size(state)
					total += size.get(DataSize.S_OPER) * s.expressions 
							+ size.get(DataSize.S_STATE);
				}
			}
			return total;
		}
		else
			throw new Error("Cannot determine size of message: " + m.getClass().getName());
	}
	
	/**
	 * Computes the size of an atom
	 * @param leaf
	 * @param size
	 * @return
	 */
	private static Long atomsLength(Object leaf, Size size) {
		if(leaf instanceof Atomic) //Unpack to atom
			return atomsLength(((Atomic) leaf).getAtom(), size);
		else if(leaf instanceof True || leaf instanceof False)
			return size.get(DataSize.S_BOOL);
		else if (leaf instanceof AtomString)	//String size
			return leaf.toString().length() * size.get(DataSize.S_CHAR);
		else if (leaf instanceof AtomObligation)//Atom + Timestamp
			return atomsLength(((AtomObligation) leaf).getAtom(), size) 
					+ size.get(DataSize.S_TIME);
		else if (leaf instanceof AtomEmpty)		//Smaller than anything
			return size.get(DataSize.S_CTRL);
		else if (leaf instanceof AtomNegation) 	//Operator + Atom
			return size.get(DataSize.S_OPER)
					+ atomsLength(((AtomNegation) leaf).getNegated(),size);
		else
			throw new Error("Object Size not specified: " + leaf.getClass().getName());
	}
}
