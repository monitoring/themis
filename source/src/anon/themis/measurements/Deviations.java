/**
 * Deviations
 *
 * name <email>
 */
package anon.themis.measurements;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;

/**
 * Compute Load/Spread
 * 
 */
public class Deviations<T> {
	private int total;
	private Map<T, Integer> all;
	private Map<T, Integer> temp;
	private final Set<T> factors;
	

	public Deviations(final Set<T> set) {
		factors = set;
		all  	= init(set);
		temp 	= init(set);
		total	= 0;
	}
	public void reset() {
		temp = init(factors);
	}
	
	public void push(T key) {
		push(key, 1);
	}
	public void push(T key, Integer value) {
		all.put (key, all.get(key)  + value);
		temp.put(key, temp.get(key) + value);
		total++;
	}
	
	public double[] update() {
		
		if(total == 0) return new double[] {0d, 0d};
		

		double dev 	 = 0d;
		double devs	 = 0d;
		
		long total  = 0l;
		long totals = 0l;
		
		for(Map.Entry<T, Integer> entry : all.entrySet()) {
			total  += all.get(entry.getKey());
			totals += temp.get(entry.getKey());
		}
		
		double ideal  = 1 / factors.size();
		
		
		for(Map.Entry<T, Integer> entry : all.entrySet()) {
			double load = entry.getValue() / (double) total;
			
			dev += Math.pow(load - ideal, 2);
			
			if(totals > 0) {
				double spread = temp.get(entry.getKey()) / totals;
				devs	   += Math.pow(spread - ideal, 2);
			}
		}
		
		
		return new double[] {dev, devs};
	}
	
	private Map<T, Integer> init(Set<T> from) {
		Map<T, Integer> map = new HashMap<T, Integer>(from.size());
		
		for(T key : from)
			map.put(key, 0);
		
		return map;
	}

}
