package anon.themis.measurements;

import java.util.HashMap;
import java.util.Map;

import anon.themis.monitoring.Component;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.utils.Simplifier;

/**
 * Average simplifications per timestamp measures
 * 
 */
public privileged aspect Simplifications extends Instrumentation {
	private MonitoringAlgorithm alg;
	protected Map<Integer, ? extends Monitor> mons;
	protected Map<Integer, Integer> evals; 
	protected Deviations<Component> devs;

	
	public pointcut callSimplify(Monitor mon) :
		execution(* Simplifier.simplify(*))
		&& Commons.inMonitor(mon);
	

	after() returning (Map<Integer, ? extends Monitor> mons): 
		call(protected Map<Integer, ? extends Monitor> MonitoringAlgorithm.setup())
	{
		this.mons = mons;
	}
	
	protected int[] compute() {
		Integer max = 0;
		Integer sum = 0;

		for(Integer x : evals.values()) {
			max = Math.max(max, x);
			sum += x;
		}

		return new int[]{max, sum};
	}
	protected void updateLoad() {
		double[] d = devs.update();
		
		if(d[0] > 0) {
			update("load_simp"  , d[0]);
			update("spread_simp", d[1]);
		}
	}

	after(Monitor mon) : callSimplify(mon)
	{
		evals.put(mon.getID(), 	evals.get(mon.getID()) + 1);
		devs.push(alg.getComponentForMonitor(mon.getID()));

	}
	
	@Override
	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Simplifications");
		addMeasure(new Measure("simp_avg"	, "Total : Max Simplifications / Mon /Round", 0, Measures.addInt));
		addMeasure(new Measure("simp_total"	, "Total Simplficiations", 0, Measures.addInt));
		addMeasure(new Measure("load_simp"	, "Simplification Load", 0d, Measures.replace));
		addMeasure(new Measure("spread_simp", "Simplification Spread", 0d, Measures.addDouble));
		
		this.alg = alg;
		devs = new Deviations<Component>(alg.getConfig().getComponents());

	}
	@Override
	protected void stepBegin(int t)
	{
		evals 	= new HashMap<Integer, Integer>(mons.size());
		for(Monitor mon : mons.values())
			evals.put(mon.getID(), 0);
		devs.reset();
	}
	@Override
	protected void stepEnd(int t)
	{
		recompute(t);
	}
	protected void stepReport(int t, ReportVerdict rep) 
	{
		recompute(t);
	}
	protected void recompute(int t) {
		int[] v = compute();
		update("simp_avg",   v[0]);
		update("simp_total", v[1]);
		updateLoad();
	}
}
