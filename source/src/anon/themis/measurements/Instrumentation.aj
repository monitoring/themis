package anon.themis.measurements;

import java.lang.Double;
import java.lang.Float;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.utils.Opts;

/**
 * Benchmark Abstract Aspect
 * Defines basic benchmark functionality
 * Is intercepted by other aspects to save measures
 */
public abstract aspect Instrumentation {

	private String description = "";
	protected Map<String, Measure> measures = new HashMap<String,Measure>();
	private static int verbose = Integer.parseInt(Opts.getGlobal("THEMIS_BENCH_VERBOSE", "1"));
	
	/**
	 * Set the benchmark description (for printing purposes)
	 * @param label
	 */
	protected void setDescription(String label) {
		this.description = label;
	}
	/**
	 * Returns the measure associated with a key
	 * @param key
	 * @return
	 */
	public Measure getMeasure(String key) {
		return measures.get(key);
	}
	/**
	 * Adds a measure, the measures getKey defines the measure key for retreival or update
	 * @param value
	 */
	public void addMeasure(Measure value) {
		measures.put(value.getKey(), value);
	}
	/**
	 * Add Several measures at once
	 * @param measures
	 */
	public void addMeasures(Measure...measures) {
		for(Measure measure : measures)
			addMeasure(measure);
	}
	/**
	 * Update a function with a custom update function
	 * @param key
	 * @param mfunc
	 * @return
	 */
	public Object update(String key, MeasureFunction mfunc) {
		if(!measures.containsKey(key)) 
			return null;
		Object res = mfunc.update(measures.get(key).getValue());
		measures.get(key).setValue(res);
		return res;
	}
	/**
	 * Update a measure using its default update function
	 * @param key
	 * @param nvalue
	 * @return
	 */
	public Object update(String key, Object... nvalue) {
		if(!measures.containsKey(key)) 
			return null;
		Object res = measures.get(key).update(nvalue);
		measures.get(key).setValue(res);
		return res;
	}
	/**
	 * Return all registered measures
	 * @return
	 */
	public Map<String, Measure> getAllMeasures() {
		return measures;
	}

	after(MonitoringAlgorithm alg) : Commons.monitoringSetup(alg) {
		setupOnce(alg);
	}
	before(MonitoringAlgorithm alg) : 
		call(void MonitoringAlgorithm.start()) && target(alg)
	{
		cleanup();
		setupRun(alg);
		runBegin();
	}
	after() returning() : Commons.monitoringMain() {
		runEnd();
		if(verbose > 0)
			printAll();
	}
	
	void around(Integer t) : Commons.monitoringStep(t) {
		stepBegin(t);
		proceed(t);
		stepEnd(t);
	}
	after(Integer t) throwing(ReportVerdict rep) : Commons.monitoringStep(t) {
		stepReport(t, rep);
	}
	/**
	 * The purpose of this is to capture information and setup the run
	 * @param alg The current MonitoringAlgorithm setup
	 */
	protected void setupRun(MonitoringAlgorithm alg)  {}
	
	/**
	 * Executes before starting reading each trace
	 */
	protected void runBegin()  			{}
	/**
	 * Executes at the start of each discrete step
	 * @param t Timestamp for the step
	 */
	protected void stepBegin(int t) 	{}
	
	/**
	 * Executes at the end of each discrete step
	 * This will not execute when the algorithm throws a verdict
	 * @param t Timestamp of the step
	 */
	protected void stepEnd(int t) 		{}

	/**
	 * Executes when a verdict is reported by throwing the exception
	 * (This indicates the full algorithm has reported a verdict)
	 * This executes instead of stepEnd
	 * @param t
	 * @param rep
	 */
	protected void stepReport(int t, ReportVerdict rep)	{}
	/**
	 * Executes after finishing reading each trace
	 */
	protected void runEnd() 			{}
	
	/**
	 * Executes once before reading ALL traces
	 * @param alg
	 */
	protected void setupOnce(MonitoringAlgorithm alg)  {}

	/**
	 * Executes Before a run 
	 * The default removes non-persistent measures
	 */
	protected void cleanup() {
		Iterator<Measure> iter = measures.values().iterator();
		while(iter.hasNext()){
			Measure m = iter.next();
			if(!m.isPersistant()) {
				iter.remove();
			}
		}
	}
	/**
	 * Prints all measures
	 */
	protected void printAll() {
		Commons.printSection(description.isEmpty() ? this.getClass().getSimpleName() : description);
		for(Measure m : measures.values()) {
			Object data = m.getValue();
			String fmt  = getFormat(data);
			Commons.printMeasureln(m.getDescription(), fmt, data);
		}
	}
	protected String getFormat(Object o){
		if(o instanceof Long || o instanceof Integer) {
			return Commons.FMT_NUM;
		}
		if(o instanceof Double ||  o instanceof Float) {
			return Commons.FMT_DEC;
		}
		if(o instanceof Boolean) {
			return Commons.FMT_BOOL;
		}
		return Commons.FMT_STR;
	}
}
