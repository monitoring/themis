package anon.themis.measurements;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import anon.themis.automata.Atom;
import anon.themis.automata.Verdict;
import anon.themis.comm.protocol.Message;
import anon.themis.inference.Memory;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;
import anon.themis.utils.Opts;

/**
 * Standard Measures
 * 
 */
public privileged aspect GlobalRun extends Instrumentation {
	
	TimeUnit unit = TimeUnit.valueOf(Opts.getGlobal("THEMIS_BENCH_UNIT_RUNTIME", "MICROSECONDS"));
	HashMap<String,  Long> comms   = new HashMap<String, Long>();
	HashMap<Integer, Long> runtime = new HashMap<Integer, Long>();
	
	@Override
	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Full Run Stats");
		addMeasures(
			  new Measure("run_len"  , "Run Length" 		, 0 , Measures.replace)
			, new Measure("msg_num"  , "Number of Messages"	, 0L, Measures.addLong)
			, new Measure("msg_data" , "Data Exchanged"	, 0L, Measures.addLong)
			, new Measure("comps" 	 , "Components"		, alg.getComponentCount(),  Measures.replace)
			, new Measure("verdict"  , "Verdict"		, Verdict.NA, Measures.replace)
			, new Measure("alg"      , "Algorithm"	    , alg.getClass().getSimpleName())
		);
		comms.clear();
		runtime.clear();
	}
	
	void around(int t, Memory<Atom> obs) : Commons.monitorStep(t, obs)
	{
		Monitor mon 			= (Monitor) 			thisJoinPoint.getTarget();
		
		long start = System.nanoTime();
		proceed(t, obs);
		long end = System.nanoTime();
		Integer id = mon.getID();
		
		Long total = 0l;
		if(runtime.containsKey(id))
			total += runtime.get(id);
		total += end - start;
		runtime.put(id, total);
		
		return;
	}
	after(Integer to, Message m) : Commons.sendMessage(to, m)
	{
		update("msg_num" , 1L);
		update("msg_data", Config.sizeMessage(m, new Config.SizeDefault()));
		
		String s = m.getClass().getSimpleName();
		
		Long n = 0l;
		if(comms.containsKey(s))
			n += comms.get(s);
		n++;
		comms.put(s, n);
	}
	@Override
	protected void stepBegin(int t) {
		update("run_len", t);
	}
	
	@Override
	protected void stepReport(int t, ReportVerdict ver) {
		update("verdict", ver.getVerdict());
		update("run_len", t);
	}


	protected void printAll()
	{	
		super.printAll();
		
		//Extra Debug Measures
		Commons.printSection("Runtime Per Monitor (" + unit.toString() + ")");
		for(Entry<Integer, Long> entry : runtime.entrySet()) {
			Commons.printMeasureln("Monitor " + entry.getKey().toString(), Commons.FMT_NUM,
					unit.convert(entry.getValue(), TimeUnit.NANOSECONDS) );
		}
		Commons.printSection("Messages Details");
		for(Entry<String, Long> entry : comms.entrySet())
			Commons.printMeasureln(entry.getKey(), Commons.FMT_NUM, entry.getValue());
		
		comms.clear();
	}
}
