package anon.themis.measurements;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.LinkedList;

import anon.themis.tools.Analyze;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.Specification;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.SpecLTL;
import anon.themis.algorithms.choreography.Choreography;
import anon.themis.algorithms.choreography.MonitorNetwork;
import anon.themis.algorithms.choreography.Score;
import anon.themis.utils.Expressions;
import anon.themis.expressions.ltl.LTLExpression;

/**
 * Benchmarks Related to the LTL information
 * 
 */
public privileged aspect AutData extends Instrumentation {
	
	@Override
	protected void setupOnce(MonitoringAlgorithm alg) {
		Specification spec  = alg.getConfig().getSpec().get("root");
		Analyze.Stats stats = Analyze.analyze(spec);
		
		setDescription("Spec Information");
		
		addMeasures(
				new Measure("aut_states"  , "Total States", stats.states, true)
			,	new Measure("aut_labelavg"	  , "Average Label Size", stats.avgLabel, true)
			,	new Measure("aut_labelmax"	  , "Maximum Label Size", stats.maxLabel, true)
			,	new Measure("aut_pairsavg"	  , "Average Transition Pairs", stats.avgTransitionPairs, true)
			,	new Measure("aut_pairsmax"	  , "Maximum Transition Pairs", stats.maxTransitionPairs, true)
			,	new Measure("aut_outavg"	  , "Average Outbound Transitions", stats.avgOut, true)
			,	new Measure("aut_outmax"	  , "Maximum Outbound Transitions", stats.maxOut, true)
		);
		

	}
	
}
