package anon.themis.measurements;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import anon.themis.measurements.LTLData;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.utils.Opts;
import anon.themis.utils.SpecLoader;

/**
 * Persistence Layer 
 * Store in Database the measures per run
 */
public privileged aspect DatabaseStore {
	public static final String DB_NAME = "THEMIS_BENCH_DB";
	public static final String DEF_TAG = "THEMIS_BENCH_TAG";

	private static int run_hash = UUID.randomUUID().hashCode();
	
	//Should we create the database
	private boolean create = false;
	private String spec  = "";
	
	declare precedence: DatabaseStore, Instrumentation;

	//Generate an ID for the run
	private static long getRun() {
		return System.currentTimeMillis();
	}
	
	private String form = null;
	
	//Active benchmarks
	List<Instrumentation> benchmarks = new LinkedList<Instrumentation>();
	
	after(File f) :  call(* SpecLoader.loadSpec(File)) 
		&& args(f)
		&& withincode(* *.main(..))
	{
		spec = f.getPath();
	}
	
	before (MonitoringAlgorithm alg) : Commons.monitoringSetup(alg)
	{
		benchmarks.clear();
	}
	//Capture the active benchmarks
	after(Instrumentation b) : 
		call(void Instrumentation.setupOnce(MonitoringAlgorithm)) 
		&& target(b)
	{
		benchmarks.add(b);
	}
	
	before(MonitoringAlgorithm alg) : Commons.monitoringSetup(alg) {
		String database = Opts.getGlobal(DB_NAME, "");
		//Check Whether or not the database needs to be created
		if(!database.isEmpty())
			create = !(new File(database)).exists();
	}

	
	/**
	 * Once done monitoring save benchmarks in database
	 */
	after() returning() : Commons.monitoringMain() {
		
		String database = Opts.getGlobal(DB_NAME, "" );
		String tag		= Opts.getGlobal(DEF_TAG, "");
		
		//If set to empty do not log to db
		if(database.isEmpty()) return;
		
		Connection c = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:" + database);
	      c.setAutoCommit(true);
	      
		  if(create) { makeTable(c); create = false;	}
		    
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      return;
	    }

	    
	    StringBuilder keys = new StringBuilder("run_id, tag, run_hash, spec");
	    StringBuilder vals = new StringBuilder("?, ?, ?, ?");
		
		for(Instrumentation b : benchmarks) {
			for(String key : b.getAllMeasures().keySet()) {
				keys.append(", ").append(key);
				vals.append(", ?");
			}
		}
		
		StringBuilder sql = new StringBuilder("INSERT INTO bench (");
		sql.append(keys.toString()).append(") VALUES (")
		   .append(vals.toString()).append(")");
		
		
		try {
			PreparedStatement stmt = c.prepareStatement(sql.toString());

			int i = 5;
			stmt.setNull(1, Types.INTEGER);
			if(tag.isEmpty())
				stmt.setNull(2, Types.VARCHAR);
			else
				stmt.setString(2, tag);
			stmt.setInt(3, run_hash);
			stmt.setString(4, spec);
			
			for(Instrumentation b : benchmarks) {
				for(Measure m : b.getAllMeasures().values()) {
					stmt.setObject(i, m.getValue());
					i++;
				}
			}
			stmt.executeUpdate();
			c.close();
			
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		finally {
			
		}
		
	}

	private void makeTable(Connection c) {
		StringBuilder sql = new StringBuilder("CREATE TABLE bench ( run_id INTEGER PRIMARY KEY AUTOINCREMENT, tag TEXT, run_hash INT NOT NULL, spec TEXT NOT NULL");
		for(Instrumentation b : benchmarks) {
			for(Measure measure : b.getAllMeasures().values()) {
				String key = measure.getKey();
				Object val = measure.getValue();
				String type= getSQLType(val);
				
				sql.append(", ")
					.append(key)
					.append(" ")
					.append(type)
					.append(" NOT NULL");
			}
		}
		sql.append(")");
		try {
			Statement stmt = c.createStatement();
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			return;
		}
	}
	private String getSQLType(Object o) {
		if(o instanceof Integer || o instanceof Long)
			return "INT";
		if(o instanceof Float || o instanceof Double)
			return "REAL";
		else
			return "TEXT";
	}
}
