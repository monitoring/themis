package anon.themis.traces;

import anon.themis.automata.Atom;
import anon.themis.inference.Memory;
import anon.themis.inference.MemoryAtoms;
import anon.themis.symbols.AtomString;

/**
 * Reads a trace from a string
 * The timestamps are separated by >
 * 
 */
public class TraceString implements Trace {
	String[] trace;
	int t = 0;
	
	public TraceString(String fullTrace) {
		trace = fullTrace.split(">");
	}
	
	@Override
	public Memory<Atom> next() {
		Memory<Atom> mem = new MemoryAtoms();
		if(t >= trace.length || trace[t].isEmpty()) 
			return mem;
		
		String[] aps = trace[t].split(",");
		for(String ap : aps)
		{
			String[] obs = ap.split(":");
			mem.add(new AtomString(obs[0]),
					obs[1].equals("t"));
		}
		t++;
		return mem;
	}
	@Override
	public void stop() {
		trace = new String[0];
	}
}
