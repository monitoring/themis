package anon.themis.traces;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import anon.themis.automata.Atom;
import anon.themis.inference.Memory;
import anon.themis.inference.MemoryAtoms;
import anon.themis.symbols.AtomString;

/**
 * Reads a trace from a file
 * Each event is on a line
 * 
 * Example:
 * a0:t,b0:f,c0:t
 * a0:t,b0:t,c0:f
 * 
 */
public class TraceFile implements Trace {

	BufferedReader trace;
	FileReader fr;
	
	public TraceFile(File file) throws FileNotFoundException {
		fr	  = new FileReader(file);
		trace = new BufferedReader(fr);
	}
	
	@Override
	public Memory<Atom> next() {
		Memory<Atom> mem = new MemoryAtoms();
		String line      = null;
    try {
      line = trace.readLine();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
		if(line == null || line.isEmpty()) return mem;
		
		String[] aps = line.split(",");
		for(String ap : aps)
		{
			String[] obs = ap.split(":");
			mem.add(new AtomString(obs[0]),
					obs[1].equals("t"));
		}
		return mem;
	}

	@Override
	public void stop() {
		try {
			trace.close();
			fr.close();
			
		} catch (IOException e) {
			
		}
	}

	@Override
	protected void finalize() throws Throwable {
		trace.close();
		fr.close();
	}
}
