package anon.themis.traces;

import anon.themis.automata.Atom;
import anon.themis.inference.Memory;

/**
 * The trace interface
 * Similar to a stream of memory of atoms
 */
public interface Trace {
  /**
   * Get the next observation
   * @return
   */
	Memory<Atom> next();
	/**
	 * Signal no need to read any more
	 */
	void stop();
}
