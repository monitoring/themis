package anon.themis;

import anon.themis.utils.Opts;

/**
 * Environment Variables
 *
 */
public enum Env {
	  LTL2MON_BIN    ("THEMIS_LTL2MON_BIN")
	, TIMEOUT        ("THEMIS_SUBPROCESS_TIMEOUT")
	, SPOT_LTLFILT   ("THEMIS_SPOT_LTLFILT")
	;

	private String envname;
	
	private Env(String name) {
		this.envname = name;
	}
	/**
	 * Return the name of the environment variable
	 * @return
	 */
	public String getKey() {
		return envname;
	}
	/**
	 * Return the value of the environment variable
	 * @see Opts#getGlobal(String, String)
	 * @param def
	 * @return
	 */
	public String getValue(String def) {
		return Opts.getGlobal(envname, def);
	}
}
