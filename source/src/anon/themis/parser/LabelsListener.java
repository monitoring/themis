// Generated from Labels.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LabelsParser}.
 */
public interface LabelsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code L_EMPTY}
	 * labeled alternative in {@link LabelsParser#label}.
	 * @param ctx the parse tree
	 */
	void enterL_EMPTY(LabelsParser.L_EMPTYContext ctx);
	/**
	 * Exit a parse tree produced by the {@code L_EMPTY}
	 * labeled alternative in {@link LabelsParser#label}.
	 * @param ctx the parse tree
	 */
	void exitL_EMPTY(LabelsParser.L_EMPTYContext ctx);
	/**
	 * Enter a parse tree produced by the {@code L_event}
	 * labeled alternative in {@link LabelsParser#label}.
	 * @param ctx the parse tree
	 */
	void enterL_event(LabelsParser.L_eventContext ctx);
	/**
	 * Exit a parse tree produced by the {@code L_event}
	 * labeled alternative in {@link LabelsParser#label}.
	 * @param ctx the parse tree
	 */
	void exitL_event(LabelsParser.L_eventContext ctx);
	/**
	 * Enter a parse tree produced by the {@code E_COMPOSITE}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 */
	void enterE_COMPOSITE(LabelsParser.E_COMPOSITEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code E_COMPOSITE}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 */
	void exitE_COMPOSITE(LabelsParser.E_COMPOSITEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code EV_PAR}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 */
	void enterEV_PAR(LabelsParser.EV_PARContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EV_PAR}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 */
	void exitEV_PAR(LabelsParser.EV_PARContext ctx);
	/**
	 * Enter a parse tree produced by the {@code E_SINGLE}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 */
	void enterE_SINGLE(LabelsParser.E_SINGLEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code E_SINGLE}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 */
	void exitE_SINGLE(LabelsParser.E_SINGLEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code E_PAREN}
	 * labeled alternative in {@link LabelsParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterE_PAREN(LabelsParser.E_PARENContext ctx);
	/**
	 * Exit a parse tree produced by the {@code E_PAREN}
	 * labeled alternative in {@link LabelsParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitE_PAREN(LabelsParser.E_PARENContext ctx);
	/**
	 * Enter a parse tree produced by the {@code E_ATOM}
	 * labeled alternative in {@link LabelsParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterE_ATOM(LabelsParser.E_ATOMContext ctx);
	/**
	 * Exit a parse tree produced by the {@code E_ATOM}
	 * labeled alternative in {@link LabelsParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitE_ATOM(LabelsParser.E_ATOMContext ctx);
	/**
	 * Enter a parse tree produced by {@link LabelsParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(LabelsParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link LabelsParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(LabelsParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link LabelsParser#obligation}.
	 * @param ctx the parse tree
	 */
	void enterObligation(LabelsParser.ObligationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LabelsParser#obligation}.
	 * @param ctx the parse tree
	 */
	void exitObligation(LabelsParser.ObligationContext ctx);
}