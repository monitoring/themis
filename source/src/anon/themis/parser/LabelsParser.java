// Generated from Labels.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LabelsParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, STRING=8, INT=9, 
		WS=10;
	public static final int
		RULE_label = 0, RULE_event = 1, RULE_expr = 2, RULE_atom = 3, RULE_obligation = 4;
	public static final String[] ruleNames = {
		"label", "event", "expr", "atom", "obligation"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'(<empty>)'", "'&&'", "'('", "')'", "'<'", "','", "'>'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, "STRING", "INT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Labels.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LabelsParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LabelContext extends ParserRuleContext {
		public LabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_label; }
	 
		public LabelContext() { }
		public void copyFrom(LabelContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class L_eventContext extends LabelContext {
		public EventContext event() {
			return getRuleContext(EventContext.class,0);
		}
		public L_eventContext(LabelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterL_event(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitL_event(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitL_event(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class L_EMPTYContext extends LabelContext {
		public L_EMPTYContext(LabelContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterL_EMPTY(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitL_EMPTY(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitL_EMPTY(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LabelContext label() throws RecognitionException {
		LabelContext _localctx = new LabelContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_label);
		try {
			setState(12);
			switch (_input.LA(1)) {
			case T__0:
				_localctx = new L_EMPTYContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(10);
				match(T__0);
				}
				break;
			case T__2:
			case T__4:
			case STRING:
				_localctx = new L_eventContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(11);
				event(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EventContext extends ParserRuleContext {
		public EventContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_event; }
	 
		public EventContext() { }
		public void copyFrom(EventContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class E_COMPOSITEContext extends EventContext {
		public EventContext event() {
			return getRuleContext(EventContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public E_COMPOSITEContext(EventContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterE_COMPOSITE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitE_COMPOSITE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitE_COMPOSITE(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EV_PARContext extends EventContext {
		public EventContext event() {
			return getRuleContext(EventContext.class,0);
		}
		public EV_PARContext(EventContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterEV_PAR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitEV_PAR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitEV_PAR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class E_SINGLEContext extends EventContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public E_SINGLEContext(EventContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterE_SINGLE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitE_SINGLE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitE_SINGLE(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EventContext event() throws RecognitionException {
		return event(0);
	}

	private EventContext event(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EventContext _localctx = new EventContext(_ctx, _parentState);
		EventContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_event, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(20);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
				_localctx = new EV_PARContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(15);
				match(T__2);
				setState(16);
				event(0);
				setState(17);
				match(T__3);
				}
				break;
			case 2:
				{
				_localctx = new E_SINGLEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(19);
				expr();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(27);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new E_COMPOSITEContext(new EventContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_event);
					setState(22);
					if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
					setState(23);
					match(T__1);
					setState(24);
					expr();
					}
					} 
				}
				setState(29);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class E_PARENContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public E_PARENContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterE_PAREN(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitE_PAREN(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitE_PAREN(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class E_ATOMContext extends ExprContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public E_ATOMContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterE_ATOM(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitE_ATOM(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitE_ATOM(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_expr);
		try {
			setState(35);
			switch (_input.LA(1)) {
			case T__2:
				_localctx = new E_PARENContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(30);
				match(T__2);
				setState(31);
				expr();
				setState(32);
				match(T__3);
				}
				break;
			case T__4:
			case STRING:
				_localctx = new E_ATOMContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(34);
				atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public ObligationContext obligation() {
			return getRuleContext(ObligationContext.class,0);
		}
		public TerminalNode STRING() { return getToken(LabelsParser.STRING, 0); }
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_atom);
		try {
			setState(39);
			switch (_input.LA(1)) {
			case T__4:
				enterOuterAlt(_localctx, 1);
				{
				setState(37);
				obligation();
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(38);
				match(STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObligationContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(LabelsParser.INT, 0); }
		public TerminalNode STRING() { return getToken(LabelsParser.STRING, 0); }
		public ObligationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_obligation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).enterObligation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LabelsListener ) ((LabelsListener)listener).exitObligation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LabelsVisitor ) return ((LabelsVisitor<? extends T>)visitor).visitObligation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObligationContext obligation() throws RecognitionException {
		ObligationContext _localctx = new ObligationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_obligation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			match(T__4);
			setState(42);
			match(INT);
			setState(43);
			match(T__5);
			setState(44);
			match(STRING);
			setState(45);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return event_sempred((EventContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean event_sempred(EventContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\f\62\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\5\2\17\n\2\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\5\3\27\n\3\3\3\3\3\3\3\7\3\34\n\3\f\3\16\3\37\13\3\3\4\3\4\3\4\3\4"+
		"\3\4\5\4&\n\4\3\5\3\5\5\5*\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\2\3\4\7\2\4"+
		"\6\b\n\2\2\61\2\16\3\2\2\2\4\26\3\2\2\2\6%\3\2\2\2\b)\3\2\2\2\n+\3\2\2"+
		"\2\f\17\7\3\2\2\r\17\5\4\3\2\16\f\3\2\2\2\16\r\3\2\2\2\17\3\3\2\2\2\20"+
		"\21\b\3\1\2\21\22\7\5\2\2\22\23\5\4\3\2\23\24\7\6\2\2\24\27\3\2\2\2\25"+
		"\27\5\6\4\2\26\20\3\2\2\2\26\25\3\2\2\2\27\35\3\2\2\2\30\31\f\5\2\2\31"+
		"\32\7\4\2\2\32\34\5\6\4\2\33\30\3\2\2\2\34\37\3\2\2\2\35\33\3\2\2\2\35"+
		"\36\3\2\2\2\36\5\3\2\2\2\37\35\3\2\2\2 !\7\5\2\2!\"\5\6\4\2\"#\7\6\2\2"+
		"#&\3\2\2\2$&\5\b\5\2% \3\2\2\2%$\3\2\2\2&\7\3\2\2\2\'*\5\n\6\2(*\7\n\2"+
		"\2)\'\3\2\2\2)(\3\2\2\2*\t\3\2\2\2+,\7\7\2\2,-\7\13\2\2-.\7\b\2\2./\7"+
		"\n\2\2/\60\7\t\2\2\60\13\3\2\2\2\7\16\26\35%)";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}