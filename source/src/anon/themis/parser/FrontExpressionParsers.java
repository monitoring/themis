package anon.themis.parser;

import java.io.IOException;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import anon.themis.automata.Event;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.BoolSym;
import anon.themis.expressions.ltl.LTLExpression;

/**
 * Front for the Expression Parsers
 * 
 */
public class FrontExpressionParsers {
  
  /**
   * The Antlr Error Listener
   * We override it so that it causes the exception to be thrown to the caller
   * This basically means that if the parse fails an exception will be raised
   */
  public static ANTLRErrorListener listener = new BaseErrorListener() {
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
        throws ParseCancellationException {

      throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
    }
  };
  /**
   * Add the listener to a parser
   * @param parser
   */
  public static void addParserError(Parser parser) {
    parser.removeErrorListeners();
    parser.addErrorListener(listener);
  }

  public static LabelsParser getParserBool(String expr) throws IOException
  {
    ANTLRInputStream input = new ANTLRInputStream(expr);
    LabelsLexer lexer = new LabelsLexer(input);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    LabelsParser parser = new LabelsParser(tokens);
    addParserError(parser);
    return parser;
  }
  public static LTLParser getParserLTL(String expr) throws IOException
  {
    ANTLRInputStream input = new ANTLRInputStream(expr);
    LTLLexer lexer = new LTLLexer(input);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    LTLParser parser = new LTLParser(tokens);
    addParserError(parser);
    return parser;
  }
  public static BoolParser getParserBoolFILT(String expr) throws IOException
  {
    ANTLRInputStream input = new ANTLRInputStream(expr);
    BoolLexer lexer = new BoolLexer(input);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    BoolParser parser = new BoolParser(tokens);
    addParserError(parser);
    return parser;
  }

  public static BoolExpression parseStringExpression(String expr) {
    LabelsParser p2;
    try {
      p2 = getParserBool(expr);
    } catch (IOException e1) {
      return null;
    }
    LabelsVisitor<BoolExpression> v = new VisitorLabelExpression();

    return v.visit(p2.label());
  }

  public static Event parseStringEvent(String expr) {
    LabelsParser p2;
    try {
      p2 = getParserBool(expr);
    } catch (IOException e1) {
      return null;
    }
    LabelsVisitor<Object> v = new VisitorLabelEvent();

    return (Event) v.visit(p2.label());

  }
  public static BoolExpression parseStringBoolFILT(String expr, Map<String, BoolSym> syms) {
    BoolParser p2;
    try {
      p2 = getParserBoolFILT(expr);
    } catch (IOException e1) {
      return null;
    }
    VisitorBoolFilt v = new VisitorBoolFilt(syms);

    return v.visit(p2.expr());
  }
  public static LTLExpression parseStringLTL(String expr) {
    LTLParser p2;
    try {
      p2 = getParserLTL(expr);
    } catch (IOException e1) {
      return null;
    }
    LTLVisitor<LTLExpression> v = new VisitorLTLExpression();

    return v.visit(p2.ltlexpr());
  }
}
