// Generated from Bool.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BoolParser}.
 */
public interface BoolListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code BOOL_AND}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOL_AND(BoolParser.BOOL_ANDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOL_AND}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOL_AND(BoolParser.BOOL_ANDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOL_PAREN}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOL_PAREN(BoolParser.BOOL_PARENContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOL_PAREN}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOL_PAREN(BoolParser.BOOL_PARENContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOL_TRUE}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOL_TRUE(BoolParser.BOOL_TRUEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOL_TRUE}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOL_TRUE(BoolParser.BOOL_TRUEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOL_SYM}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOL_SYM(BoolParser.BOOL_SYMContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOL_SYM}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOL_SYM(BoolParser.BOOL_SYMContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOl_NOT}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOl_NOT(BoolParser.BOOl_NOTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOl_NOT}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOl_NOT(BoolParser.BOOl_NOTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOl_FALSE}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOl_FALSE(BoolParser.BOOl_FALSEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOl_FALSE}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOl_FALSE(BoolParser.BOOl_FALSEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOL_OR}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBOOL_OR(BoolParser.BOOL_ORContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOL_OR}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBOOL_OR(BoolParser.BOOL_ORContext ctx);
}