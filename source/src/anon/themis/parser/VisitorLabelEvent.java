package anon.themis.parser;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.parser.LabelsParser.AtomContext;
import anon.themis.parser.LabelsParser.EV_PARContext;
import anon.themis.parser.LabelsParser.E_ATOMContext;
import anon.themis.parser.LabelsParser.E_COMPOSITEContext;
import anon.themis.parser.LabelsParser.E_PARENContext;
import anon.themis.parser.LabelsParser.E_SINGLEContext;
import anon.themis.parser.LabelsParser.L_EMPTYContext;
import anon.themis.parser.LabelsParser.L_eventContext;
import anon.themis.parser.LabelsParser.ObligationContext;
import anon.themis.symbols.AtomObligation;
import anon.themis.symbols.AtomString;


/**
 * Generates an event from parsing a Label from LTL2MON dot
 * 
 */
public class VisitorLabelEvent extends LabelsBaseVisitor<Object>{
	private Event ev;
	@Override
	public Object visitL_EMPTY(L_EMPTYContext ctx) {
		Event event = new Event();
		event.addObservation(Atom.empty());
		return event;
	}
	@Override
	public Object visitL_event(L_eventContext ctx) {
		ev = new Event();
		visit(ctx.event());
		return ev;
	}

	@Override
	public Object visitEV_PAR(EV_PARContext ctx) {
		return visit(ctx.event());
	}
	@Override
	public Object visitE_SINGLE(E_SINGLEContext ctx) {
		return visit(ctx.expr());
	}
	@Override
	public Object visitE_COMPOSITE(E_COMPOSITEContext ctx) {
		visit(ctx.event());
		visit(ctx.expr());
		return null;
	}

	@Override
	public Object visitE_PAREN(E_PARENContext ctx) {
		return visit(ctx.expr());
	}
	@Override
	public Object visitE_ATOM(E_ATOMContext ctx) {
		return visit(ctx.atom());
	}
	@Override
	public Object visitAtom(AtomContext ctx) {
		if(ctx.obligation() != null)
			visit(ctx.obligation());
		else
			ev.addObservation(new AtomString(ctx.STRING().getText()));
		return null;
	}
	@Override
	public Object visitObligation(ObligationContext ctx) {
		ev.addObservation(new AtomObligation(
				new AtomString(ctx.STRING().getText()), 
				Integer.parseInt(ctx.INT().getText()))
			);
		return null;
	}
}
