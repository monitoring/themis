// Generated from Bool.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BoolParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BoolVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code BOOL_AND}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL_AND(BoolParser.BOOL_ANDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOL_PAREN}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL_PAREN(BoolParser.BOOL_PARENContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOL_TRUE}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL_TRUE(BoolParser.BOOL_TRUEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOL_SYM}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL_SYM(BoolParser.BOOL_SYMContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOl_NOT}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOl_NOT(BoolParser.BOOl_NOTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOl_FALSE}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOl_FALSE(BoolParser.BOOl_FALSEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOL_OR}
	 * labeled alternative in {@link BoolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL_OR(BoolParser.BOOL_ORContext ctx);
}