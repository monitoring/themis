// Generated from LTL.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LTLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LTLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code LTL_PAREN}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_PAREN(LTLParser.LTL_PARENContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_SYM}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_SYM(LTLParser.LTL_SYMContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_FINALLY}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_FINALLY(LTLParser.LTL_FINALLYContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_AND}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_AND(LTLParser.LTL_ANDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_EQUIV}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_EQUIV(LTLParser.LTL_EQUIVContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_RELEASE}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_RELEASE(LTLParser.LTL_RELEASEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_NEG}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_NEG(LTLParser.LTL_NEGContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_GLOBAL}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_GLOBAL(LTLParser.LTL_GLOBALContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_IMPLIES}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_IMPLIES(LTLParser.LTL_IMPLIESContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_NEXT}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_NEXT(LTLParser.LTL_NEXTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_OR}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_OR(LTLParser.LTL_ORContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LTL_UNTIL}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLTL_UNTIL(LTLParser.LTL_UNTILContext ctx);
	/**
	 * Visit a parse tree produced by {@link LTLParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtom(LTLParser.AtomContext ctx);
}