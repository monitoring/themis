package anon.themis.parser;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.expressions.bool.Atomic;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.EventExpr;
import anon.themis.parser.LabelsParser.AtomContext;
import anon.themis.parser.LabelsParser.EV_PARContext;
import anon.themis.parser.LabelsParser.E_ATOMContext;
import anon.themis.parser.LabelsParser.E_COMPOSITEContext;
import anon.themis.parser.LabelsParser.E_PARENContext;
import anon.themis.parser.LabelsParser.E_SINGLEContext;
import anon.themis.parser.LabelsParser.L_EMPTYContext;
import anon.themis.parser.LabelsParser.ObligationContext;
import anon.themis.symbols.AtomObligation;
import anon.themis.symbols.AtomString;


/**
 * Generates a boolean expression from parsing a Label from LTL2MON dot
 * 
 */
public class VisitorLabelExpression extends LabelsBaseVisitor<BoolExpression>{
	private Event ev;
	private int depth = 0;
	@Override
	public BoolExpression visitL_EMPTY(L_EMPTYContext ctx) {
		return new Atomic(Atom.empty());
	}

	@Override
	public BoolExpression visitEV_PAR(EV_PARContext ctx) {
		return visit(ctx.event());
	}
	@Override
	public BoolExpression visitE_SINGLE(E_SINGLEContext ctx) {
		return visit(ctx.expr());
	}
	@Override
	public BoolExpression visitE_COMPOSITE(E_COMPOSITEContext ctx) {
		if(depth == 0) ev = new Event();
		Atomic ex  = (Atomic) visit(ctx.expr());
		depth++;
		BoolExpression e = visit(ctx.event());
		if(e != null)
			ev.addObservation(((Atomic) e).getAtom());
		depth--;
		ev.addObservation(ex.getAtom());
		if(depth == 0)
		{
			return new EventExpr(ev);
		}
		else
			return null;
	}

	@Override
	public BoolExpression visitE_PAREN(E_PARENContext ctx) {
		return visit(ctx.expr());
	}
	@Override
	public BoolExpression visitE_ATOM(E_ATOMContext ctx) {
		return visit(ctx.atom());
	}
	@Override
	public BoolExpression visitAtom(AtomContext ctx) {
		if(ctx.obligation() != null)
			return visit(ctx.obligation());
		else
			return new Atomic(new AtomString(ctx.STRING().getText()));
	}
	@Override
	public BoolExpression visitObligation(ObligationContext ctx) {
		return new Atomic(new AtomObligation(
				new AtomString(ctx.STRING().getText()), 
				Integer.parseInt(ctx.INT().getText()))
			);
	}
}
