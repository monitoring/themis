// Generated from LTL.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LTLParser}.
 */
public interface LTLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code LTL_PAREN}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_PAREN(LTLParser.LTL_PARENContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_PAREN}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_PAREN(LTLParser.LTL_PARENContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_SYM}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_SYM(LTLParser.LTL_SYMContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_SYM}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_SYM(LTLParser.LTL_SYMContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_FINALLY}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_FINALLY(LTLParser.LTL_FINALLYContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_FINALLY}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_FINALLY(LTLParser.LTL_FINALLYContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_AND}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_AND(LTLParser.LTL_ANDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_AND}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_AND(LTLParser.LTL_ANDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_EQUIV}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_EQUIV(LTLParser.LTL_EQUIVContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_EQUIV}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_EQUIV(LTLParser.LTL_EQUIVContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_RELEASE}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_RELEASE(LTLParser.LTL_RELEASEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_RELEASE}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_RELEASE(LTLParser.LTL_RELEASEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_NEG}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_NEG(LTLParser.LTL_NEGContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_NEG}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_NEG(LTLParser.LTL_NEGContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_GLOBAL}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_GLOBAL(LTLParser.LTL_GLOBALContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_GLOBAL}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_GLOBAL(LTLParser.LTL_GLOBALContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_IMPLIES}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_IMPLIES(LTLParser.LTL_IMPLIESContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_IMPLIES}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_IMPLIES(LTLParser.LTL_IMPLIESContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_NEXT}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_NEXT(LTLParser.LTL_NEXTContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_NEXT}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_NEXT(LTLParser.LTL_NEXTContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_OR}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_OR(LTLParser.LTL_ORContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_OR}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_OR(LTLParser.LTL_ORContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LTL_UNTIL}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void enterLTL_UNTIL(LTLParser.LTL_UNTILContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LTL_UNTIL}
	 * labeled alternative in {@link LTLParser#ltlexpr}.
	 * @param ctx the parse tree
	 */
	void exitLTL_UNTIL(LTLParser.LTL_UNTILContext ctx);
	/**
	 * Enter a parse tree produced by {@link LTLParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(LTLParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link LTLParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(LTLParser.AtomContext ctx);
}