// Generated from Labels.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LabelsParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LabelsVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code L_EMPTY}
	 * labeled alternative in {@link LabelsParser#label}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitL_EMPTY(LabelsParser.L_EMPTYContext ctx);
	/**
	 * Visit a parse tree produced by the {@code L_event}
	 * labeled alternative in {@link LabelsParser#label}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitL_event(LabelsParser.L_eventContext ctx);
	/**
	 * Visit a parse tree produced by the {@code E_COMPOSITE}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitE_COMPOSITE(LabelsParser.E_COMPOSITEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EV_PAR}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEV_PAR(LabelsParser.EV_PARContext ctx);
	/**
	 * Visit a parse tree produced by the {@code E_SINGLE}
	 * labeled alternative in {@link LabelsParser#event}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitE_SINGLE(LabelsParser.E_SINGLEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code E_PAREN}
	 * labeled alternative in {@link LabelsParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitE_PAREN(LabelsParser.E_PARENContext ctx);
	/**
	 * Visit a parse tree produced by the {@code E_ATOM}
	 * labeled alternative in {@link LabelsParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitE_ATOM(LabelsParser.E_ATOMContext ctx);
	/**
	 * Visit a parse tree produced by {@link LabelsParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtom(LabelsParser.AtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link LabelsParser#obligation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObligation(LabelsParser.ObligationContext ctx);
}