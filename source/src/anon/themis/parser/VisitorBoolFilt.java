package anon.themis.parser;

import java.util.HashMap;
import java.util.Map;

import anon.themis.expressions.bool.Atomic;
import anon.themis.expressions.bool.BoolAnd;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.BoolNot;
import anon.themis.expressions.bool.BoolOr;
import anon.themis.expressions.bool.BoolSym;
import anon.themis.expressions.bool.False;
import anon.themis.expressions.bool.True;
import anon.themis.parser.BoolParser.BOOL_ANDContext;
import anon.themis.parser.BoolParser.BOOL_ORContext;
import anon.themis.parser.BoolParser.BOOL_PARENContext;
import anon.themis.parser.BoolParser.BOOL_SYMContext;
import anon.themis.parser.BoolParser.BOOL_TRUEContext;
import anon.themis.parser.BoolParser.BOOl_FALSEContext;
import anon.themis.parser.BoolParser.BOOl_NOTContext;
import anon.themis.symbols.AtomString;

/**
 * Visitor that recreate a boolean expression from the output of ltlfilt
 * Optionally if a symbol map is given, strings that match the key
 * will be replaced by the boolean symbols provided in the map.
 * This is useful for encoded more complex atoms in strings.
 * 
 */
public class VisitorBoolFilt extends BoolBaseVisitor<BoolExpression>{
	private Map<String, BoolSym> syms = new HashMap<String, BoolSym>();

	public VisitorBoolFilt() {
	}
	/**
	 * By specifying a symbol map elements that match the key
	 * will be associated with the symbol in the map
	 * @param syms
	 */
	public VisitorBoolFilt(Map<String, BoolSym> syms) {
		this.syms = syms;
	}
	
	@Override
	public BoolExpression visitBOOL_TRUE(BOOL_TRUEContext ctx) {
		return new True();
	}
	@Override
	public BoolExpression visitBOOl_FALSE(BOOl_FALSEContext ctx) {
		return new False();
	}
	
	@Override
	public BoolExpression visitBOOL_PAREN(BOOL_PARENContext ctx) {
		return visit(ctx.expr());
	}
	
	@Override
	public BoolExpression visitBOOL_SYM(BOOL_SYMContext ctx) {
		String txt = ctx.getText();
		if(syms.containsKey(txt))
			return syms.get(txt);
		else
			return new Atomic(new AtomString(txt));
	}
	@Override
	public BoolExpression visitBOOL_AND(BOOL_ANDContext ctx) {
		return new BoolAnd(visit(ctx.expr(0)), visit(ctx.expr(1)));
	}
	@Override
	public BoolExpression visitBOOL_OR(BOOL_ORContext ctx) {
		return new BoolOr(visit(ctx.expr(0)), visit(ctx.expr(1)));
	}
	@Override
	public BoolExpression visitBOOl_NOT(BOOl_NOTContext ctx) {
		return new BoolNot(visit(ctx.expr()));
	}

}
