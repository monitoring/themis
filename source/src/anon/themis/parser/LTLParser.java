// Generated from LTL.g4 by ANTLR 4.5.1
package anon.themis.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LTLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, OPNEG=6, OPG=7, OPF=8, OPX=9, 
		OPAND=10, OPOR=11, OPR=12, STRING=13, INT=14, WS=15;
	public static final int
		RULE_ltlexpr = 0, RULE_atom = 1;
	public static final String[] ruleNames = {
		"ltlexpr", "atom"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'<->'", "'->'", "'U'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "OPNEG", "OPG", "OPF", "OPX", "OPAND", 
		"OPOR", "OPR", "STRING", "INT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "LTL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LTLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LtlexprContext extends ParserRuleContext {
		public LtlexprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ltlexpr; }
	 
		public LtlexprContext() { }
		public void copyFrom(LtlexprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LTL_PARENContext extends LtlexprContext {
		public LtlexprContext ltlexpr() {
			return getRuleContext(LtlexprContext.class,0);
		}
		public LTL_PARENContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_PAREN(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_PAREN(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_PAREN(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_SYMContext extends LtlexprContext {
		public TerminalNode STRING() { return getToken(LTLParser.STRING, 0); }
		public LTL_SYMContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_SYM(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_SYM(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_SYM(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_FINALLYContext extends LtlexprContext {
		public TerminalNode OPF() { return getToken(LTLParser.OPF, 0); }
		public LtlexprContext ltlexpr() {
			return getRuleContext(LtlexprContext.class,0);
		}
		public LTL_FINALLYContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_FINALLY(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_FINALLY(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_FINALLY(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_ANDContext extends LtlexprContext {
		public List<LtlexprContext> ltlexpr() {
			return getRuleContexts(LtlexprContext.class);
		}
		public LtlexprContext ltlexpr(int i) {
			return getRuleContext(LtlexprContext.class,i);
		}
		public TerminalNode OPAND() { return getToken(LTLParser.OPAND, 0); }
		public LTL_ANDContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_AND(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_AND(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_AND(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_EQUIVContext extends LtlexprContext {
		public List<LtlexprContext> ltlexpr() {
			return getRuleContexts(LtlexprContext.class);
		}
		public LtlexprContext ltlexpr(int i) {
			return getRuleContext(LtlexprContext.class,i);
		}
		public LTL_EQUIVContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_EQUIV(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_EQUIV(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_EQUIV(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_RELEASEContext extends LtlexprContext {
		public List<LtlexprContext> ltlexpr() {
			return getRuleContexts(LtlexprContext.class);
		}
		public LtlexprContext ltlexpr(int i) {
			return getRuleContext(LtlexprContext.class,i);
		}
		public TerminalNode OPR() { return getToken(LTLParser.OPR, 0); }
		public LTL_RELEASEContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_RELEASE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_RELEASE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_RELEASE(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_NEGContext extends LtlexprContext {
		public TerminalNode OPNEG() { return getToken(LTLParser.OPNEG, 0); }
		public LtlexprContext ltlexpr() {
			return getRuleContext(LtlexprContext.class,0);
		}
		public LTL_NEGContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_NEG(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_NEG(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_NEG(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_GLOBALContext extends LtlexprContext {
		public TerminalNode OPG() { return getToken(LTLParser.OPG, 0); }
		public LtlexprContext ltlexpr() {
			return getRuleContext(LtlexprContext.class,0);
		}
		public LTL_GLOBALContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_GLOBAL(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_GLOBAL(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_GLOBAL(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_IMPLIESContext extends LtlexprContext {
		public List<LtlexprContext> ltlexpr() {
			return getRuleContexts(LtlexprContext.class);
		}
		public LtlexprContext ltlexpr(int i) {
			return getRuleContext(LtlexprContext.class,i);
		}
		public LTL_IMPLIESContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_IMPLIES(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_IMPLIES(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_IMPLIES(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_NEXTContext extends LtlexprContext {
		public TerminalNode OPX() { return getToken(LTLParser.OPX, 0); }
		public LtlexprContext ltlexpr() {
			return getRuleContext(LtlexprContext.class,0);
		}
		public LTL_NEXTContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_NEXT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_NEXT(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_NEXT(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_ORContext extends LtlexprContext {
		public List<LtlexprContext> ltlexpr() {
			return getRuleContexts(LtlexprContext.class);
		}
		public LtlexprContext ltlexpr(int i) {
			return getRuleContext(LtlexprContext.class,i);
		}
		public TerminalNode OPOR() { return getToken(LTLParser.OPOR, 0); }
		public LTL_ORContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_OR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_OR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_OR(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LTL_UNTILContext extends LtlexprContext {
		public List<LtlexprContext> ltlexpr() {
			return getRuleContexts(LtlexprContext.class);
		}
		public LtlexprContext ltlexpr(int i) {
			return getRuleContext(LtlexprContext.class,i);
		}
		public LTL_UNTILContext(LtlexprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterLTL_UNTIL(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitLTL_UNTIL(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitLTL_UNTIL(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LtlexprContext ltlexpr() throws RecognitionException {
		return ltlexpr(0);
	}

	private LtlexprContext ltlexpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		LtlexprContext _localctx = new LtlexprContext(_ctx, _parentState);
		LtlexprContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_ltlexpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(18);
			switch (_input.LA(1)) {
			case OPNEG:
				{
				_localctx = new LTL_NEGContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(5);
				match(OPNEG);
				setState(6);
				ltlexpr(11);
				}
				break;
			case OPG:
				{
				_localctx = new LTL_GLOBALContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(7);
				match(OPG);
				setState(8);
				ltlexpr(10);
				}
				break;
			case OPF:
				{
				_localctx = new LTL_FINALLYContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(9);
				match(OPF);
				setState(10);
				ltlexpr(9);
				}
				break;
			case OPX:
				{
				_localctx = new LTL_NEXTContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(11);
				match(OPX);
				setState(12);
				ltlexpr(8);
				}
				break;
			case T__0:
				{
				_localctx = new LTL_PARENContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(13);
				match(T__0);
				setState(14);
				ltlexpr(0);
				setState(15);
				match(T__1);
				}
				break;
			case STRING:
				{
				_localctx = new LTL_SYMContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(17);
				match(STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(40);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(38);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new LTL_ANDContext(new LtlexprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ltlexpr);
						setState(20);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(21);
						match(OPAND);
						setState(22);
						ltlexpr(8);
						}
						break;
					case 2:
						{
						_localctx = new LTL_ORContext(new LtlexprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ltlexpr);
						setState(23);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(24);
						match(OPOR);
						setState(25);
						ltlexpr(7);
						}
						break;
					case 3:
						{
						_localctx = new LTL_EQUIVContext(new LtlexprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ltlexpr);
						setState(26);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(27);
						match(T__2);
						setState(28);
						ltlexpr(6);
						}
						break;
					case 4:
						{
						_localctx = new LTL_IMPLIESContext(new LtlexprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ltlexpr);
						setState(29);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(30);
						match(T__3);
						setState(31);
						ltlexpr(5);
						}
						break;
					case 5:
						{
						_localctx = new LTL_UNTILContext(new LtlexprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ltlexpr);
						setState(32);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(33);
						match(T__4);
						setState(34);
						ltlexpr(4);
						}
						break;
					case 6:
						{
						_localctx = new LTL_RELEASEContext(new LtlexprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ltlexpr);
						setState(35);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(36);
						match(OPR);
						setState(37);
						ltlexpr(3);
						}
						break;
					}
					} 
				}
				setState(42);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(LTLParser.STRING, 0); }
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LTLListener ) ((LTLListener)listener).exitAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LTLVisitor ) return ((LTLVisitor<? extends T>)visitor).visitAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_atom);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0:
			return ltlexpr_sempred((LtlexprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean ltlexpr_sempred(LtlexprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 7);
		case 1:
			return precpred(_ctx, 6);
		case 2:
			return precpred(_ctx, 5);
		case 3:
			return precpred(_ctx, 4);
		case 4:
			return precpred(_ctx, 3);
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\21\60\4\2\t\2\4\3"+
		"\t\3\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\25\n"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\7\2)\n\2\f\2\16\2,\13\2\3\3\3\3\3\3\2\3\2\4\2\4\2\28\2\24\3\2\2\2"+
		"\4-\3\2\2\2\6\7\b\2\1\2\7\b\7\b\2\2\b\25\5\2\2\r\t\n\7\t\2\2\n\25\5\2"+
		"\2\f\13\f\7\n\2\2\f\25\5\2\2\13\r\16\7\13\2\2\16\25\5\2\2\n\17\20\7\3"+
		"\2\2\20\21\5\2\2\2\21\22\7\4\2\2\22\25\3\2\2\2\23\25\7\17\2\2\24\6\3\2"+
		"\2\2\24\t\3\2\2\2\24\13\3\2\2\2\24\r\3\2\2\2\24\17\3\2\2\2\24\23\3\2\2"+
		"\2\25*\3\2\2\2\26\27\f\t\2\2\27\30\7\f\2\2\30)\5\2\2\n\31\32\f\b\2\2\32"+
		"\33\7\r\2\2\33)\5\2\2\t\34\35\f\7\2\2\35\36\7\5\2\2\36)\5\2\2\b\37 \f"+
		"\6\2\2 !\7\6\2\2!)\5\2\2\7\"#\f\5\2\2#$\7\7\2\2$)\5\2\2\6%&\f\4\2\2&\'"+
		"\7\16\2\2\')\5\2\2\5(\26\3\2\2\2(\31\3\2\2\2(\34\3\2\2\2(\37\3\2\2\2("+
		"\"\3\2\2\2(%\3\2\2\2),\3\2\2\2*(\3\2\2\2*+\3\2\2\2+\3\3\2\2\2,*\3\2\2"+
		"\2-.\7\17\2\2.\5\3\2\2\2\5\24(*";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}