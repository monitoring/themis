package anon.themis.inference;

import java.util.Collection;
import java.util.HashSet;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
import anon.themis.symbols.AtomNegation;

/**
 * A memory that stores atoms
 * 
 */
public class MemoryAtoms implements Memory<Atom> {

	HashSet<Atom> mem;
	
	public MemoryAtoms() {
		mem = new HashSet<Atom>();
	}
	
	/**
	 * Add an observation to memory
	 * Observations are grouped into events using
	 * Atom.group() -> Object (event key in memory)
	 * @param a
	 * @param v
	 */
	public void add(Atom a, boolean observed)
	{
		if(!observed)
			a = new AtomNegation(a);
		mem.add(a);
	}
	/**
	 * Has {a} been observed?
	 * Returns Verdict.NA if none is found
	 * @param a
	 * @return
	 */
	public Verdict get(Atom a)
	{
		if(mem.contains(new AtomNegation(a)))
			return Verdict.FALSE;
		else if(!mem.contains(a))
			return Verdict.NA;
		else
			return Verdict.TRUE;
	}
	/**
	 * Is the Event found in the memory?
	 * TRUE		: if all sub atoms are in the memory
	 * NA   	: if one or more are missing
	 * FALSE	: if a negation is encoutered
	 * @param e
	 * @return
	 */
	public Verdict get(Event e)
	{
		// An Empty event is always associated with false
		// To designate special no-observations an event can contain Atom.empty()
		// And it will be handled via Event.hasEmpty()
		if(e.empty()) return Verdict.FALSE;

		for(Atom a : e.getAtoms()) 
		{
			Verdict v = get(a);
			switch(v){
				case FALSE:
						return Verdict.FALSE;
				case NA:
						return Verdict.NA;
			default:
				break;
			}
		}
		return Verdict.TRUE;
	}
	/**
	 * Hard Clone
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		MemoryAtoms m = new MemoryAtoms();
		m.mem = (HashSet<Atom>) mem.clone();
		return m;
	}
	/**
	 * Return all atoms in Memory
	 * @return
	 */
	public Collection<Atom> getData()
	{
		return mem;
	}
	/**
	 * Memory contains no records
	 * @return
	 */
	public boolean isEmpty () {
		return mem.isEmpty();
	}
	@Override
	public String toString() {
		return mem.toString();
	}
	
	/**
	 * Add all atoms from Memory m
	 * Merges observations of events with same key
	 * @param m
	 */
	public void merge(Memory<Atom> m)
	{
		mem.addAll(m.getData());
	}
	/**
	 * Empty Memory
	 */
	public void clear(){
		mem.clear();
	}
	/**
	 * Remove specific keys from Memory
	 * @param keys
	 */
	public void remove(Collection<Atom> keys)
	{
		
		mem.removeAll(keys);
	}
	/**
	 * Groups memory elements by a key
	 * An atom may specify null to group by its string content
	 * Otherwise it can specify a key such as timestamp or component id etc.
	 * Memory events are formed using key:
	 * 	atoms with the same key belong to the same event
	 * @param a
	 * @return
	 */
	public static Object getKey(Atom a)
	{
		Object key = a.group();
		if(key == null)
			return a.observe();
		else
			return key;
	}


}
