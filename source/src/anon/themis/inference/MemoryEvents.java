package anon.themis.inference;

import java.util.Collection;
import java.util.HashMap;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.automata.Verdict;
/**
 * A memory that stores events
 * 
 */
public class MemoryEvents implements Memory<Event> {

	HashMap<Object, Event> mem;
	
	public MemoryEvents() {
		mem = new HashMap<Object, Event>();
	}
	
	/**
	 * Add an observation to memory
	 * Observations are grouped into events using
	 * Atom.group() -> Object (event key in memory)
	 * @param a
	 * @param v
	 */
	public void add(Atom a, boolean observed)
	{
		Object group = getKey(a);
		if(!mem.containsKey(group))
			mem.put(group, new Event());
			
		mem.get(group).addObservation(a);
	}
	/**
	 * Has {a} been observed?
	 * Returns Verdict.NA if none is found
	 * @param a
	 * @return
	 */
	public Verdict get(Atom a)
	{
		Object group = getKey(a);
		if(!mem.containsKey(group))
			return Verdict.NA;
		return Verdict.fromBoolean(mem.get(group).observed(a));
	}
	/**
	 * Is the Event found in the memory?
	 * Returns Verdict.NA if Event not in memory
	 * i.e there has been no observations grouped by the event key
	 * @param e
	 * @return
	 */
	public Verdict get(Event e)
	{
		// An Empty event is always associated with false
		// To designate special no-observations an event can contain Atom.empty()
		// And it will be handled via Event.hasEmpty()
		if(e.empty()) return Verdict.FALSE;
		Object group = getKey(e.getAtoms().iterator().next());
		
		//Is it really empty, i.e event does not exist in memory | not observed!
		if(!mem.containsKey(group))
			return Verdict.NA;
		
		Event stored = mem.get(group);
		boolean v = stored.equals(e);
		//Handle special "empty" event
		if(!v && e.hasEmpty() && stored.empty())
			v = true;
		
		return Verdict.fromBoolean(v);
	}
	/**
	 * Hard Clone
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		MemoryEvents m = new MemoryEvents();
		m.mem = (HashMap<Object, Event>) mem.clone();
		return m;
	}
	/**
	 * Return all events in Memory
	 * @return
	 */
	public Collection<Event> getData()
	{
		return mem.values();
	}
	/**
	 * Memory contains no records
	 * @return
	 */
	public boolean isEmpty () {
		return mem.isEmpty();
	}
	@Override
	public String toString() {
		return mem.toString();
	}
	
	/**
	 * Add all Events from Memory m
	 * Merges observations of events with same key
	 * @param m
	 */
	public void merge(Memory<Event> m)
	{
		for(Event entry : m.getData())
		{
			Object key = getKey(entry);
			if(mem.containsKey(key))
				mem.get(key).merge(entry);
			else
				mem.put(key, entry);
		}
		
	}
	/**
	 * Empty Memory
	 */
	public void clear(){
		mem.clear();
	}

	public void remove(Collection<Event> events) {
		for(Event ev : events) mem.remove(getKey(ev));
	}
	/**
	 * Groups memory elements by a key
	 * An atom may specify null to group by its string content
	 * Otherwise it can specify a key such as timestamp or component id etc.
	 * Memory events are formed using key:
	 * 	atoms with the same key belong to the same event
	 * @param a
	 * @return
	 */
	public static Object getKey(Atom a)
	{
		Object key = a.group();
		if(key == null)
			return a.observe();
		else
			return key;
	}
	public static Object getKey(Event e)
	{
		if(e.empty()) return null;
		return getKey(e.getAtoms().iterator().next());
	}


}
