package anon.themis.inference;

import java.util.Collection;

import anon.themis.automata.Atom;
import anon.themis.automata.Event;
import anon.themis.automata.Verdict;

/**
 * The Memory interface for storing information
 * @param <K>
 */
public interface Memory<K> extends Cloneable{

	/**
	 * Add an observation to memory
	 * @param a
	 * @param v
	 */
	public void add(Atom a, boolean observed);
	
	/**
	 * Has {a} been observed?
	 * @param a
	 * @return
	 */
	public Verdict get(Atom a);
	/**
	 * Is the K found in the memory?
	 * Returns Verdict.NA if K not in memory
	 * @param e
	 * @return
	 */
	public Verdict get(Event e);
	
	
	
	/**
	 * Memory contains no records
	 * @return
	 */
	public boolean isEmpty ();
	
	/**
	 * Merge {m} with this memory
	 * @param m
	 */
	public void merge(Memory<K> m);
	
	/**
	 * Empty Memory
	 */
	public void clear();
	
	/**
	 * Remove specific K from Memory
	 * @param keys
	 */
	public void remove(Collection<K> keys);
	
	/**
	 * Return memory data
	 * @return
	 */
	public Collection<K> getData();

	public Object clone() throws CloneNotSupportedException;
	

}
