package anon.themis.inference;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import anon.themis.automata.Automata;
import anon.themis.automata.InvalidStateException;
import anon.themis.automata.State;
import anon.themis.automata.Transition;
import anon.themis.automata.Verdict;
import anon.themis.automata.VerdictTimed;
import anon.themis.expressions.bool.BoolAnd;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.BoolOr;
import anon.themis.expressions.bool.False;
import anon.themis.expressions.bool.True;
import anon.themis.utils.Expressions;
import anon.themis.utils.Simplifier;

/**
 * The Execution Encoding History of an automaton
 * We use Rule as a terminology to indicate the entry at a timestamp
 * @see Rule
 */
public class Representation implements Cloneable {
	private Automata aut;
	private Map<Integer, Rule> rules;
	private Map<Integer, Verdict> verdicts;
	private int start 	= 0;
	private int end 	= 0;
	private int lastResolved = 0;
	private int lastVerdict	 = 0;
	private Simplifier simplifier = null;
	

	private Representation() {
		rules 	= new HashMap<Integer, Rule>();
		verdicts= new HashMap<Integer, Verdict>();
		start = end  = lastResolved = lastVerdict = 0;
	}
	/**
	 * Create a newExecution History Encoding from an automaton
	 * @param a
	 */
	public Representation(Automata a) {
		this();
		aut 	= a;
		addInitRule();
	}
	/**
	 * Create a new Execution History Encoding from an automaton that uses a simplifier
	 * @param a
	 * @param simplify
	 */
	public Representation(Automata a, Simplifier simplify) {
		this(a);
		simplifier = simplify;
	}
	
	/**
	 * Create the initial state rule for the start timestamp
	 * @see Representation#addInitRule(int)
	 */
	public void addInitRule() { addInitRule(start); } 
	
	/**
	 * Create the initial state rule at a timestamp 
	 * [timestamp -> q_0 -> true]
	 * @param timestamp 
	 */
	public void addInitRule(int timestamp)
	{
		Rule r	= new Rule(simplifier);
		r.add(aut.getInitialState(), new True());
		rules.put(timestamp, r);
		verdicts.put(timestamp, aut.getVerdict());
	}
	/**
	 * Reset the execution to start from timestamp + 1
	 * This is done by setting the initial state at timestamp t with expression "true"
	 * @param timestamp
	 */
	public void reset(int timestamp) {
		rules.clear();
		verdicts.clear();
		start = end  = lastResolved = lastVerdict = timestamp;
		addInitRule(timestamp);
	}
	@SuppressWarnings("serial")
	private class ERule extends HashMap<State, BoolExpression>{}
	/**
	 * Update ruleset with timestamp t+1
	 * Takes previous rules and appends the transitions going from those states
	 */
	public void tick()
	{
		int timestamp = end + 1;
		Rule update = new Rule(simplifier);
		Map<State, List<Transition>> delta = aut.getDelta();
		Map<State, ERule> build = new HashMap<State,ERule>();
		
		//Where am I?
		for(Entry<State, BoolExpression> e : rules.get(end).getPairs().entrySet())
		{
			//What can I reach?
			for(Transition t : delta.get(e.getKey()))
			{
				State to 		= t.getTo();
				State from 		= t.getFrom();
				BoolExpression expr = Expressions.fromTransition(t, timestamp);
				
				
				if(!build.containsKey(to))
					build.put(to, new ERule());
				ERule ruleEntry = build.get(to);
				
				//From where can I reach it?
				if(!ruleEntry.containsKey(from))
					ruleEntry.put(from, expr);
				else
					ruleEntry.put(from, new BoolOr(ruleEntry.get(from), expr));
			}
			
		}

		for(Entry<State, ERule> e : build.entrySet())
		{
			BoolExpression expr = new False();
			for(Entry<State, BoolExpression> r : e.getValue().entrySet())
			{
				expr = new BoolOr(expr, new BoolAnd(
						rules.get(end).get(r.getKey()), 
						r.getValue()));
			}
			update.add(e.getKey(), expr);
		}
		
		rules.put(timestamp, update);
		verdicts.put(timestamp, Verdict.NA);
		end++;
	}

	/**
	 * Return minimal timestamp in this encoding
	 * @return
	 */
	public int getStart() {
		return start;
	}
	/**
	 * Set the minimal timestamp in this encoding
	 * @param start
	 */
	public void setStart(int start) {
		this.start = start;
	}
	/**
	 * Returns the maximal timestamp in this encoding
	 * @return
	 */
	public int getEnd() {
		return end;
	}
	/**
	 * Set the maximal timestmap in this encoding
	 * @param end
	 */
	public void setEnd(int end) {
		this.end = end;
	}
	/**
	 * Rewrites the inference system starting from timestamp
	 * (1) By simplifying expressions
	 * (2) By rewriting statements based on contents on mem
	 * Returns true if a state can be reached and verdict was updated
	 * @param mem
	 * @param timestampFrom
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public boolean update(Memory mem, Integer timestampFrom)
	{
		return update(mem, timestampFrom, -1);
	}
	/**
	 * Updates up to a certain depth
	 * @param mem
	 * @param depth
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public boolean updateDepth(Memory mem, int depth) {
		return update(mem, lastResolved + 1, depth);
	}
	@SuppressWarnings("rawtypes")
	public boolean update(Memory mem, Integer timestampFrom, int depth)
	{
		boolean updated 			= false;
		int from = Math.max(timestampFrom, lastResolved + 1);
		
		for(int k = from; k <= end; k++)
		{
			Rule r 		= rules.get(k);
			Verdict v 	= verdicts.get(k);
			
			//No need to update a resolved verdict
			if(v.isFinal())	
				continue;
			
			r.update(mem);
			State s = r.resolve(mem);

			if(s == null) {
				depth--;
				if(depth == 0)
					return updated;
				else
					continue;
			}
			lastResolved = Math.max(lastResolved, k);
			try
			{
				Verdict verdict = aut.getVerdict(s);
				verdicts.put(k, verdict);
				
				if(verdict.isFinal())
					lastVerdict = k;
				updated = true;
			}
			catch(InvalidStateException e)
			{
				System.err.print("Could not update verdict: no verdict found for " + s);
			}
		}
		return updated;
	}

	/**
	 * Drop all rules before the resolved (known) state
	 * A state is known if it contains one expression that evaluates to true
	 * @return
	 */
	public int dropResolved() {
		return drop(lastResolved);
	}
	/**
	 * Discard all rules prior to the given timestamp
	 * @param from
	 * @return
	 */
	public int drop(int from) {
		int toRemove = from - start;
		for(int x = start; x < from; x++) {
			rules.remove(x);
			verdicts.remove(x);
		}
		
		start = from;
		return toRemove;
	}
	/**
	 * Returns a new encoding starting at the timestamp
	 * @see Representation#slice(int, int)
	 * @param from
	 * @return
	 */
	public Representation slice(int from) { return slice(from,  end); }
	/**
	 * Returns a new encoding starting from the last known state
	 * @see Representation#slice(int, int)
	 * @see Representation#dropResolved()
	 * @return
	 */
	public Representation sliceLive() {
		return slice(lastResolved, end);
	}
	
	/**
	 * Returns a new encoding that encodes the execution between two timestamps
	 * @param from
	 * @param to
	 * @return
	 */
	public Representation slice(int from, int to) {
		
		Representation newRep = new Representation(aut, simplifier);
		
		from = Math.max(from, start);

		newRep.start    	= from;
		newRep.end      	= from;
		newRep.lastResolved = lastResolved;

		//No slices to give
		if(to < from)	return newRep;
		
		to = Math.min(to, end);
		while(from <= to)
		{
			newRep.rules.put(from, rules.get(from));
			newRep.verdicts.put(from, verdicts.get(from));
			from++;
		}
		newRep.end = to;
		
		return newRep;
	}
	
	/**
	 * Merge a list of rules with the current list of rules
	 * Ignores all data before start
	 * @param rules
	 */
	public void mergeForward(Representation r2)
	{
		
		int from = Math.max(r2.start, start);
		int to	 = r2.end;
		
		for(int t = from; t <= to; t++)
		{
			Rule r;
			if(rules.containsKey(t))
				r = rules.get(t); 
			else 
				r = new Rule(simplifier);
			
			r.merge(r2.rules.get(t));
			rules.put(t, r);
			
			Verdict v2 = r2.verdicts.get(t);
			if(verdicts.containsKey(t))
				verdicts.put(t, Verdict.merge(verdicts.get(t), v2));
			else
				verdicts.put(t ,  v2);
		}
		end = Math.max(end, r2.end);
	}
	
	/**
	 * Returns the currently determined verdict for a timestamp t
	 * @param t
	 * @return
	 */
	public Verdict getVerdict(int t) {
		return verdicts.get(t);
	}
	/**
	 * Returns the verdict associated with the last timestamp
	 * @see Representation#getVerdict(int)
	 * @return
	 */
	public Verdict getVerdictLast() {
		return getVerdict(end);
	}
	/**
	 * Return the timestamp of the last known verdict
	 * @return the lastVerdict
	 */
	public int getLastVerdict()
	{
		return lastVerdict;
	}
	/**
	 *  Find the earliest final verdict
	 * @see Representation#scanVerdict(int, int)
	 * @return
	 */
	public VerdictTimed scanVerdict() 		   { return scanVerdict(start, end); }
	/**
	 * Find the earliest final verdict starting at a given timestamp
	 * @see Representation#scanVerdict(int, int)
	 * @return
	 */
	public VerdictTimed scanVerdict(int start) { return scanVerdict(start, end); }
	/**
	 * Find the earliest final verdict between start and end
	 * @param start
	 * @param end
	 * @return
	 */
	public VerdictTimed scanVerdict(int start, int end)
	{
		int begin 	= Math.max(start, this.start);
		int to		= Math.min(end  , this.end  );
		for(int i = begin; i <= to; i++) {
			if(Verdict.isFinal(verdicts.get(i)))
				return new VerdictTimed(verdicts.get(i), i);
		}
		return new VerdictTimed(Verdict.NA, end);
	}
	/**
	 * Return all final verdicts
	 * @return
	 */
	public List<VerdictTimed> scanVerdictsAll() { 
		List<VerdictTimed> list = new LinkedList<VerdictTimed>();
		
		int begin 	= Math.max(start, this.start);
		int to		= Math.min(end  , this.end  );
		for(int i = begin; i <= to; i++) {
			if(Verdict.isFinal(verdicts.get(i)))
				list.add(new VerdictTimed(verdicts.get(i), i));
		}
		return list;
	}
	
	/**
	 * Return the list of rules for a timestamp
	 * Rules are the [state -> expr] bindings
	 * @return
	 */
	public Map<Integer, Rule> getRules() {
		return rules;
	}
	/**
	 * Returns the list of verdicts
	 * @return
	 */
	public Map<Integer, Verdict> getVerdicts() {
		return verdicts;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Entry<Integer, Rule> entry : rules.entrySet())
		{
			int t 	= entry.getKey();
			Rule r 	= entry.getValue(); 
			String s;
			if(r.isResolved())  s = r.resolve(new MemoryAtoms()).toString();
			else				s = "?";
			sb.append("[" + t + "] [" + s + "]-> " + verdicts.get(t)  + " \n" + entry.getValue().toString());
			sb.append("\n");
		}
		return  sb.toString();
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return this.slice(start);
	}
	/**
	 * Returns the size of the encoding
	 * Size = Number of timestamps encoded
	 * @return
	 */
	public int size() {
		return rules.size();
	}

}
