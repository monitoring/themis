package anon.themis.inference;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import anon.themis.automata.State;
import anon.themis.automata.Verdict;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.expressions.bool.BoolOr;
import anon.themis.expressions.bool.True;
import anon.themis.utils.Expressions;
import anon.themis.utils.Simplifier;

/**
 * A Rule is a map that maps states with boolean expressions
 * 
 * It represents the partial function for each timestamp that 
 * encodes an expression that checks if a state is reachable
 * 
 * get(q) = expr, if expr.eval(mem) then we are in q
 * 
 */
public class Rule implements Serializable {
	private static final long serialVersionUID = 1L;
	
	Map<State, BoolExpression> map;
	Simplifier simplifier = null;
	
	public Rule()
	{
		map = new HashMap<State, BoolExpression>();
	}
	public Rule(Simplifier simp) {
		this();
		this.simplifier = simp;
	}
	
	/**
	 * And an entry to the map
	 * Associate a state with an expression
	 * @param state
	 * @param expr
	 */
	public void add(State state, BoolExpression expr)
	{
		map.put(state, expr);
	}
	/**
	 * Return the expression associated with a state
	 * @param state
	 * @return
	 */
	public BoolExpression get(State state)
	{
		if(map.containsKey(state))
			return map.get(state);
		else
			return null;
	}
	/**
	 * Merge Rules
	 * Two expressions that have the same state are ORed
	 * @param r
	 */
	public synchronized void merge(Rule r)
	{
		for(Entry<State, BoolExpression> e : r.getPairs().entrySet())
		{
			if(map.containsKey(e.getKey()))
				map.put(e.getKey(), new BoolOr(map.get(e.getKey()), e.getValue()));
			else
				map.put(e.getKey(), e.getValue());
		}
	}
	
	/**
	 * Did we find a reachable state?
	 * i.e There exists a state q, such that get(q) == expr, and expr == True
	 * @return
	 */
	public synchronized boolean isResolved() {
		for(Entry<State, BoolExpression> entry : map.entrySet()) {
			if(entry.getValue() instanceof True)
				return true;
		}
		return false;
	}
	/**
	 * Rewrite all expressions with a memory and simplify
	 * This coincides with the rw() operation followed by a simplifier call for each expression
	 * @param mem
	 */
	@SuppressWarnings("rawtypes")
	public synchronized void update(Memory mem)
	{
		for(Entry<State, BoolExpression> entry : map.entrySet())
		{
			BoolExpression replace = Expressions.process(entry.getValue(), mem);
			if(simplifier != null) replace = simplifier.simplify(replace);
			map.put(entry.getKey(), replace);
		}
	}
	/**
	 * Eval operation
	 * Check that expr.eval(mem) == true for all expressions
	 * [Note: Needs to be updated first]
	 * If an expr evaluates to True then cleanup and return the found state
	 * @param mem
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public synchronized State resolve(Memory mem)
	{
		List<State> toRemove = new LinkedList<State>();
		State found = null;
		
		for(Entry<State, BoolExpression> entry : map.entrySet())
		{

			Verdict v = entry.getValue().eval(mem);
			if(v == Verdict.NA) continue;
			if(v == Verdict.TRUE)
			{
				found = entry.getKey();
				break;
			}
			else
				toRemove.add(entry.getKey());
		}

		if(found != null)
		{
			Set<State> states = map.keySet();
			for(State s : states)
				if(s != found)
					toRemove.add(s);
		}
		for(State s : toRemove)
			map.remove(s);

		return found;
	}
	/**
	 * Return the map of state <-> bool expr
	 * @return
	 */
	public Map<State, BoolExpression> getPairs()
	{
		return map;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Entry<State, BoolExpression> entry : map.entrySet())
		{
			sb.append(entry.getValue().toString() + " -> " + entry.getKey());
			sb.append("\n");
		}
		return sb.toString();
	}
}
