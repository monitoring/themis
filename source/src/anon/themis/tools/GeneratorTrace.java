package anon.themis.tools;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

import anon.themis.utils.Convert;
import anon.themis.utils.OptAction;
import anon.themis.utils.Opts;
import anon.themis.utils.Opts.OptError;

/**
 * Trace Generator using DecentMon Traces
 * 
 */
public class GeneratorTrace {
	public static String GEN_TRACE 	= "dm_trace";
	
	private static Opts options = initOptions();
	private static Opts initOptions() {
		Opts opts = new Opts();
		opts.addOpts(
			"-nc",  false, 1, "Number of components",
			"-oc",  false, 1, "Number of observations per component",
			"-st",  false, 1, "Trace Length",
			"-nt",	false, 1, "Number of traces to generate",
			"-out", false, 1, "Output directory to store traces",
			"-at",  true , 1, "Additional arguments for trace generation",
			"-dm",  true , 0, "Save the trace for DecentMon | Default: false",
			"-fmt", true , 1, "File name format: %c component, %t trace number | Default: %t-%c.trace"
		);
		return opts;
	}
	public static class Options {
		public int ncomp;
		public int percomp;
		public int strace;
		public int ntraces;
		public File outdir;
		public boolean keep = false;
		public String fmt   = "%t-%c.trace";
		public String[] atrace = new String[] {};
	}
	public static void npos(int n) throws OptError {
		if(n <= 0) throw new OptError(n + " must be > 0");
	}
	public static void main(String[] args) throws IOException {
		Options config = new Options();
		options.registerActions(
			"-nc", 	(OptAction) x -> {config.ncomp 	 = Integer.parseInt(x[0]); npos(config.ncomp);	},
			"-oc", 	(OptAction) x -> {config.percomp = Integer.parseInt(x[0]); npos(config.percomp);},
			"-st", 	(OptAction) x -> {config.strace	 = Integer.parseInt(x[0]); npos(config.strace);	},
			"-nt",	(OptAction) x -> {config.ntraces = Integer.parseInt(x[0]); npos(config.ntraces);},
			"-at", 	(OptAction) x -> config.atrace	= x[0].split(","),
			"-out", (OptAction) x -> {
					config.outdir 	= new File(x[0]);
					if(!config.outdir.exists())
						throw new OptError(x[0] + ": directory does not exist");
					if(!config.outdir.canWrite())
						throw new OptError(x[0] + ": not writable");
			}
			,
			"-dm", (OptAction) x -> config.keep = true ,
			"-fmt",(OptAction) x -> config.fmt  = x[0]
		);
		
		args = Opts.process(args, options);
		
		genTraces(config.outdir, config.fmt, config.ntraces, config.strace, 
				config.ncomp, config.percomp, 
				config.atrace, config.keep);
	}
	/**
	 * Generate traces
	 * @param outdir Output directory
	 * @param fmt Filename format
	 * @param ntraces Number of traces
	 * @param size Size of a trace
	 * @param components Number of components in the trace
	 * @param percomp Observations per component
	 * @param args Additional args passed to the generator
	 * @param dm_trace Keep a DM trace for use with DecentMon
	 * @throws IOException 
	 */
	public static void genTraces(File outdir, String fmt,
			int ntraces, int size, 
			int components, int percomp, 
			String[] args, boolean dm_trace) 
	throws IOException {
		if(args == null) args = new String[0];
		
		String alpha = generateAlphabet(components, percomp);
		Object[] params = new Object[args.length + 4];
		params[0] = "-dalpha";
		params[1] = alpha;
		params[2] = "-st";
		params[3] = size == 1 ? 1 : size - 1;
		for(int j = 0; j < args.length; j++)
			params[3+j] = args[j];
		
		for(int n = 0; n < ntraces; n++) {
			InputStream stream = (Convert.runProc(GEN_TRACE, params))
									.getInputStream();
			Scanner scan = new Scanner(stream);
			String trace = scan.nextLine();
			scan.close();
			writeTrace(alpha, trace, n, outdir, fmt);
			
			//DecentMon
			if(dm_trace) {
				File tracefiledm = newFile(outdir, fmt, "dm", n);
				writeFile(tracefiledm, alpha + "\n" + trace );
			}
		}
	}
	
	@SuppressWarnings("serial")
	public static class Syms extends HashSet<String> {}
	
	public static File newFile(File root, String format, String component, Integer trace) {
		String name = format.replace("%c", component).replace("%t", trace.toString());
		File f = new File(root, name);
		try {
			f.createNewFile();
		} catch (IOException e) {
			return null;
		}
		return f;
	}
	public static void writeFile(File file, String s) throws FileNotFoundException {
		PrintWriter write = new PrintWriter(file);
		write.println(s);
		write.flush();
		write.close();
	}
	public static void writeTrace(String alpha, String trace, int id, File out, String format) throws IOException {
		Syms[] full = getSymbols(alpha);
		
		//Create trace files
		PrintWriter[] traces = new PrintWriter[full.length];
		for(int i = 0; i < traces.length; i++)
		{
			File f = newFile(out, format, String.valueOf(((char) ('a' + i))), id);
			f.createNewFile();
			try {
				traces[i] = new PrintWriter(f);
			} catch (Exception e) {}	
		}
	
		//Read trace and split it to files
		String[] steps = trace.split(";");
		for(int i = 0; i < steps.length; i++) {
			Syms[] step = getSymbols(steps[i]);
			for(int j = 0; j < traces.length; j++) {
				PrintWriter writer = traces[j];
				Iterator<String> iter = full[j].iterator();
				while(iter.hasNext()) {
					String sym = iter.next();
					writer.print(sym + ":");
					writer.print(step[j].contains(sym) ? "t" : "f");
					if(iter.hasNext())
						writer.print(",");
				}
				writer.println();
				writer.flush();
			}
		}

	}
	public static Syms[] getSymbols(String alpha) {
		alpha = alpha.trim();
		alpha = alpha.substring(1, alpha.length() - 1);
		String[] comps 	= alpha.split("[|]");
		Syms[] symbols 	= new Syms[comps.length];
		for(int i = 0; i < comps.length; i++) {
			symbols[i] = new Syms();
			for(String sym : comps[i].split(","))
				symbols[i].add(sym);
		}
		return symbols;
	}
	public static String generateAlphabet(int ncomp, int percomp) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for(int i = 0; i < ncomp; i++) {
			String prefix = String.valueOf((char) ('a' + i));
			sb.append(prefix + "0");
			for(int j = 1; j < percomp; j++) {
				sb.append(",").append(prefix + j);
			}
			if(i != ncomp - 1)
				sb.append("|");
		}
		sb.append("}");
		return sb.toString();
	}

}
