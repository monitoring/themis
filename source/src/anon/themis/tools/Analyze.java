package anon.themis.tools;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import anon.themis.automata.Automata;
import anon.themis.automata.State;
import anon.themis.automata.Transition;
import anon.themis.expressions.bool.BoolExpression;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.monitoring.Specification;
import anon.themis.utils.Convert;
import anon.themis.utils.Expressions;
import anon.themis.utils.SpecLoader;
import anon.themis.utils.Expressions.ExprSize;

/**
 * Basic tool to return parameters on the complexity of a specification
 * automaton
 * 
 */
public class Analyze {
	public static class Stats {
		public Integer maxLabel = -1;
		public Integer states	 = -1;
		public Integer maxTransitionPairs = -1;
		public Integer maxOut = -1;
		public Double  avgOut = 0d;
		public Double  avgLabel = 0d;
		public Double  avgTransitionPairs = 0d;
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder("{ ");
			builder.append("states: " 	+ states);
			builder.append(", maxLabel: " + maxLabel);
			builder.append(", avgLabel: " + avgLabel);
			builder.append(", maxOut: " + maxOut);
			builder.append(", avgOut: " + avgOut);
			builder.append(", maxTransitionPairs: " + maxTransitionPairs);
			builder.append(", avgTransitionPairs: " + avgTransitionPairs);
			builder.append(" }");
			return builder.toString();
		}
		
	}
	public static void main(String[] args) {
		Map<String, Specification> specs = SpecLoader.loadSpec(new File(args[0]));
		
		for(Entry<String, Specification> spec : specs.entrySet())
			System.out.printf("%-20s : %s\n", spec.getKey(), analyze(spec.getValue()));
		
	}

	public static Stats analyze(Specification specsrc) {
		try {
			SpecAutomata spec =  Convert.makeAutomataSpec(specsrc);
			if(spec == null) return null;
			return analyze(spec.getAut());
			
		} catch (Exception | Error e) {
			System.err.println("Failed: " + e.getLocalizedMessage());
			return null;
		}
	}
	
	public static Stats analyze(Automata aut) {
		Stats stat = new Stats();
		stat.states = aut.getStates().size();
		Map<String, Integer> pairs = new HashMap<String, Integer>();
		Integer sumPairs = 0;
		Integer sumLabel = 0;
		Integer cLabel = 0;
		Integer sumOut = 0;
		
		for(Entry<State, List<Transition>> entry : aut.getDelta().entrySet()) {
			String origin = entry.getKey().toString();
			for(Transition t : entry.getValue()) {
				String dest = t.getTo().toString();
				String k    = origin.compareTo(dest) < 0 
								? origin + "-" + dest 
								: dest + "-" + origin;
				if(pairs.containsKey(k)) { 
					pairs.put(k, pairs.get(k) + 1);
				}
				else
					pairs.put(k, 1);
				
				BoolExpression expr = Expressions.fromTransition(t, 0);
				ExprSize size 		= Expressions.getLength(expr);
				stat.maxLabel 		= Math.max(stat.maxLabel, size.leaves.size());
				sumLabel		   += size.leaves.size();
				cLabel++;
			}
			stat.maxOut = Math.max(stat.maxOut, entry.getValue().size());
			sumOut		+= entry.getValue().size();
			
		}
		for(Integer i : pairs.values()) {
			sumPairs += i;
			stat.maxTransitionPairs = Math.max(stat.maxTransitionPairs, i);
		}
		stat.avgLabel = ((double) sumLabel) /  cLabel;
		stat.avgTransitionPairs = ((double) sumPairs) / pairs.size();
		stat.avgOut = ((double) sumOut) / aut.getStates().size();
		
		return stat;
	}
}
