package anon.themis.tools;

/**
 * Utility Tool to convert formulas from LTL2Mon format to DecentMon
 */
public class Formatter {
	private static class Tuple {
		String from;
		String to;
		public Tuple(String from, String to) {
			this.from = from;
			this.to   = to;
		}
		public String replace(String s) {
			return s.replace(from, to);
		}
	}
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.print("Specify LTL formula");
			System.exit(1);
		}
		
		Tuple[] replacements = new Tuple[] {
				new Tuple("&&", "&"),
				new Tuple("||", "|"),
				new Tuple("!" , "-"),
				new Tuple("[]", "G"),
				new Tuple("<>", "F"),
		};
		String res = args[0];
		for(int i = 0; i < replacements.length; i++)
			res = replacements[i].replace(res);
		System.out.println(res);
	}
}
