package anon.themis.tools.experiment;

import java.util.Map;

import anon.themis.monitoring.Specification;


public interface  TestOracle {
	boolean verify(Map<String, Specification> spec);
}
