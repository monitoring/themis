/**
 * ConfigExperiment
 *
 * name <email>
 */
package anon.themis.tools.experiment;


public enum ConfigExperiment {
	
	SKIP_TESTS 		("tests.skip")
	, SPEC_DISCARD 	("tests.discard")
	, SPEC_VERIFIER	("tests.oracle")
	, TRACES_DIR 	("traces.dir")
	, TRACES_GEN 	("traces.generate")
	, TRACES_COMP 	("traces.components")
	, TRACES_OBS 	("traces.observations")
	, TRACES_NUM	("traces.maximum")
	, TRACES_LEN	("traces.length")
	, RUN_LEN 		("run.length")
	, RUN_COMP 		("run.components")
	, RUN_SPECS 	("run.specs")
	, RUN_ALGS 		("run.algorithms")
	;
	
	private String value;

	private ConfigExperiment(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

}
