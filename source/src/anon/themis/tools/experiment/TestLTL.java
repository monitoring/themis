/**
 * TestLTL
 *
 * name <email>
 */
package anon.themis.tools.experiment;

import java.util.Map;

import anon.themis.monitoring.SpecLTL;
import anon.themis.monitoring.Specification;
import anon.themis.tools.Verify;


public class TestLTL implements TestOracle {

	public TestLTL() { }
	
	@Override
	public boolean verify(Map<String, Specification> spec) {
		return Verify.verify((SpecLTL) spec.get("root"));
	}

}
