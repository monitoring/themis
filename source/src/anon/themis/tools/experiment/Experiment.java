package anon.themis.tools.experiment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.LinkedList;
import java.util.HashSet;

import anon.themis.monitoring.Component;
import anon.themis.monitoring.DefaultSymbolMapper;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.Specification;
import anon.themis.tools.GeneratorTrace;
import anon.themis.traces.TraceFile;
import anon.themis.utils.Opts;
import anon.themis.utils.SpecLoader;

/**
 * Command-Line tool to run an experiment
 */
public class Experiment  {
	private static String PATH_CONFIG = "setup.properties";
	
	@SuppressWarnings("serial")
	private static class Config extends Properties {
		public String getProperty(ConfigExperiment key) {
			return getProperty(key.getValue());
		}
		public void merge() {
			System.getProperties().putAll(this);
		}
	}

	public static class DevNull extends OutputStream {
		  public void write(int b) throws IOException { }
	}
	
	public static void main(String[] args) throws Exception {
		// Basic Diagnostics
		
		
		
		if(args.length != 1)	abort("Experiment path not provided");
		
		File root = new File(args[0]);
		if(!root.exists())		abort("Experiment path does not exist: " + args[0]);
		
		
		
		notify("Running experiment: " + root.getAbsolutePath());
		
		File cfile = new File(root, PATH_CONFIG);
		FileReader cread = new FileReader(cfile);

		Config config = new Config();
		config.load(cread);
		cread.close();
	
		
		notify("Using configuration: " + cfile.getAbsolutePath());
		if(!verifyConfig(config)) 	abort("Experiment configuration incomplete or invalid");
		
		config.merge();
		
		
		// Load Algorithms
		File algfile = new File(root, config.getProperty(ConfigExperiment.RUN_ALGS));
		if(!algfile.exists())		abort("Could not find Algorithms file: " + algfile.getAbsolutePath());
		notify("Loading algorithms in: " +  algfile.getAbsolutePath());
		
		List<Class<MonitoringAlgorithm>> algs = new LinkedList<Class<MonitoringAlgorithm> >();
		BufferedReader in = new BufferedReader(new FileReader(algfile));
		String line = "";
		while( (line = in.readLine()) != null) {
			try {
				@SuppressWarnings("unchecked")
				Class<MonitoringAlgorithm> cls = 
						(Class<MonitoringAlgorithm>) Experiment.class.getClassLoader().loadClass(line);
				algs.add(cls);
				notify("Loaded: " + line);
			}
			catch(Exception e) {
				abort("Could not load: " + line + " (" + e.getMessage() + ")");
			}
		}
		in.close();

		
		// Verify Traces
		int ntcomps = Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_COMP));
		int nrcomps = Integer.parseInt(config.getProperty(ConfigExperiment.RUN_COMP));
		int ntraces = Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_NUM));
		int nobs 	= Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_OBS));
		int tlen 	= Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_LEN));
		int runlen 	= Integer.parseInt(config.getProperty(ConfigExperiment.RUN_LEN));

		
		if(nrcomps > ntcomps) 
			abort("Run requires more components than those provided in trace");
		
		boolean generate = !config.getProperty(ConfigExperiment.TRACES_GEN).equals("false");
		File tfile = new File(root, config.getProperty(ConfigExperiment.TRACES_DIR));
		if(!tfile.exists() || !tfile.isDirectory())
			abort("Cannot load traces: invalid directory " + tfile.getAbsolutePath());
		if(tfile.listFiles().length == 0) {
			if(!generate)
				abort("Trace directory empty: " + tfile.getAbsolutePath());
			else
			{
				notify("Generating "+ ntraces + " traces of size " + tlen);
				try {
					GeneratorTrace.genTraces(tfile, "%t-%c.trace", 
							ntraces, tlen,
							ntcomps, nobs, 
							new String[0], 
							true
					);
				}
				catch(Exception e) {
					abort("Could not create traces: " + e.getMessage());
				}
			}
		}
		else
			notify("Reusing traces in "+  tfile.getAbsolutePath());
		
		

		
		// Verify Formulas
		
		File forms = new File(root, config.getProperty(ConfigExperiment.RUN_SPECS));
		if(!forms.exists())		abort("Could not find formula file: " + forms.getAbsolutePath());
		
		if(!config.getProperty(ConfigExperiment.SKIP_TESTS).equals("true")) {
			String oraclecls = config.getProperty(ConfigExperiment.SPEC_VERIFIER);
			if(oraclecls.isEmpty()) abort("No Verifier for the specification provided");
			
			try {
				@SuppressWarnings("unchecked")
				Class<? extends TestOracle> cls = (Class<? extends TestOracle>) 
						Experiment.class.getClassLoader().loadClass(oraclecls);
				
				TestOracle oracle = cls.newInstance();
				notify("Using Verifier: " + oracle.getClass().getCanonicalName());
				verifySpec(oracle, forms, config.getProperty(ConfigExperiment.SPEC_DISCARD));
			
			}catch(Exception e) {
				e.printStackTrace();
				abort("Could not verify specification: "+ e.getMessage());
			}
			
		
		}


		notify("Beginning Runs");
		
		int totalForms 	= 0;
		int currentProg = 1;
		in = new BufferedReader(new FileReader(forms));
		while( (line = in.readLine()) != null ) {
			totalForms++;
		}
		in.close();
		in = new BufferedReader(new FileReader(forms));
		int totalAlgs 	= algs.size();
		
		

		
		while( (line = in.readLine()) != null ) {
			int currentAlg 	= 0;
			for(Class<MonitoringAlgorithm> algcls : algs) {
				currentAlg++;
				
				MonitoringAlgorithm alg 	= algcls.newInstance();
				MonitorSettings settings 	= new MonitorSettings();
				
				
				Map<String, Specification> spec = SpecLoader.loadSpec(new File(root,line));

				settings.setSpec(spec);
				settings.setRunLength(runlen);
				settings.setMapper(new DefaultSymbolMapper());
				
				
				bindTrace(tfile, 0, nrcomps, settings);
				alg.setConfig(settings);

				try {
					alg.setupNetwork();
				} catch(Error e) {
					abort("Monitoring Setup Failed: " + e.getLocalizedMessage());
				}
				settings.clearTraces();
				int vlvl = Integer.parseInt(Opts.getGlobal("THEMIS_BENCH_VERBOSE", "0"));
				try {
					for(int trace = 0; trace < ntraces; trace++) {
						
						bindTrace(tfile, trace, nrcomps, settings);
						statusLine(currentProg, totalForms, 
								trace + 1, ntraces, 
								currentAlg, totalAlgs,
								alg.getClass().getSimpleName());
						
						PrintStream out = System.out;
						if(vlvl < 10) {
							System.setOut(new PrintStream(new DevNull()));
							alg.start();
							System.setOut(out);
						}
						else
							alg.start();
						settings.clearTraces();
					}
				}
				catch(Error e) {
					System.err.println("Monitoring Failed: " + e.getLocalizedMessage());
					e.printStackTrace();
					System.exit(1);
				}
			}
			
			currentProg++;
		}
		System.out.println();
		System.out.println("Done");
	}

	private static void bindTrace(File input, int tid, int ncomps, MonitorSettings settings) 
	throws FileNotFoundException {
		for(int i = 0; i < ncomps; i++) {
			Component comp = new Component(String.valueOf( (char) ('a' + i)));
			settings.registerTrace(comp, new TraceFile(new File(input, 
					tid+ "-" + comp.toString() + ".trace")));
		}
	}

	private static void verifySpec(TestOracle oracle, File forms, String discard) {
		
		notify("Verifying specifications in: " + forms.getAbsolutePath());
		Set<Integer> detected = new HashSet<Integer>();
		BufferedReader in;
		int lineno = 1;
		int maxlines = 0;
		String spec;
		try {
			in = new BufferedReader(new FileReader(forms));
			while(in.readLine() != null)
				maxlines++;
			in.close();

			in = new BufferedReader(new FileReader(forms));
			while( (spec = in.readLine()) != null) {
				System.out.printf("\r%8d/%8d", lineno, maxlines);
				
				Map<String, Specification> specinfo = SpecLoader.loadSpec(new File(spec));
				if(!oracle.verify(specinfo)) {
					if(discard.isEmpty())
						abort("Could not verify spec" + spec);
					else
						detected.add(lineno);
				}

				lineno++;
			}
			System.out.println();
			in.close();
			
			if(detected.size() > 0) {
				in 		= new BufferedReader(new FileReader(forms));
				lineno 	= 1;
				
				PrintWriter fkeep = new PrintWriter(
						new FileOutputStream(forms.getCanonicalPath() + ".keep"));
				PrintWriter fdiscard = new PrintWriter(
						new FileOutputStream(forms.getCanonicalPath() + ".discard"));
				
				while((spec = in.readLine()) != null) {
					if(detected.contains(lineno))
						fdiscard.println(spec);
					else
						fkeep.println(spec);
					lineno++;
				}
				fkeep.close();
				fdiscard.close();
				in.close();
				abort("Invalid specs: " + detected.size());
			}
		} catch (IOException e) {
			abort(e.getLocalizedMessage());
		}


	}
	private static boolean verifyConfig(Config conf) {
		for(ConfigExperiment v : ConfigExperiment.values()) {
			if(!conf.containsKey(v.getValue()))
			{
				System.err.println("Missing: " + v.getValue());
				return false;
			}
		}
		return true;
	}
	private static void statusLine(int f_cur, int f_total, int t_cur, int t_total, 
			int c_alg, int t_alg, String alg) {
		System.out.printf("\rSpec: %8d/%8d | Trace: %8d/%8d | Algorithm: (%3d/%3d) %-50s", 
				f_cur, f_total, t_cur, t_total, c_alg, t_alg, alg);
	}
	private static void abort(String message) {
		System.err.println(message);
		System.exit(1);
	}
	private static void notify(String message) {
		System.out.println(message);
	}
	
}
