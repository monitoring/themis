package anon.themis.tools;

import java.io.File;
import java.util.Map;

import anon.themis.monitoring.Component;
import anon.themis.monitoring.DefaultSymbolMapper;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.Specification;
import anon.themis.traces.TraceFile;
import anon.themis.utils.OptAction;
import anon.themis.utils.Opts;
import anon.themis.utils.SpecLoader;
import anon.themis.utils.Opts.OptError;

/**
 * Command-Line tool to run the formulae
 */
public class Run  {
	private static Opts options = initOptions();
	private static Opts initOptions() {
		Opts opts = new Opts();
		opts.addOpts(
			"-nc",  false, 1, "Number of components",
			"-spec",false, 1, "Spec file",
			"-tid",	false, 1, "Trace ID to read",
			"-tmax",true,  1, "Trace ID to read last, if specified, the traces from tid to tmax will be ran",
			"-nr",	false, 1, "Length of the Run",
			"-in",  false, 1, "Directory to read traces",
			"-alg", false, 1, "Full java classname of algorithm to use"
		);
		return opts;
	}
	public static class Options {
		public int 	ncomp;
		public int 	nrun;
		public int  tid;
		public int tmax = -1;
		public File in;
		public Map<String, Specification> spec;
		public MonitoringAlgorithm alg;
		public boolean simplify = false;
	}
	public static void npos(int n) throws OptError {
		if(n <= 0) throw new OptError(n + " must be > 0");
	}
	public static void nzer(int n) throws OptError {
		if(n <  0) throw new OptError(n + " must be >= 0");
	}

	public static void main(String[] args) throws Exception {
		Options config = new Options();
		options.registerActions(
			"-nc", 	(OptAction) x -> {config.ncomp 	 = Integer.parseInt(x[0]); npos(config.ncomp);	},
			"-nr", 	(OptAction) x -> {config.nrun	 = Integer.parseInt(x[0]); npos(config.nrun);	},
			"-tid", (OptAction) x -> {config.tid	 = Integer.parseInt(x[0]); nzer(config.tid);	},
			"-tmax",(OptAction) x -> {config.tmax	 = Integer.parseInt(x[0]); nzer(config.tmax);	},
			"-simp",(OptAction) x -> config.simplify = true,
			"-spec", (OptAction) x -> {
				File ltl = new File(x[0]);
				if(!ltl.exists())
					throw new OptError(x[0] + ": does not exist");

				config.spec = SpecLoader.loadSpec(ltl);
			},
			"-in", (OptAction) x -> {
					config.in 	= new File(x[0]);
					if(!config.in.exists())
						throw new OptError(x[0] + ": directory does not exist");
					if(!config.in.canRead())
						throw new OptError(x[0] + ": not readable");
			},
			"-alg", (OptAction) x -> {
				@SuppressWarnings("unchecked")
				Class<MonitoringAlgorithm> cls = 
					(Class<MonitoringAlgorithm>) Run.class.getClassLoader().loadClass(x[0]);
				config.alg = cls.newInstance();
			}
		);
		
		args = Opts.process(args, options);
		
		if(config.spec == null) {
			System.err.println("No Spec given");
			options.printUsage();
		}
		
		
		MonitorSettings settings = new MonitorSettings();
		settings.setSpec(config.spec);
		
		settings.setRunLength(config.nrun);
		settings.setMapper(new DefaultSymbolMapper());
		
		

		MonitoringAlgorithm alg = config.alg;

		for(int i = 0; i < config.ncomp; i ++) {
			Component comp = new Component(String.valueOf( (char) ('a' + i)));
			settings.registerTrace(comp, new TraceFile(new File(config.in, 
					config.tid + "-" + comp.toString() + ".trace")));
		}

		alg.setConfig(settings);
		try {
			alg.setupNetwork();
		} catch(Error e) {
			System.err.println("Monitoring Setup Failed: " + e.getLocalizedMessage());
			e.printStackTrace();
			System.exit(1);
		}
		
		config.tmax = Math.max(config.tid, config.tmax);
		try {
			settings.clearTraces();
			for(int trace = config.tid; trace <= config.tmax; trace++) {
				for(int i = 0; i < config.ncomp; i ++) {
					Component comp = new Component(String.valueOf( (char) ('a' + i)));
					settings.registerTrace(comp, new TraceFile(new File(config.in, 
							trace + "-" + comp.toString() + ".trace")));
				}
				alg.start();
			}
			
		}
		catch(Error e) {
			System.err.println("Monitoring Failed: " + e.getLocalizedMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

}
