# THEMIS: Example Usage #

This small project illustrates how it is possible to create new measures and algorithms using the THEMIS framework.

### Overview ###

* The [src](src/) folder contains all necessary source files for the additions. 
* The [test](test/) folder contains a small experiment designed to test the new algorithm and measure.


### Creating a Measure ###
The [MeasureEx.aj](src/example/measure/MeasureEx.aj) creates an instrumentation that counts the calls to `Memory.get()` thereby tracking the number of requested evaluations.
The file illustrates how it is possible to:

* Use AspectJ to intercept the existing algorithm and datastructures to instrument running algorithms.
* Use the `Instrumentation` API to create and update measures.

It is also possible to specify which measure to disable or enable by using AspectJ's Load-Time Weaving (LTW) configuration.
This is found in [aop.xml](src/META-INF/aop.xml), the file configures the AspectJ agent that weaves the aspects during load-time. In short, this allows us to disable and enable aspects which in turn we can use to enable/disable measures.
The provided [aop.xml](src/META-INF/aop.xml) disables the [LTLData](../source/src/anon/themis/measurements/LTLData.aj) measure and enables [MeasureEx](src/example/measure/MeasureEx.aj).
When loaded before the `themis.jar` on the classpath, it overrides the default [aop.xml](../source/src/META-INF/aop.xml).

### Creating an Algorithm ###
The [MonitoringEx.java](src/example/alg/MonitoringEx.java) creates a new monitoring algorithm that simply sets up one monitor that prints the observations on the first component.
It shows the basic functionality of:

* Creating a monitor `MonitorEx`;
* Attaching monitors to components;
* Handling verdicts.

### Testing the Algorithm ###
The [Makefile](Makefile) allows two different build instructions so as to illustrate various ways to build the tools:


Make sure you are in the [example](.) directory:
```bash
cd example
```

#### Building with Maven ####
To build the example project (measure + algorithm) using Maven using the [pom.xml](pom.xml) file, run:
```bash
make build
```

#### Build using AJC (AspectJ Compiler) ####
To build the example project using the AspectJ compiler, run:
```bash
make buildajc
``` 

#### Testing ####
After building the project a `JAR` file will be created under `target`.
To test the project, run it using: 
```bash
make run
```
In the testing experiment we simply append our jar file generated from the build to the classpath and invoke the regular tools of THEMIS (in this case **Experiment**).
