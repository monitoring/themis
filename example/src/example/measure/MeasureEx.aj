package example.measure;

import anon.themis.inference.Memory;
import anon.themis.measurements.Instrumentation;
import anon.themis.measurements.Measure;
import anon.themis.measurements.Measures;
import anon.themis.monitoring.MonitoringAlgorithm;

import example.alg.MonitorEx;

public aspect MeasureEx extends Instrumentation {
	after() : call(* Memory.get(..)) {
		update("ex_memory", 1);
	}
	after() : call(* MonitorEx.someFunc()) {
		System.out.println(" -- Intercept -- ");
	}
	
	protected void setupOnce(MonitoringAlgorithm alg) {
		
	};
	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Example Measure");
		addMeasure(new Measure("ex_memory", "Memory Access",  0, Measures.addInt));
	};
}
