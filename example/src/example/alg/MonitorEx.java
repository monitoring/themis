package example.alg;

import anon.themis.automata.Atom;
import anon.themis.inference.Memory;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;

public class MonitorEx extends GeneralMonitor {

	public MonitorEx(int id) {
		super(id);
	}
	
	@Override
	public void communicate() {
		
	}

	@Override
	public void monitor(int timestamp, Memory<Atom> obs) throws ReportVerdict,
			ExceptionStopMonitoring {
		
		someFunc();
		System.out.printf("Monitor [%3d] @ %3d - %s\n", this.getID(), timestamp, obs.toString());
	}

	@Override
	public void reset() {

	}

	@Override
	public void setup() {
		
	}

	public void someFunc() {
		
	}
}
