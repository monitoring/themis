package example.alg;

import java.util.HashMap;
import java.util.Map;

import anon.themis.monitoring.Component;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;

public class MonitoringEx extends MonitoringAlgorithm {

	@Override
	public void abort(ExceptionStopMonitoring arg0) {
		System.out.println("Aborted");
	}

	@Override
	public void report(ReportVerdict arg0) {
		System.out.println("New Verdict: " + arg0.toString());
		
	}

	@Override
	protected Map<Integer, ? extends Monitor> setup() {
		Map<Integer, MonitorEx> map = new HashMap<Integer, MonitorEx>(1);
		MonitorEx m = new MonitorEx(0);
		map.put(0, m);
		attachMonitor(new Component("a"), m);
		return map;
	}

}
